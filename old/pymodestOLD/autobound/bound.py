import re
def findTag(tagstart, tagend, filename):
	textfile = open(filename, 'r')
	filetext = textfile.read()
	textfile.close()
	matches = re.findall(tagstart+"(.*?)"+tagend, filetext)
	searchResult=[]
	for match in matches:
		if("," in match):
			newadd=match.split(",")
			for elem in newadd:
				if("=" in elem):
					safe=elem.split("=")
					elem=safe[0]
				searchResult=searchResult+[elem.strip()]
		elif("=" in match):
			safe=match.split("=")
			searchResult=searchResult+[safe[0].strip()]
		else:
			searchResult=searchResult+[match.strip()]

	return searchResult


def findTag2(tagstart, tagend, filename):
	textfile = open(filename, 'r')
	filetext = textfile.read()
	textfile.close()
	matches = re.findall(tagstart+"(.*?)"+tagend, filetext)
	return matches


filename="demo.txt"
bounded_int=findTag("const int", ";", filename)
not_bounded_int=findTag("int", ";", filename)

not_bounded_int = list(set(not_bounded_int)^set(bounded_int))
if(len(not_bounded_int)>0):
	print("Unbounded variables found!")
	print(str(len(not_bounded_int))+" instance(s) found!")
	#bound variables definition
	with open (filename, 'r' ) as f:
    		content = f.read()
	for elem in not_bounded_int:
		re.sub(r"Pulp Fiction", "Forrest Gump", text)
	#bound updates
else:
	print("All variables are bounded!")


#print(findTag2("= count", "=", filename))
#print(re.findall("{= count"+"(.*?)"+"=}", "   b {= count = count + 1 =}"))
