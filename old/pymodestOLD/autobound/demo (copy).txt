action a, b, c;

const int BOUND = 20;

const int BOUND222 = 20;
int count = 0;
bool did_c = false;

property DidWeDoC = E <> did_c;

do {
:: a;
   b {= count = min(count + 1, BOUND) =}
:: when(count < 0) b;
   a;
   break
};
do {
:: c {= did_c = true =}
}
