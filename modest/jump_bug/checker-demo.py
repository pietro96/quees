# Run as python checker-demo.py model.py
# Requires Python 3.7 or newer

import sys
from importlib import util
from timeit import default_timer as timer
import os



############################ Run Modest Model and gen. graph #################################
# Define Modest installation
#modest = "/usr/bin/Modest-linux-x64-20191112/Modest/modest"  # 1st Version
#modest = "/usr/bin/Modest-linux-x64-20191203/Modest/modest"   # 2nd Version
#modest = "/usr/bin/Modest-linux-x64-20191205/Modest/modest"   # 3rd Version
modest = "/usr/bin/Modest-linux-x64-20191214/Modest/modest"   # 4th Version


modest_file=os.path.basename(sys.argv[1])
modest_file_name=os.path.splitext(modest_file)[0]
dot_file = modest_file_name+'.dot'
pdf_file = modest_file_name+'.pdf'
python_file=modest_file_name+'.py'

os.system(modest+" export-to-dot "+sys.argv[1]+" -O "+dot_file+" -Y --dot pdf "+pdf_file+" --open-output --no-time-progress")
os.system(modest+" export-to-python "+sys.argv[1]+" -O "+ python_file +" -Y")
#############################################################################################


################################## Start Load the model #####################################
if len(sys.argv) < 2:
	print("Error: No model specified.")
	quit()
print("Loading model from \"{0}\"...".format(python_file), end = "", flush = True)
spec = util.spec_from_file_location("model", python_file)
model = util.module_from_spec(spec)
spec.loader.exec_module(model)
network = model.Network() # create network instance
print(" done.")
################################## End Load the model #######################################

#check jump function
is_reward = network.properties[0].exp is not None and network.properties[0].exp.op == "xmin" and network.properties[0].exp.args[1].op == "ap"
if is_reward:
	reward_exps = [network.properties[0].exp.args[0]]
initial_state = network.get_initial_state()
initial_state_transitions = network.get_transitions(initial_state)


i=0
while i<4:
	print("Reward exps (a): ",reward_exps)
	#reward_exps_eval=[network.properties[0].exp.args[0]]
	reward_exps_eval=reward_exps.copy()
	print("Reward exps (b): ",reward_exps_eval)
	next_state = network.jump_np(initial_state, initial_state_transitions[0], reward_exps_eval)
	print("Reward exps evaluation: ", reward_exps_eval)
	print("#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*")
	i=i+1