# Run as python checker-demo.py model.py
# Requires Python 3.7 or newer

import sys
from importlib import util
from timeit import default_timer as timer
import os
from termcolor import colored

def explore_neighbours(current_state, transitions, visited_states, states_queue):
	add2_queue = set()
	states_queue = set(states_queue)
	for transition in transitions:
		next_state = network.jump_np(current_state, transition)
		add2_queue.add(next_state)

	add2_queue = add2_queue - add2_queue.intersection(visited_states) - add2_queue.intersection(states_queue)
	return list(add2_queue)



def reachable_states(initial_state):
	current_state=initial_state
	transitions = network.get_transitions(current_state)
	visited_states={initial_state}
	states_queue=explore_neighbours(current_state, transitions, visited_states, [])
	print("* The current state is:", str(current_state))
	print(colored("* The value of ap(0) in the initial state is:", "green"), str(network.get_expression_value(current_state, 0)))
	while len(states_queue)!=0:
		current_state = states_queue.pop()
		print("* The current state is:", str(current_state))
		print(colored("* The value of ap(0) in the initial state is:", "green"), str(network.get_expression_value(current_state, 0)))
		visited_states.add(current_state)
		transitions = network.get_transitions(current_state)
		neighbours=explore_neighbours(current_state, transitions, visited_states, states_queue)
		states_queue = neighbours + states_queue
	return visited_states




############################ Run Modest Model and gen. graph #################################
#modest = "/usr/bin/Modest-linux-x64-20191112/Modest/modest" 
#modest = "/Applications/Modest/modest" #MAC OS
modest = "/usr/bin/Modest-linux-x64-20191203/Modest/modest" 
modest_file=os.path.basename(sys.argv[1])
modest_file_name=os.path.splitext(modest_file)[0]
dot_file = modest_file_name+'.dot'
pdf_file = modest_file_name+'.pdf'
python_file=modest_file_name+'.py'


os.system(modest+" export-to-dot "+sys.argv[1]+" -O "+dot_file+" -Y --dot pdf "+pdf_file+" --open-output --no-time-progress")
os.system(modest+" export-to-python "+sys.argv[1]+" -O "+ python_file +" -Y")
#############################################################################################


################################## Start Load the model #####################################
if len(sys.argv) < 2:
	print("Error: No model specified.")
	quit()
print("Loading model from \"{0}\"...".format(python_file), end = "", flush = True)
spec = util.spec_from_file_location("model", python_file)
model = util.module_from_spec(spec)
spec.loader.exec_module(model)
network = model.Network() # create network instance
print(" done.")
################################## End Load the model #######################################

initial_state = network.get_initial_state()
reachable_states(initial_state)

sys. exit()
# Print some information about the model and the initial state
start_time = timer()
print("* The model has", str(len(network.properties)), "properties.")
print("* The first property is:", str(network.properties[0]))
is_reach = network.properties[0].exp is not None and network.properties[0].exp.op == "exists" and network.properties[0].exp.args[0].op == "eventually" and network.properties[0].exp.args[0].args[0].op == "ap"
print("* The first property is", "" if is_reach else " not", " a reachability property.", sep = "")
reward_exps = []
is_reward = network.properties[0].exp is not None and network.properties[0].exp.op == "xmin" and network.properties[0].exp.args[1].op == "ap"
if is_reward:
	reward_exps = [network.properties[0].exp.args[0]]
print("* The first property is", "" if is_reward else " not", " a cost-optimal reachability property.", sep = "")
initial_state = network.get_initial_state()
print("* The initial state is:", str(initial_state))
initial_state_transitions = network.get_transitions(initial_state)
print("* The value of ap(0) in the initial state is:", str(network.get_expression_value(initial_state, 0)))
print("* The initial state has", str(len(initial_state_transitions)), "transitions.")
print("* The first transition out of the initial state is labelled:", network.transition_labels[initial_state_transitions[0].label])
next_state = network.jump_np(initial_state, initial_state_transitions[0], reward_exps)
if len(reward_exps) != 0:
	print("* The transition has a reward of", reward_exps[0], "for the first property.")
print("* The transition leads to state:", str(next_state))
print("* The value of ap(0) in that state is:", str(network.get_expression_value(initial_state, 0)))
end_time = timer()
print("Done in {0:.2f} seconds.".format(end_time - start_time))
