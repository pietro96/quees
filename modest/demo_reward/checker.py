# Requires Python 3.7 or newer
import sys
from importlib import util
import os
import random
import time

#def debug_get_reward(current_state, transition, reward_exps):
#	reward_exps_eval=reward_exps
#	network.jump_np(current_state, transition, reward_exps_eval)
#	return reward_exps_eval

def explore_neighbours(current_state, transitions, visited_states, states_queue, reward_exps = [], reward_dictionary={}):
	add2_queue = set()
	states_queue = set(states_queue)
	for transition in transitions:
		transition_reward_exps = reward_exps.copy()
		next_state = network.jump_np(current_state, transition, transition_reward_exps)
		add2_queue.add(next_state)
		reward_dictionary.update({next_state:transition_reward_exps})

	keys2remove = set(add2_queue.intersection(visited_states) | add2_queue.intersection(states_queue))
	list(map(reward_dictionary.pop, keys2remove))
	add2_queue = add2_queue - add2_queue.intersection(visited_states) - add2_queue.intersection(states_queue)
	return list(add2_queue)



def evaluate_neighbours(current_state, transitions, visited_states, property_id):
	add2_queue = set()
	for transition in transitions:
		next_state= network.jump_np(current_state, transition)
		property_check=network.get_expression_value(next_state, property_id)
		if(property_check):
			add2_queue.add(next_state)
	add2_queue = add2_queue - add2_queue.intersection(visited_states)
	return list(add2_queue)



#############################################################################
# Function name: has_loop
# Input param:
#	-) current_state: (type str) contains the current state
#	-) transitions: (type array) this array contains all the transitions from
#					current state
# Output: (type bool) it will return True if and only if I can loop over the 
#         same state
def has_loop(current_state, transitions):
	return current_state in transitions

#############################################################################



def eg_properties(properties, network, initial_state):
	egs=[]
	for prop in properties:
		if (prop.exp.op == "exists" and prop.exp.args[0].op == "always"):
			ap=int(prop.exp.args[0].args[0].args[0])
			evaluation_done = not network.get_expression_value(initial_state, ap)
			if (evaluation_done):
				result= False
			else:
				result= None

			property_dic = {
				"id": ap,
				"text": str(prop),
				"visited_states": set(),
				"current_state": initial_state,
				"trace": [],
				"all_trace": set(),
				"result": result,
				"evaluation_done": evaluation_done
			}
			egs = egs +[property_dic]

	return egs

def ef_properties(properties):
	pass

def rw_expressions(properties):
	rw_exps = []
	for prop in properties:
		is_reward = prop.exp is not None and prop.exp.op == "xmin" and prop.exp.args[1].op == "ap"
		if is_reward:
			rw_exps = rw_exps+[prop.exp.args[0]]
	return rw_exps


def reachable_states(initial_state, print_states=False):
	print("* Exploring reachable  states...")
	current_state=initial_state
	transitions = network.get_transitions(current_state)
	visited_states={current_state}
	current_state_id=1
	state_dictionary =	{
		1: current_state
	}
	reward_dictionary={}
	neighbours_reward_dictionary={}
	transition_dictionary ={}
	rtransition_dictionary={} # reverse transition dictionary
	reward_exps=rw_expressions(network.properties)
	neighbours=explore_neighbours(current_state, transitions, visited_states,[], reward_exps, neighbours_reward_dictionary)
	states_queue=neighbours
	max_state_id = 1 + len(neighbours)

	while len(states_queue)!=0:
		if(print_states):
			print("* The current state is:", str(current_state))

		for i in range(len(neighbours)):
			state_id=max_state_id-i
			state_dictionary[state_id]=neighbours[i]

			reward_dictionary[(current_state_id, state_id)] = neighbours_reward_dictionary[neighbours[i]]
			try:
				transition_dictionary[current_state_id].add(state_id)
			except:
				transition_dictionary[current_state_id]={state_id}

			try:
				rtransition_dictionary[state_id].add(current_state_id)
			except:
				rtransition_dictionary[state_id]={current_state_id}

		current_state = states_queue.pop()
		visited_states.add(current_state)
		current_state_id=current_state_id+1
		transitions = network.get_transitions(current_state)
		neighbours_reward_dictionary={}
		neighbours=explore_neighbours(current_state, transitions, visited_states, states_queue, reward_exps, neighbours_reward_dictionary)
#		print(neighbours_reward_dictionary)
		states_queue = neighbours + states_queue
		max_state_id = max_state_id + len(neighbours)

	return transition_dictionary, rtransition_dictionary, state_dictionary, reward_dictionary, visited_states



def check_egs(egs):
	print("* Checking E[] properties ...")
	stop_condition= True
	for prop in egs:
		stop_condition = stop_condition and prop['evaluation_done']

	while (not stop_condition):
		stop_condition = True
		for prop in egs:
			if (not prop['evaluation_done']):
				transitions = network.get_transitions(prop['current_state'])
				neighbours = evaluate_neighbours(prop['current_state'], transitions, prop['visited_states'], prop['id'])
				prop['visited_states'].add(prop['current_state'])
				if (len(neighbours)==0):
					if (len(prop['trace'])==0):
						prop['evaluation_done']=True
						prop['result']=False
					else:
						while (len(prop['trace'])!=0):
							i=len(prop['trace'])-1
							neighbours= evaluate_neighbours(prop['trace'][i], transitions, prop['visited_states'], prop['id'])
							if (len(neighbours)==0):
								prop['trace'].pop()
							else:
								prop['current_state']= random.choice(neighbours)
								break

						if (len(prop['trace'])==0):
							prop['evaluation_done']=True
							prop['result']=False
				else:
					for neighbour in neighbours:
						transitions=network.get_transitions(neighbour)
						hasLoop = has_loop(neighbour, transitions)
						graphEnd = explore_neighbours(neighbour, transitions, prop['visited_states'], [])
						if (len(transitions)==0 or hasLoop or len(graphEnd)==0):
							prop['evaluation_done']=True
							prop['trace'].insert(len(prop['trace']),prop['current_state'])
							prop['trace'].insert(len(prop['trace']),neighbour)
							prop['result']=True
							break
					if(not prop['evaluation_done']):
						prop['trace'].insert(len(prop['trace']),prop['current_state'])
						prop['current_state']= random.choice(neighbours)


				stop_condition = stop_condition and prop['evaluation_done'] # update stop conditions only for False

	return egs


def paths2state(state_id, rtransition_dictionary):
	paths=set()
	stop_condition=True
	for state in rtransition_dictionary[state_id]:
		paths.add([state])
		if(state!=1):
			stop_condition=False
	while not stop_condition:
		stop_condition=True
		paths_update=set()
		for path in paths:
			if (path[0]!=1):
				stop_condition=False
				current_state_id=path[0]
				back_states=rtransition_dictionary[current_state_id]
				for back_state in back_states:
					paths_update.add([back_state]+path)
		paths=paths_update
	return paths

############################ Run Modest Model and gen. graph #################################
# Define Modest installation
#modest = "/usr/bin/Modest-linux-x64-20191112/Modest/modest"  # 1st Version
#modest = "/usr/bin/Modest-linux-x64-20191203/Modest/modest"   # 2nd Version
#modest = "/usr/bin/Modest-linux-x64-20191205/Modest/modest"   # 3rd Version
modest = "/usr/bin/Modest-linux-x64-20191214/Modest/modest"   # 4th Version

modest_file=os.path.basename(sys.argv[1])
modest_file_name=os.path.splitext(modest_file)[0]
dot_file = modest_file_name+'.dot'
pdf_file = modest_file_name+'.pdf'
python_file=modest_file_name+'.py'

start_modest = time.time()
os.system(modest+" export-to-dot "+sys.argv[1]+" -O "+dot_file+" -Y --dot pdf "+pdf_file+" --open-output --no-time-progress")
os.system(modest+" export-to-python "+sys.argv[1]+" -O "+ python_file +" -Y")
end_modest = time.time()
#############################################################################################

################################## Start Load the model #####################################
if len(sys.argv) < 2:
	print("Error: No model specified.")
	quit()
print("Loading model from \"{0}\"...".format(python_file), end = "", flush = True)
spec = util.spec_from_file_location("model", python_file)
model = util.module_from_spec(spec)
spec.loader.exec_module(model)
network = model.Network() # create network instance
print(" done.")
################################## End Load the model #######################################


################################## Explore model properties #################################
properties=network.properties
initial_state=network.get_initial_state()


start_egs = time.time()
egs = eg_properties(properties, network, initial_state) # load all exists always property
egs = check_egs(egs)
end_egs = time.time()

print("* The model has", len(properties), "properties.")
print("* 	-) Exists always property (E[] or EG):", len(egs))
#############################################################################################


################################## Explore reachable states #################################
start_rstates= time.time()
transition_dictionary, rtransition_dictionary, state_dictionary, reward_dictionary, visited_states=reachable_states(initial_state)
end_rstates = time.time()
print ( "* visited states (dictionary): "+str(len(state_dictionary)) )
print ( "* visited states (set): "+str(len(visited_states)) )
print(reward_dictionary)
#############################################################################################



print("=========== Results =============")
for prop in egs:
	if(prop["evaluation_done"]):
		print("The property "+ prop["text"] + "is "+ str(prop["result"]))


################################## Timing ###################################################
print("=========== Timing =============")
print("Modest execution time (s):", end_modest - start_modest)
print("Reachable states explored in (s):", end_rstates - start_rstates)
print("E[] properties verified in (s):", end_egs - start_egs)
#############################################################################################