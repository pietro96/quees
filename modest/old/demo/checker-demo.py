# Run as python checker-demo.py model.py
# Requires Python 3.7 or newer

import sys
from importlib import util
from timeit import default_timer as timer

# Load the model
if len(sys.argv) < 2:
	print("Error: No model specified.")
	quit()
print("Loading model from \"{0}\"...".format(sys.argv[1]), end = "", flush = True)
spec = util.spec_from_file_location("model", sys.argv[1])
model = util.module_from_spec(spec)
spec.loader.exec_module(model)
network = model.Network() # create network instance
print(" done.")

print("* The model has", len(network.properties), "properties.")

print("* It is", str(network.properties[0]))

ap_index = network.properties[0].exp.args[0].args[0].args[0]
initial_state = network.get_initial_state()
print(network.get_expression_value(initial_state, ap_index))


print("* The initial state is:", str(initial_state))

transitions = network.get_transitions(initial_state)
print("* It has", len(transitions), "transitions.")

print("* It is labelled", network.transition_labels[transitions[0].label])
next_state = network.jump_np(initial_state, transitions[0])
print("* The next state is:", str(next_state))

transitions = network.get_transitions(next_state)
print("* It has", len(transitions), "transitions.")
print("* It is labelled", network.transition_labels[transitions[0].label])
i=0


visited_states=[]
print(network.properties[0].exp)
print(network.properties[0].exp.op)
print(network.properties[0].exp.args[0].op)
print(network.properties[0].exp.args[0].args[0].op)
print(network.properties[0])


print("* The first property is", "" if network.properties[0].exp is not None and network.properties[0].exp.op == "exists" and network.properties[0].exp.args[0].op == "eventually" and network.properties[0].exp.args[0].args[0].op == "ap" else " not", " a reachability property.", sep = "")


while i in range(0,80):
	next_state = network.jump_np(next_state, transitions[0])
	if str(next_state) in visited_states:
		print("The states was alweady visited!!")

	visited_states=[str(next_state)]+visited_states
	print("* The next state is:", str(next_state))
	transitions = network.get_transitions(next_state)
	print(len(transitions))
	i=i+1



sys.exit(0)


print(next_state.did_c)

# Print some information about the model and the initial state
start_time = timer()
print("* The model has", str(len(network.properties)), "properties.")
print("* The first property is:", str(network.properties[0]))

initial_state = network.get_initial_state()
print("* The initial state is:", str(initial_state))
initial_state_transitions = network.get_transitions(initial_state)
print("* The value of ap(0) in the initial state is:", str(network.get_expression_value(initial_state, 0)))
print("* The initial state has", str(len(initial_state_transitions)), "transitions.")
print("* The first transition out of the initial state is labelled:", network.transition_labels[initial_state_transitions[0].label])
next_state = network.jump_np(initial_state, initial_state_transitions[0])
print("* The transition leads to state:", str(next_state))
print("* The value of ap(0) in that state is:", str(network.get_expression_value(initial_state, 0)))
end_time = timer()
print("Done in {0:.2f} seconds.".format(end_time - start_time))
