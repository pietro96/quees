# Requires Python 3.7 or newer
import sys
from importlib import util
import os


def explore_neighbours(current_state, transitions, visited_states, states):
	i=0
	states_array=[]
	for i in range(0,len(transitions)):
		next_state = network.jump_np(current_state, transitions[i])
		if (str(next_state) not in visited_states and next_state not in states):
			states_array= states_array + [next_state]
		i=i+1

	return states_array 


def propertyChecked(properties):
	pass

def counterExample():
	pass

#################################### Run Modest Model #######################################
modest = "/usr/bin/Modest-linux-x64-20191112/Modest/modest"
modest_file=os.path.basename(sys.argv[1])
modest_file_name=os.path.splitext(modest_file)[0]
python_file=modest_file_name+'.py'
os.system(modest+" export-to-python "+sys.argv[1]+" -O "+ python_file +" -Y")

#############################################################################################

################################## Start Load the model #####################################
if len(sys.argv) < 2:
	print("Error: No model specified.")
	quit()
print("Loading model from \"{0}\"...".format(python_file), end = "", flush = True)
spec = util.spec_from_file_location("model", python_file)
model = util.module_from_spec(spec)
spec.loader.exec_module(model)
network = model.Network() # create network instance
print(" done.")
################################## End Load the model #######################################


################################## Explore model properties #################################
print("* The model has", len(network.properties), "properties.")

for prop in network.properties:
	print(prop.exp.op)
	print(prop.exp.args[0].op)

#############################################################################################


################################## Explore reachable states #################################
initial_state=network.get_initial_state()
current_state=initial_state

transitions = network.get_transitions(current_state)
visited_states=[initial_state]
states=explore_neighbours(current_state, transitions, visited_states, [])

print("* The current state is:", str(current_state))
while len(states)!=0:
	current_state = states.pop()
	print("* The current state is:", str(current_state))
	visited_states=[str(current_state)]+visited_states
	transitions = network.get_transitions(current_state)
	neighbours=explore_neighbours(current_state, transitions, visited_states, states)
	states = neighbours + states

#############################################################################################