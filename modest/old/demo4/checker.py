# Requires Python 3.7 or newer
import sys
from importlib import util
import os
import random
import time

def explore_neighbours(current_state, transitions, visited_states, states_queue):
	add2_queue = set()
	states_queue = set(states_queue)
	for transition in transitions:
		next_state = network.jump_np(current_state, transition)
		add2_queue.add(next_state)

	add2_queue = add2_queue - add2_queue.intersection(visited_states) - add2_queue.intersection(states_queue)
	return list(add2_queue)



def evaluate_neighbours(current_state, transitions, visited_states, property_id):
	add2_queue = set()
	for transition in transitions:
		next_state= network.jump_np(current_state, transition)
		property_check=network.get_expression_value(next_state, property_id)
		if(property_check):
			add2_queue.add(next_state)
	add2_queue = add2_queue - add2_queue.intersection(visited_states)
	return list(add2_queue)



#############################################################################
# Function name: has_loop
# Input param:
#	-) current_state: (type str) contains the current state
#	-) transitions: (type array) this array contains all the transitions from
#					current state
# Output: (type bool) it will return True if and only if I can loop over the 
#         same state
def has_loop(current_state, transitions):
	return current_state in transitions

#############################################################################



def eg_properties(properties, network, initial_state):
	egs=[]
	for prop in properties:
		if (prop.exp.op == "exists" and prop.exp.args[0].op == "always"):
			ap=int(prop.exp.args[0].args[0].args[0])
			evaluation_done = not network.get_expression_value(initial_state, ap)
			if (evaluation_done):
				result= False
			else:
				result= None

			property_dic = {
				"id": ap,
				"text": str(prop),
				"visited_states": set(),
				"current_state": initial_state,
				"trace": [],
				"result": result,
				"evaluation_done": evaluation_done
			}
			egs = egs +[property_dic]

	return egs

def ef_properties(properties):
	pass



def reachable_states(initial_state):
	current_state=initial_state
	transitions = network.get_transitions(current_state)
	visited_states={initial_state}
	states_queue=explore_neighbours(current_state, transitions, visited_states, [])
	print("* The current state is:", str(current_state))
	while len(states_queue)!=0:
		current_state = states_queue.pop()
		print("* The current state is:", str(current_state))
		visited_states.add(current_state)
		transitions = network.get_transitions(current_state)
		neighbours=explore_neighbours(current_state, transitions, visited_states, states_queue)
		states_queue = neighbours + states_queue
	return visited_states




def check_egs(egs):
	print("* Checking E[] properties ...")
	stop_condition= True
	for prop in egs:
		stop_condition = stop_condition and prop['evaluation_done']

	while (not stop_condition):
		stop_condition = True
		for prop in egs:
			if (not prop['evaluation_done']):
				transitions = network.get_transitions(prop['current_state'])
				neighbours = evaluate_neighbours(prop['current_state'], transitions, prop['visited_states'], prop['id'])
				prop['visited_states'].add(prop['current_state'])
				if (len(neighbours)==0):
					if (len(prop['trace'])==0):
						prop['evaluation_done']=True
						prop['result']=False
					else:
						while (len(prop['trace'])!=0):
							i=len(prop['trace'])-1
							neighbours= evaluate_neighbours(prop['trace'][i], transitions, prop['visited_states'], prop['id'])
							if (len(neighbours)==0):
								prop['trace'].pop()
							else:
								prop['current_state']= random.choice(neighbours)
								break

						if (len(prop['trace'])==0):
							prop['evaluation_done']=True
							prop['result']=False
				else:
					for neighbour in neighbours:
						transitions=network.get_transitions(neighbour)
						hasLoop = has_loop(neighbour, transitions)
						graphEnd = explore_neighbours(neighbour, transitions, prop['visited_states'], [])
						if (len(transitions)==0 or hasLoop or len(graphEnd)==0):
							prop['evaluation_done']=True
							prop['trace'].insert(len(prop['trace']),prop['current_state'])
							prop['trace'].insert(len(prop['trace']),neighbour)
							prop['result']=True
							break
					if(not prop['evaluation_done']):
						prop['trace'].insert(len(prop['trace']),prop['current_state'])
						prop['current_state']= random.choice(neighbours)


				stop_condition = stop_condition and prop['evaluation_done'] # update stop conditions only for False

	return egs




############################ Run Modest Model and gen. graph #################################
modest = "/usr/bin/Modest-linux-x64-20191112/Modest/modest"
modest_file=os.path.basename(sys.argv[1])
modest_file_name=os.path.splitext(modest_file)[0]
dot_file = modest_file_name+'.dot'
pdf_file = modest_file_name+'.pdf'
python_file=modest_file_name+'.py'

start_modest = time.time()
os.system(modest+" export-to-dot "+sys.argv[1]+" -O "+dot_file+" -Y --dot pdf "+pdf_file+" --open-output --no-time-progress")
os.system(modest+" export-to-python "+sys.argv[1]+" -O "+ python_file +" -Y")
end_modest = time.time()
#############################################################################################

################################## Start Load the model #####################################
if len(sys.argv) < 2:
	print("Error: No model specified.")
	quit()
print("Loading model from \"{0}\"...".format(python_file), end = "", flush = True)
spec = util.spec_from_file_location("model", python_file)
model = util.module_from_spec(spec)
spec.loader.exec_module(model)
network = model.Network() # create network instance
print(" done.")
################################## End Load the model #######################################


################################## Explore model properties #################################
properties=network.properties
initial_state=network.get_initial_state()


start_egs = time.time()
egs = eg_properties(properties, network, initial_state) # load all exists always property
egs = check_egs(egs)
end_egs = time.time()

print("* The model has", len(properties), "properties.")
print("* 	-) Exists always property (E[] or EG):", len(egs))
#############################################################################################


################################## Explore reachable states #################################
start_rstates= time.time()
reachable_states=reachable_states(initial_state)
end_rstates = time.time()
print ( "* visited states: "+str(len(reachable_states)) )
#############################################################################################



print("=========== Results =============")
for prop in egs:
	if(prop["evaluation_done"]):
		print("The property "+ prop["text"] + "is "+ str(prop["result"]))


################################## Timing ###################################################
print("=========== Timing =============")
print("Modest execution time (s):", end_modest - start_modest)
print("Reachable states explored in (s):", end_rstates - start_rstates)
print("E[] properties verified in (s):", end_egs - start_egs)
#############################################################################################