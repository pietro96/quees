def check_efs(efs, initial_state):
	current_state=initial_state
	transitions = network.get_transitions(current_state)
	visited_states={initial_state}
	states_queue=explore_neighbours(current_state, transitions, visited_states, [])
	stop_condition = False
	while (len(states_queue)!=0 and not stop_condition):
		current_state = states_queue.pop()
		###################### check efs properties ############
		stop_condition = True
		for prop in efs:
			if (not prop['evaluation_done']):
				if(network.get_expression_value(current_state, prop["id"])):
					prop['evaluation_done']= True
					prop['result']= True
					prop['stateWhereItHolds'] = current_state
				stop_condition = stop_condition and prop['evaluation_done']
		########################################################
		visited_states.add(current_state)
		transitions = network.get_transitions(current_state)
		neighbours=explore_neighbours(current_state, transitions, visited_states, states_queue)
		states_queue = neighbours + states_queue

	for prop in efs:
		if (not prop['evaluation_done']):
			prop['evaluation_done']= True
			prop['result']= False