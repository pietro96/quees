import json

def mydemo(p):
	for prop in p:
		prop['id']=53
		prop['trace'].insert(len(prop['trace']),1)
		prop['trace'].insert(len(prop['trace']),300)
	return p


x = 10
def globallyChange():
	global x            # Access the global var
	x = "didn't work"
globallyChange()        # Call the function
print(x)


initial_state=""
# a Python object (dict):
property_dic0 = {
  "id": 0,
  "visited_states": [],
  "current_state": initial_state,
  "trace": [],
  "result": 0,
  "evaluation_done": 0
}


i=0
p=[]
while i<=2:
	elem = {
	  "id": 0,
	  "visited_states": [],
	  "current_state": initial_state,
	  "trace": [],
	  "result": 0,
	  "evaluation_done": 0
	}
	p=p+[elem]
	i=i+1
	

print('------------ Before --------------------')
print (p[0]['id'])
print(p[0]['trace'])

p=mydemo(p)

print('------------ After --------------------')
print (p[0]['id'])
print(p[0]['trace'])


print('------------ demo --------------------')
print (p['id'])


#y = json.dumps(x) # convert into JSON
#y = json.loads(y) # load JSON
#print(y['name'])
#print(y['lang'])
