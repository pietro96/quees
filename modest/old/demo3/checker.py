# Requires Python 3.7 or newer
import sys
from importlib import util
import os
import random


def explore_neighbours(current_state, transitions, visited_states, states):
	i=0
	states_array=[]
	for i in range(0,len(transitions)):
		next_state = network.jump_np(current_state, transitions[i])
		if (next_state not in visited_states and next_state not in states):
			states_array= states_array + [next_state]
		i=i+1

	return states_array 

def evaluate_neighbours(current_state, transitions, visited_states, property_id):
	states_array=[]
	for transition in transitions:
		next_state= network.jump_np(current_state, transition)
		property_check=network.get_expression_value(next_state, property_id)
		if(next_state not in visited_states and property_check):
			states_array= states_array + [next_state]
	return states_array


def has_loop(current_state, transitions):
	if(current_state in transitions):
		return True
	else:
		return False


def eg_properties(properties, network, initial_state):
	efs=[]
	for prop in properties:
		if (prop.exp.op == "exists" and prop.exp.args[0].op == "always"):
			ap=int(prop.exp.args[0].args[0].args[0])
			evaluation_done = not network.get_expression_value(initial_state, ap)
			property_dic = {
				"id": ap,
				"text": str(prop),
				"visited_states": [],
				"current_state": initial_state,
				"trace": [],
				"result": None,
				"evaluation_done": evaluation_done
			}
			efs = efs +[property_dic]

	return efs

def ef_properties(properties):
	pass



def reachable_states(initial_state, network):
	current_state=initial_state
	transitions = network.get_transitions(current_state)
	visited_states=[initial_state]
	states=explore_neighbours(current_state, transitions, visited_states, [])

	print("* The current state is:", str(current_state))
	while len(states)!=0:
		current_state = states.pop()
		print("* The current state is:", str(current_state))
		visited_states=[current_state]+visited_states
		transitions = network.get_transitions(current_state)
		neighbours=explore_neighbours(current_state, transitions, visited_states, states)
		states = neighbours + states
	return visited_states


def check_egs(egs):
	stop_condition= True
	for prop in egs:
		stop_condition = stop_condition and prop['evaluation_done']

	while (not stop_condition):
		stop_condition = True
		for prop in egs:
			if (not prop['evaluation_done']):
				transitions = network.get_transitions(prop['current_state'])
				neighbours = evaluate_neighbours(prop['current_state'], transitions, prop['visited_states'], prop['id'])
				prop['visited_states']= prop['visited_states'] + [prop['current_state']]
				if (len(neighbours)==0):
					if (len(prop['trace'])==0):
						prop['evaluation_done']=True
						prop['result']=False
					else:
						i=len(prop['trace'])-1
						while i>=0:
							neighbours= evaluate_neighbours(prop['trace'][i], transitions, prop['visited_states'], prop['id'])
							if (len(neighbours)==0):
								prop['trace'].pop()
							else:
								prop['current_state']= random.choice(neighbours)
								break
							i=i-1

						if (len(prop['trace'])==0):
							prop['evaluation_done']=True
							prop['result']=False
				else:
					for neighbour in neighbours:
						transitions=network.get_transitions(neighbour)
						hasLoop = has_loop(neighbour, transitions)
						graphEnd = explore_neighbours(neighbour, transitions, prop['visited_states'], [])
						if (len(transitions)==0 or hasLoop or len(graphEnd)==0):
							prop['evaluation_done']=True
							prop['trace'].insert(len(prop['trace']),prop['current_state'])
							prop['trace'].insert(len(prop['trace']),neighbour)
							prop['result']=True
							break
					if(not prop['evaluation_done']):
						prop['trace'].insert(len(prop['trace']),prop['current_state'])
						prop['current_state']= random.choice(neighbours)


				stop_condition = stop_condition and prop['evaluation_done'] # update stop conditions only for False

	return egs

############################ Run Modest Model and gen. graph #################################
modest = "/usr/bin/Modest-linux-x64-20191112/Modest/modest"
modest_file=os.path.basename(sys.argv[1])
modest_file_name=os.path.splitext(modest_file)[0]
dot_file = modest_file_name+'.dot'
pdf_file = modest_file_name+'.pdf'
python_file=modest_file_name+'.py'

os.system(modest+" export-to-dot "+sys.argv[1]+" -O "+dot_file+" -Y --dot pdf "+pdf_file+" --open-output --no-time-progress")
os.system(modest+" export-to-python "+sys.argv[1]+" -O "+ python_file +" -Y")
#############################################################################################

################################## Start Load the model #####################################
if len(sys.argv) < 2:
	print("Error: No model specified.")
	quit()
print("Loading model from \"{0}\"...".format(python_file), end = "", flush = True)
spec = util.spec_from_file_location("model", python_file)
model = util.module_from_spec(spec)
spec.loader.exec_module(model)
network = model.Network() # create network instance
print(" done.")
################################## End Load the model #######################################


################################## Explore model properties #################################
properties=network.properties
initial_state=network.get_initial_state()
egs = eg_properties(properties, network, initial_state) # load all exists always property

print("* The model has", len(properties), "properties.")
print("* 	-) Exists always property (E[] or EG):", len(egs))

egs = check_egs(egs)

print("===========Results=============")

for prop in egs:
	if(prop["evaluation_done"]):
		print("The property "+ prop["text"] + "is "+ str(prop["result"]))

#############################################################################################


################################## Explore reachable states #################################
#reachable_states=reachable_states(initial_state, network)
#print ( "* visited states: "+str(len(reachable_states)) )
#############################################################################################


