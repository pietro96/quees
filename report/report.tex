\documentclass{article}
\usepackage[english]{babel}
\usepackage{listings}
\lstset{
	basicstyle=\ttfamily,
	columns=fullflexible,
	frame=single,
	breaklines=true,
}

\usepackage{rotating}
\usepackage[utf8]{inputenc}
\usepackage{fancyhdr}
\usepackage{graphicx}
\usepackage{float}
\usepackage{amsthm}
\usepackage{amsmath}
\usepackage{xcolor}
%\usepackage{tcolorbox}
\usepackage{listings}
\pagestyle{fancy}
\fancyhf{}
\rhead{Assignment Report}
\lhead{QEES}
\rfoot{\thepage}
\title{QEES}
\author{ Pietro Pennestrì}

\definecolor{mygray}{RGB}{210,210,210}


\providecommand{\modest}{\texttt{MODEST }}
\providecommand{\py}{\texttt{Python }}
\providecommand{\sat}{\texttt{Nano Satellite }}

\providecommand{\uhf}{\texttt{UHF }}
\providecommand{\lband}{\texttt{L-BAND }}
\providecommand{\xband}{\texttt{X-BAND }}





\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}

\lstdefinestyle{mystyle}{
	backgroundcolor=\color{backcolour},   
	commentstyle=\color{codegreen},
	keywordstyle=\color{magenta},
	numberstyle=\tiny\color{codegray},
	stringstyle=\color{codepurple},
	basicstyle=\ttfamily\footnotesize,
	breakatwhitespace=false,         
	breaklines=true,                 
	captionpos=b,                    
	keepspaces=true,                 
	numbers=left,                    
	numbersep=5pt,                  
	showspaces=false,                
	showstringspaces=false,
	showtabs=false,                  
	tabsize=2
}

\lstset{style=mystyle}

%\usepackage{fontawesome}
\begin{document}
\maketitle
\tableofcontents
\section{Part 1}

\newpage

\section{Part2}


\noindent\fcolorbox{white}{mygray}{%
	\begin{minipage}{\dimexpr \textwidth-2 \fboxrule-2 \fboxsep\relax}
		\textbf{Key Idea} \\
		Create a \py script that is able to atomatically generate a \modest model that can simulate all possible behaviors for the tasks scheduled for 1 single day. Finally, use the model checker developed in Part 1 is used to deduce a scheduler for the selected day.
\end{minipage}}%

\subsection{Preliminary Hypothesis}
The following hypothesis are considered true throughout all the development process:
\begin{itemize}
	\item The \sat can execute one job a time.
	\item All the \uhf jobs must be executed. This means that the jobs that overlap with the \uhf ones
	are removed directly by the \py script and will not appear in the \modest model.
	\item When \lband jobs are executed the pre-heat and slewing time are included in the duration of the jobs.
	\item When an \lband job is terminated, the \sat is back to its original orbit altitude.
	\item During the \lband jobs the power of recharge is considered constant to 6100 mW.
\end{itemize}  

\subsection{Data Preprocessing}
%2016/03/20 08:02:17.949

For a single day all available jobs, that \textbf{do not overlap with \uhf ones} are collected in a \texttt{csv} file (\texttt{YEAR\_MONTH\_DAY.csv}). For each of these job the following quantities need to be computed in order to generate the \modest template:




%Before the \modest model can be generated the following quantities need to be computed\footnote{The data provided with the assignment were elaborated to uniform the time standard as \texttt{YEAR/MONTH/DAY HOURS24:MINUTES:SECONDS.FLOAT} }:

\begin{itemize}
	\item Relative start and stop time are computed for all the jobs. The beginning of the time is assumed to be the start time of the first available job for the selected day.
	\item The total amount of time that the \sat has been exposed to sun during a job.
	\item The total amount of time that the \sat has been exposed to sun till the beginning of the job.
	\item The total amount of time that the \sat has been exposed to sun till the end of the job.
	\item The total energy require by a job to be executed (the recharge due to insulation is taken into consideration).The \py function that computes the energy for a job is shown in Listing \ref{lst:energy_python}. 
	\item A \texttt{skip\_if} set that contains all the previous job \texttt{ID} that overlap with the current one. Despite this set is not directly used in the \modest model, it is of great help while checking the obtained scheduler.
\end{itemize}


An example of the generated \texttt{CSV} is shown in Figure \ref{fig:csv}.

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{figs/csv}
	\caption{Example of a generated \texttt{csv} file}
	\label{fig:csv}
\end{figure}


\begin{lstlisting}[language=python, label={lst:energy_python} , caption=Function to compute energy required by a job]
def jobEnergy(job_type, start, stop, sun_start, sun_stop):
	#all the times are in [s], the returned energy is in [mJ]
	global GLOBAL_BPOWER  
	global GLOBAL_UHF_POWER  
	global GLOBAL_XBAND_POWER 
	global GLOBAL_LBAND_POWER  
	global GLOBAL_SLEW_POWER  
	global GLOBAL_PREHEAT_POWER   
	global GLOBAL_XBAND_RPOWER  
	global GLOBAL_LBAND_RPOWER 
	global GLOBAL_RPOWER 
	delta_sun = sun_stop - sun_start
	delta = stop - start
	
	if(job_type == "lband3" or job_type == "lband2"):
		return round(GLOBAL_LBAND_POWER*delta + GLOBAL_BPOWER*delta - GLOBAL_LBAND_RPOWER*delta_sun + (20*60)*GLOBAL_SLEW_POWER + (20*60)*GLOBAL_PREHEAT_POWER) 

	if(job_type =="xband"):
		return round(GLOBAL_XBAND_POWER*delta + GLOBAL_BPOWER*delta - GLOBAL_XBAND_RPOWER*delta_sun)

	if(job_type =="uhf") :
		return round(GLOBAL_UHF_POWER*delta + GLOBAL_BPOWER*delta - GLOBAL_RPOWER*delta_sun)
\end{lstlisting}
\subsection{Modest Template}


The idea behind the \modest model is to have a basic building block that represents the execution or the skipping of a job. The template for this building block is model in \modest as shown in Listing \ref{lst:jobTemplate}. In this template the keyword between \texttt{\#} (i.e. \texttt{\#start\_job\#}), will be automatically substituted by their corresponding value by the \py script (\texttt{scheduler.py}):

\begin{itemize}
	\item \texttt{\#job\_id\#}: the ID of the job;
	\item \texttt{\#start\_job\#}: the relative start time of the job;
	\item \texttt{\#end\_job\#}: the relative end time of the job;
	\item \texttt{\#sun\_start\_job\#}: the total time that the \sat has been exposed to to sun till the beginning of the job;
	\item \texttt{\#sun\_end\_job\#}:  the total time that the \sat has been exposed to to sun till the end of the job;
	\item \texttt{\#jobenergy\#}: energy required by the job (the recharge due to insulation is take into account).
	\item \texttt{\#can\_skip\#}: condition if a job can be skipped. This tag is substituted with \texttt{False} for \uhf jobs, while with \texttt{True} for all other cases.
\end{itemize}

\noindent Our model does not any contain any clock, but we keep track of time by two variables:
\begin{itemize}
	\item \texttt{t}: the time elapsed from the beginning of the day (the start time of the first available job)
	\item \texttt{ts}: the total time that the \sat has been exposed to sun from the beginning of the day.
\end{itemize}
Variables \texttt{t} and \texttt{ts} are defined in the header template shown Listing \ref{lst:header}.
In the following we will briefly describe how the template for a single job works:

\begin{itemize}
	\item \textbf{The Job is executed}
	\begin{itemize}
		\item[] \textbf{Guards for} \texttt{doJob}: The job can be executed if and on if the current time is less or equal then the start time of job and the current battery level minus the energy required for the job is greater or equal than 45\% of the total battery capacity (for security reason an additional 5\% margin was introduced).
		 
		
		\item[] \textbf{Update for} \texttt{doJob}: \texttt{t} and \texttt{ts} are respectively updated to \texttt{\#start\_job\#} and \texttt{\#sun\_start\_job\#}. The battery level is updated to take account the battery consumption between \texttt{t} and \texttt{\#start\_job\#}. The new battery level is computed with the formula: \\ \texttt{battery = min(max\_battery, battery - ( \#jobenergy\# ))} 
		
		\item[] \textbf{Update for} \texttt{endJob}: \texttt{t} and \texttt{ts} are respectively updated to \texttt{\#start\_job\#} and \texttt{\#sun\_start\_job\#}. The battery level is updated according to the formula \texttt{battery = min(max\_battery, battery - ( \#jobenergy\# ))}. Finally the variable \texttt{jobDone}, which keeps tracks of the jobs executed so far is increased. 
		
	\end{itemize}
	\item  \textbf{The Job is skipped}
	\begin{itemize}
		\item \textbf{Guards}: Check if the job is not of \texttt{UHF} type. 
		\item \textbf{Update for} \texttt{skipJob}: No variable is update.
		\item \textbf{Update for} \texttt{endSkipJob}:  \texttt{t} and \texttt{ts} are respectively updated to \texttt{ max(t, \#next\_start\_job\#) } and \texttt{max(ts, \#next\_sun\_start\_job\#)}. The battery level is updated with the the following formulas:
		
		\begin{quote}
			\texttt{battery = min(max\_battery ,battery - bpower*( max(t, \#next\_start\_job\#) - t) + rpower*( max(\#next\_sun\_start\_job\#,ts) -ts))}
		\end{quote}
		
	\end{itemize}
\end{itemize}

When the last job is executed or skipped the boolean variable \texttt{endOfday} is set to \texttt{True}.

\begin{lstlisting}[language=C, label={lst:header} ,caption=Header Template]
action doJob, endJob, skipJob, endSkipJob;

const int bpower = #GLOBAL_BPOWER# ;
const int rpower = #GLOBAL_RPOWER# ;
const int max_battery = #MAX_BATTERY_CAPACITY# ;
const int min_battery = #MIN_BATTERY_CAPACITY# ; 

bool endOfday= false;

int battery = #START_BATTERY_CAPACITY# ;
int t = 0; // time elapsed
int ts = 0; // time exposed to sun

int IDofLastJob = -1;
int jobDone =0;
\end{lstlisting}

\begin{lstlisting}[language=C, label={lst:jobTemplate} ,caption=Job Template]
// Job ID: #job_id#
do {
:: when(t <= #start_job# && battery - ( #jobenergy# ) >=  1.13*min_battery) doJob {= t = #start_job#, ts = #sun_start_job# , battery = min(max_battery, battery - bpower*(#start_job# - t) + rpower*(#sun_start_job# - ts)) =}; // between two jobs bpower and rpower should be taken into account
endJob {= t = #end_job# , ts = #sun_end_job#  , battery = min(max_battery, battery - ( #jobenergy# )), IDofLastJob =#job_id# , jobDone = jobDone +1 =};
break
:: when( (battery < max_battery ||  battery - ( #jobenergy# )  <  1.13*min_battery   || t > #start_job# ) && #can_skip# && IDofLastJob != #job_id#) skipJob ;
endSkipJob {= t = max(t, #next_start_job#) , ts = max(ts, #next_sun_start_job#) , battery = min(max_battery ,battery - bpower*( max(t, #next_start_job#) - t) + rpower*( max(#next_sun_start_job#,ts) -ts)) =};
break
};
\end{lstlisting}



\begin{lstlisting}[language=C, caption=Last Job Template]
// Job ID: #job_id#, This is the last Job
do {
:: when(t <= #start_job# && battery - ( #jobenergy# ) - bpower*(#start_job# - t) + rpower*( #sun_start_job# - ts) >= min_battery) doJob {= t = #start_job#, ts = #sun_start_job# , battery = min(max_battery, battery - bpower*(#start_job# - t) + rpower*(#sun_start_job# - ts)) =}; // between two jobs bpower and rpower should be taken into account
endJob {= t = #end_job# , ts = #sun_end_job#  , battery = min(max_battery, battery -( #jobenergy# ) ) , IDofLastJob =#job_id# , jobDone = jobDone +1, endOfday= true=};
break
:: when( (battery < 0.8*max_battery || battery - ( #jobenergy# ) - bpower*(#start_job# - t) + rpower*( #sun_start_job# - ts) < min_battery ||t > #start_job# ) && #can_skip# && IDofLastJob != #job_id# ) skipJob ;
endSkipJob {= t = max(t, #end_job#) , ts = max(ts, #sun_end_job#) , battery = min(max_battery, battery - bpower*( max(#end_job# , t) - t) + rpower*( max(#sun_end_job#,ts) -ts)) ,endOfday= true=};
break
}
\end{lstlisting}

\subsection{Model Checking }
Tanks to the model checker developed in part one from the \modest model we are able to gain the following information:
\begin{enumerate}
	\item End states (state where the transition array is empty) are selected. These states are returned by the function \texttt{reachable\_states} defined in our model checker.
	\item From the end states which have the \texttt{endOfday=True} the one with maximum job executed (max \texttt{jobDone}) is taken.
	
	\item By means of the function \texttt{paths2state} the different paths to the selected end state are explored (in most of the cases there is only one path). From these paths the first one is selected. All the states contained in this path are checked to ensure that the battery level never goes below the given limit. If this happen the selected end state is removed and steps 1 and 2 are repeated.  
	
	\item If step 3 terminates with success, jobs ID contained in the selected path are returned. This allows us to generate the scheduler depicted in Figure \ref{fig:thescheduler}.
	
	
\end{enumerate} 


\subsection{Final Results}

\begin{figure}[H]
	\centering
	\includegraphics{figs/2016_3_20Graph}
	\caption{The Scheduler}
	\label{fig:thescheduler}
\end{figure}


\begin{figure}[H]
	\centering
	\includegraphics{figs/energy}
	\caption{Energy Plot}
\end{figure}




The following 7 jobs are impossible to schedule because they overlap with UHF jobs!
22.58 \% of requested\textbf{ jobs removed!}


\begin{itemize}
\item \texttt{lband3,2016/03/20 20:22:46.235,2016/03/20 21:55:36.235,5570.0}
\item \texttt{lband2,2016/03/20 21:07:29.512,2016/03/20 22:40:19.512,5570.0}
\item \texttt{xband,2016/03/20 21:49:52.580,2016/03/20 21:57:05.565,432.985}
\item \texttt{lband3,2016/03/20 21:58:10.674,2016/03/20 23:31:00.674,5570.0}
\item \texttt{lband2,2016/03/20 22:42:52.170,2016/03/21 00:15:42.170,5570.0}
\item \texttt{xband,2016/03/20 23:23:22.747,2016/03/20 23:33:23.408,600.661}
\item \texttt{lband3,2016/03/20 23:34:05.030,2016/03/21 01:06:55.030,5570.0}
\end{itemize}

\begin{figure}
	\includegraphics[width=\textwidth]{figs/GreenLight}
	\caption{Scheduler validation by means of SimuLink}
\end{figure}


\end{document}