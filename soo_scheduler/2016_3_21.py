# 2016_3_21

from __future__ import annotations
from typing import List, Union, Optional

class VariableInfo(object):
	__slots__ = ("name", "component", "type", "minValue", "maxValue")
	
	def __init__(self, name: str, component: Optional[int], type: str, minValue = None, maxValue = None):
		self.name = name
		self.component = component
		self.type = type
		self.minValue = minValue
		self.maxValue = maxValue

# States
class State(object):
	__slots__ = ("endOfday", "battery", "t", "ts", "IDofLastJob", "jobDone", "Main_location")
	
	def get_variable_value(self, variable: int):
		if variable == 0:
			return self.endOfday
		elif variable == 1:
			return self.battery
		elif variable == 2:
			return self.t
		elif variable == 3:
			return self.ts
		elif variable == 4:
			return self.IDofLastJob
		elif variable == 5:
			return self.jobDone
		elif variable == 6:
			return self.Main_location
	
	def copy_to(self, other: State):
		other.endOfday = self.endOfday
		other.battery = self.battery
		other.t = self.t
		other.ts = self.ts
		other.IDofLastJob = self.IDofLastJob
		other.jobDone = self.jobDone
		other.Main_location = self.Main_location
	
	def __eq__(self, other):
		return isinstance(other, self.__class__) and self.endOfday == other.endOfday and self.battery == other.battery and self.t == other.t and self.ts == other.ts and self.IDofLastJob == other.IDofLastJob and self.jobDone == other.jobDone and self.Main_location == other.Main_location
	
	def __ne__(self, other):
		return not self.__eq__(other)
	
	def __hash__(self):
		result = 75619
		result = (((101 * result) & 0xFFFFFFFF) + hash(self.endOfday)) & 0xFFFFFFFF
		result = (((101 * result) & 0xFFFFFFFF) + hash(self.battery)) & 0xFFFFFFFF
		result = (((101 * result) & 0xFFFFFFFF) + hash(self.t)) & 0xFFFFFFFF
		result = (((101 * result) & 0xFFFFFFFF) + hash(self.ts)) & 0xFFFFFFFF
		result = (((101 * result) & 0xFFFFFFFF) + hash(self.IDofLastJob)) & 0xFFFFFFFF
		result = (((101 * result) & 0xFFFFFFFF) + hash(self.jobDone)) & 0xFFFFFFFF
		result = (((101 * result) & 0xFFFFFFFF) + hash(self.Main_location)) & 0xFFFFFFFF
		return result
	
	def __str__(self):
		result = "("
		result += "endOfday = " + str(self.endOfday)
		result += ", battery = " + str(self.battery)
		result += ", t = " + str(self.t)
		result += ", ts = " + str(self.ts)
		result += ", IDofLastJob = " + str(self.IDofLastJob)
		result += ", jobDone = " + str(self.jobDone)
		result += ", Main_location = " + str(self.Main_location)
		result += ")"
		return result

# Transients
class Transient(object):
	__slots__ = ()
	
	def copy_to(self, other: Transient):
		pass
	
	def __eq__(self, other):
		return isinstance(other, self.__class__)
	
	def __ne__(self, other):
		return not self.__eq__(other)
	
	def __hash__(self):
		result = 75619
		return result
	
	def __str__(self):
		result = "("
		result += ")"
		return result

# Automaton: Main
class MainAutomaton(object):
	__slots__ = ("network", "transition_counts", "transition_labels", "branch_counts")
	
	def __init__(self, network: Network):
		self.network = network
		self.transition_counts = [2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 2, 1, 1, 2, 1, 1, 2, 1, 1, 2, 1, 1, 2, 1, 1, 2, 1, 1, 2, 1, 1, 2, 1, 1, 2, 1, 1, 2, 1, 1, 2, 1, 1, 2, 1, 1, 2, 1, 1, 2, 1, 1, 2, 1, 1, 2, 1, 1, 2, 1, 1, 2, 1, 1, 2, 1, 1, 2, 1, 1, 2, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
		self.transition_labels = [[1, 2], [3], [0], [1], [3], [0], [1], [3], [0], [1], [3], [0], [1], [3], [0], [1, 2], [3], [0], [1, 2], [3], [0], [1, 2], [3], [0], [1, 2], [3], [0], [1, 2], [3], [0], [1, 2], [3], [0], [1, 2], [3], [0], [1, 2], [3], [0], [1, 2], [3], [0], [1, 2], [3], [0], [1, 2], [3], [0], [1, 2], [3], [0], [1, 2], [3], [0], [1, 2], [3], [0], [1, 2], [3], [0], [1, 2], [3], [0], [1, 2], [3], [0], [1, 2], [3], [0], [1, 2], [3], [0], [1, 2], [3], [0], [1, 2], [3], [0], [1, 2], [3], [0], [1], [3], [0], [1, 2], [3], [0], [1], [3], [0], [], [4], [4], [4], [4], [4], [4], [4], [4], [4], [4], [4], [4], [4], [4], [4], [4], [4], [4], [4], [4], [4], [4], [4], [4], [4], [4], [4], [4], [4], [4]]
		self.branch_counts = [[1, 1], [1], [1], [1], [1], [1], [1], [1], [1], [1], [1], [1], [1], [1], [1], [1, 1], [1], [1], [1, 1], [1], [1], [1, 1], [1], [1], [1, 1], [1], [1], [1, 1], [1], [1], [1, 1], [1], [1], [1, 1], [1], [1], [1, 1], [1], [1], [1, 1], [1], [1], [1, 1], [1], [1], [1, 1], [1], [1], [1, 1], [1], [1], [1, 1], [1], [1], [1, 1], [1], [1], [1, 1], [1], [1], [1, 1], [1], [1], [1, 1], [1], [1], [1, 1], [1], [1], [1, 1], [1], [1], [1, 1], [1], [1], [1, 1], [1], [1], [1, 1], [1], [1], [1], [1], [1], [1, 1], [1], [1], [1], [1], [1], [], [1], [1], [1], [1], [1], [1], [1], [1], [1], [1], [1], [1], [1], [1], [1], [1], [1], [1], [1], [1], [1], [1], [1], [1], [1], [1], [1], [1], [1], [1]]
	
	def set_initial_values(self, state: State) -> None:
		state.Main_location = 0
	
	def set_initial_transient_values(self, transient: Transient) -> None:
		pass
	
	def get_transient_value(self, state: State, transient_variable: str):
		location = state.Main_location
		return None
	
	def get_transition_count(self, state: State) -> int:
		return self.transition_counts[state.Main_location]
	
	def get_transition_label(self, state: State, transition: int) -> int:
		return self.transition_labels[state.Main_location][transition]
	
	def get_guard_value(self, state: State, transition: int) -> bool:
		location = state.Main_location
		if location == 1 or location == 2 or location == 4 or location == 5 or location == 7 or location == 8 or location == 10 or location == 11 or location == 13 or location == 14 or location == 16 or location == 17 or location == 19 or location == 20 or location == 22 or location == 23 or location == 25 or location == 26 or location == 28 or location == 29 or location == 31 or location == 32 or location == 34 or location == 35 or location == 37 or location == 38 or location == 40 or location == 41 or location == 43 or location == 44 or location == 46 or location == 47 or location == 49 or location == 50 or location == 52 or location == 53 or location == 55 or location == 56 or location == 58 or location == 59 or location == 61 or location == 62 or location == 64 or location == 65 or location == 67 or location == 68 or location == 70 or location == 71 or location == 73 or location == 74 or location == 76 or location == 77 or location == 79 or location == 80 or location == 82 or location == 83 or location == 85 or location == 86 or location == 88 or location == 89 or location == 91 or location == 92 or location == 93 or location == 94 or location == 95 or location == 96 or location == 97 or location == 98 or location == 99 or location == 100 or location == 101 or location == 102 or location == 103 or location == 104 or location == 105 or location == 106 or location == 107 or location == 108 or location == 109 or location == 110 or location == 111 or location == 112 or location == 113 or location == 114 or location == 115 or location == 116 or location == 117 or location == 118 or location == 119 or location == 120:
			return True
		elif location == 3:
			return ((state.t <= 1113) and ((state.battery - 3259020) >= 67691520))
		elif location == 6:
			return ((state.t <= 6813) and ((state.battery - 2364120) >= 67691520))
		elif location == 9:
			return ((state.t <= 12533) and ((state.battery - 1132404) >= 67691520))
		elif location == 12:
			return ((state.t <= 18343) and ((state.battery - -17253) >= 67691520))
		elif location == 81:
			return ((state.t <= 77913) and ((state.battery - 2489217) >= 67691520))
		elif location == 87:
			return ((state.t <= 83530) and ((((state.battery - 3163497) - (2989 * (83530 - state.t))) + (5700 * (52854 - state.ts))) >= 59904000))
		elif location == 0:
			if transition == 0:
				return ((state.t <= 0) and ((state.battery - 8587050) >= 67691520))
			elif transition == 1:
				return ((((state.battery < 119808000) or ((state.battery - 8587050) < 67691520)) or (state.t > 0)) and (state.IDofLastJob != 0))
		elif location == 15:
			if transition == 0:
				return ((state.t <= 19722) and ((state.battery - 17333440) >= 67691520))
			elif transition == 1:
				return ((((state.battery < 119808000) or ((state.battery - 17333440) < 67691520)) or (state.t > 19722)) and (state.IDofLastJob != 5))
		elif location == 18:
			if transition == 0:
				return ((state.t <= 22395) and ((state.battery - 17717740) >= 67691520))
			elif transition == 1:
				return ((((state.battery < 119808000) or ((state.battery - 17717740) < 67691520)) or (state.t > 22395)) and (state.IDofLastJob != 6))
		elif location == 21:
			if transition == 0:
				return ((state.t <= 24080) and ((state.battery - 2489646) >= 67691520))
			elif transition == 1:
				return ((((state.battery < 119808000) or ((state.battery - 2489646) < 67691520)) or (state.t > 24080)) and (state.IDofLastJob != 7))
		elif location == 24:
			if transition == 0:
				return ((state.t <= 25488) and ((state.battery - 17321240) >= 67691520))
			elif transition == 1:
				return ((((state.battery < 119808000) or ((state.battery - 17321240) < 67691520)) or (state.t > 25488)) and (state.IDofLastJob != 8))
		elif location == 27:
			if transition == 0:
				return ((state.t <= 28175) and ((state.battery - 17705540) >= 67691520))
			elif transition == 1:
				return ((((state.battery < 119808000) or ((state.battery - 17705540) < 67691520)) or (state.t > 28175)) and (state.IDofLastJob != 9))
		elif location == 30:
			if transition == 0:
				return ((state.t <= 31213) and ((state.battery - 17302940) >= 67691520))
			elif transition == 1:
				return ((((state.battery < 119808000) or ((state.battery - 17302940) < 67691520)) or (state.t > 31213)) and (state.IDofLastJob != 10))
		elif location == 33:
			if transition == 0:
				return ((state.t <= 33902) and ((state.battery - 17687240) >= 67691520))
			elif transition == 1:
				return ((((state.battery < 119808000) or ((state.battery - 17687240) < 67691520)) or (state.t > 33902)) and (state.IDofLastJob != 11))
		elif location == 36:
			if transition == 0:
				return ((state.t <= 35398) and ((state.battery - 4339980) >= 67691520))
			elif transition == 1:
				return ((((state.battery < 119808000) or ((state.battery - 4339980) < 67691520)) or (state.t > 35398)) and (state.IDofLastJob != 12))
		elif location == 39:
			if transition == 0:
				return ((state.t <= 36954) and ((state.battery - 17284640) >= 67691520))
			elif transition == 1:
				return ((((state.battery < 119808000) or ((state.battery - 17284640) < 67691520)) or (state.t > 36954)) and (state.IDofLastJob != 13))
		elif location == 42:
			if transition == 0:
				return ((state.t <= 39634) and ((state.battery - 17668940) >= 67691520))
			elif transition == 1:
				return ((((state.battery < 119808000) or ((state.battery - 17668940) < 67691520)) or (state.t > 39634)) and (state.IDofLastJob != 14))
		elif location == 45:
			if transition == 0:
				return ((state.t <= 41073) and ((state.battery - 5152572) >= 67691520))
			elif transition == 1:
				return ((((state.battery < 119808000) or ((state.battery - 5152572) < 67691520)) or (state.t > 41073)) and (state.IDofLastJob != 15))
		elif location == 48:
			if transition == 0:
				return ((state.t <= 42783) and ((state.battery - 17266340) >= 67691520))
			elif transition == 1:
				return ((((state.battery < 119808000) or ((state.battery - 17266340) < 67691520)) or (state.t > 42783)) and (state.IDofLastJob != 16))
		elif location == 51:
			if transition == 0:
				return ((state.t <= 45440) and ((state.battery - 17644540) >= 67691520))
			elif transition == 1:
				return ((((state.battery < 119808000) or ((state.battery - 17644540) < 67691520)) or (state.t > 45440)) and (state.IDofLastJob != 17))
		elif location == 54:
			if transition == 0:
				return ((state.t <= 48794) and ((state.battery - 17241940) >= 67691520))
			elif transition == 1:
				return ((((state.battery < 119808000) or ((state.battery - 17241940) < 67691520)) or (state.t > 48794)) and (state.IDofLastJob != 18))
		elif location == 57:
			if transition == 0:
				return ((state.t <= 51418) and ((state.battery - 17229740) >= 67691520))
			elif transition == 1:
				return ((((state.battery < 119808000) or ((state.battery - 17229740) < 67691520)) or (state.t > 51418)) and (state.IDofLastJob != 19))
		elif location == 60:
			if transition == 0:
				return ((state.t <= 54877) and ((state.battery - 17614040) >= 67691520))
			elif transition == 1:
				return ((((state.battery < 119808000) or ((state.battery - 17614040) < 67691520)) or (state.t > 54877)) and (state.IDofLastJob != 20))
		elif location == 63:
			if transition == 0:
				return ((state.t <= 57513) and ((state.battery - 17211440) >= 67691520))
			elif transition == 1:
				return ((((state.battery < 119808000) or ((state.battery - 17211440) < 67691520)) or (state.t > 57513)) and (state.IDofLastJob != 21))
		elif location == 66:
			if transition == 0:
				return ((state.t <= 60797) and ((state.battery - 17595740) >= 67691520))
			elif transition == 1:
				return ((((state.battery < 119808000) or ((state.battery - 17595740) < 67691520)) or (state.t > 60797)) and (state.IDofLastJob != 22))
		elif location == 69:
			if transition == 0:
				return ((state.t <= 63463) and ((state.battery - 17193140) >= 67691520))
			elif transition == 1:
				return ((((state.battery < 119808000) or ((state.battery - 17193140) < 67691520)) or (state.t > 63463)) and (state.IDofLastJob != 23))
		elif location == 72:
			if transition == 0:
				return ((state.t <= 66584) and ((state.battery - 17571340) >= 67691520))
			elif transition == 1:
				return ((((state.battery < 119808000) or ((state.battery - 17571340) < 67691520)) or (state.t > 66584)) and (state.IDofLastJob != 24))
		elif location == 75:
			if transition == 0:
				return ((state.t <= 69264) and ((state.battery - 17174840) >= 67691520))
			elif transition == 1:
				return ((((state.battery < 119808000) or ((state.battery - 17174840) < 67691520)) or (state.t > 69264)) and (state.IDofLastJob != 25))
		elif location == 78:
			if transition == 0:
				return ((state.t <= 72314) and ((state.battery - 17559140) >= 67691520))
			elif transition == 1:
				return ((((state.battery < 119808000) or ((state.battery - 17559140) < 67691520)) or (state.t > 72314)) and (state.IDofLastJob != 26))
		elif location == 84:
			if transition == 0:
				return ((state.t <= 82392) and ((state.battery - 8781192) >= 67691520))
			elif transition == 1:
				return ((((state.battery < 119808000) or ((state.battery - 8781192) < 67691520)) or (state.t > 82392)) and (state.IDofLastJob != 28))
	
	def get_branch_count(self, state: State, transition: int) -> int:
		return self.branch_counts[state.Main_location][transition]
	
	def get_probability_value(self, state: State, transition: int, branch: int) -> float:
		location = state.Main_location
		if location == 0:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 1:
			if transition == 0:
				return 1
		elif location == 2:
			if transition == 0:
				return 1
		elif location == 3:
			if transition == 0:
				return 1
		elif location == 4:
			if transition == 0:
				return 1
		elif location == 5:
			if transition == 0:
				return 1
		elif location == 6:
			if transition == 0:
				return 1
		elif location == 7:
			if transition == 0:
				return 1
		elif location == 8:
			if transition == 0:
				return 1
		elif location == 9:
			if transition == 0:
				return 1
		elif location == 10:
			if transition == 0:
				return 1
		elif location == 11:
			if transition == 0:
				return 1
		elif location == 12:
			if transition == 0:
				return 1
		elif location == 13:
			if transition == 0:
				return 1
		elif location == 14:
			if transition == 0:
				return 1
		elif location == 15:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 16:
			if transition == 0:
				return 1
		elif location == 17:
			if transition == 0:
				return 1
		elif location == 18:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 19:
			if transition == 0:
				return 1
		elif location == 20:
			if transition == 0:
				return 1
		elif location == 21:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 22:
			if transition == 0:
				return 1
		elif location == 23:
			if transition == 0:
				return 1
		elif location == 24:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 25:
			if transition == 0:
				return 1
		elif location == 26:
			if transition == 0:
				return 1
		elif location == 27:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 28:
			if transition == 0:
				return 1
		elif location == 29:
			if transition == 0:
				return 1
		elif location == 30:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 31:
			if transition == 0:
				return 1
		elif location == 32:
			if transition == 0:
				return 1
		elif location == 33:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 34:
			if transition == 0:
				return 1
		elif location == 35:
			if transition == 0:
				return 1
		elif location == 36:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 37:
			if transition == 0:
				return 1
		elif location == 38:
			if transition == 0:
				return 1
		elif location == 39:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 40:
			if transition == 0:
				return 1
		elif location == 41:
			if transition == 0:
				return 1
		elif location == 42:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 43:
			if transition == 0:
				return 1
		elif location == 44:
			if transition == 0:
				return 1
		elif location == 45:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 46:
			if transition == 0:
				return 1
		elif location == 47:
			if transition == 0:
				return 1
		elif location == 48:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 49:
			if transition == 0:
				return 1
		elif location == 50:
			if transition == 0:
				return 1
		elif location == 51:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 52:
			if transition == 0:
				return 1
		elif location == 53:
			if transition == 0:
				return 1
		elif location == 54:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 55:
			if transition == 0:
				return 1
		elif location == 56:
			if transition == 0:
				return 1
		elif location == 57:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 58:
			if transition == 0:
				return 1
		elif location == 59:
			if transition == 0:
				return 1
		elif location == 60:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 61:
			if transition == 0:
				return 1
		elif location == 62:
			if transition == 0:
				return 1
		elif location == 63:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 64:
			if transition == 0:
				return 1
		elif location == 65:
			if transition == 0:
				return 1
		elif location == 66:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 67:
			if transition == 0:
				return 1
		elif location == 68:
			if transition == 0:
				return 1
		elif location == 69:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 70:
			if transition == 0:
				return 1
		elif location == 71:
			if transition == 0:
				return 1
		elif location == 72:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 73:
			if transition == 0:
				return 1
		elif location == 74:
			if transition == 0:
				return 1
		elif location == 75:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 76:
			if transition == 0:
				return 1
		elif location == 77:
			if transition == 0:
				return 1
		elif location == 78:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 79:
			if transition == 0:
				return 1
		elif location == 80:
			if transition == 0:
				return 1
		elif location == 81:
			if transition == 0:
				return 1
		elif location == 82:
			if transition == 0:
				return 1
		elif location == 83:
			if transition == 0:
				return 1
		elif location == 84:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 85:
			if transition == 0:
				return 1
		elif location == 86:
			if transition == 0:
				return 1
		elif location == 87:
			if transition == 0:
				return 1
		elif location == 88:
			if transition == 0:
				return 1
		elif location == 89:
			if transition == 0:
				return 1
		elif location == 90:
			pass
		elif location == 91:
			if transition == 0:
				return 1
		elif location == 92:
			if transition == 0:
				return 1
		elif location == 93:
			if transition == 0:
				return 1
		elif location == 94:
			if transition == 0:
				return 1
		elif location == 95:
			if transition == 0:
				return 1
		elif location == 96:
			if transition == 0:
				return 1
		elif location == 97:
			if transition == 0:
				return 1
		elif location == 98:
			if transition == 0:
				return 1
		elif location == 99:
			if transition == 0:
				return 1
		elif location == 100:
			if transition == 0:
				return 1
		elif location == 101:
			if transition == 0:
				return 1
		elif location == 102:
			if transition == 0:
				return 1
		elif location == 103:
			if transition == 0:
				return 1
		elif location == 104:
			if transition == 0:
				return 1
		elif location == 105:
			if transition == 0:
				return 1
		elif location == 106:
			if transition == 0:
				return 1
		elif location == 107:
			if transition == 0:
				return 1
		elif location == 108:
			if transition == 0:
				return 1
		elif location == 109:
			if transition == 0:
				return 1
		elif location == 110:
			if transition == 0:
				return 1
		elif location == 111:
			if transition == 0:
				return 1
		elif location == 112:
			if transition == 0:
				return 1
		elif location == 113:
			if transition == 0:
				return 1
		elif location == 114:
			if transition == 0:
				return 1
		elif location == 115:
			if transition == 0:
				return 1
		elif location == 116:
			if transition == 0:
				return 1
		elif location == 117:
			if transition == 0:
				return 1
		elif location == 118:
			if transition == 0:
				return 1
		elif location == 119:
			if transition == 0:
				return 1
		elif location == 120:
			if transition == 0:
				return 1
	
	def jump(self, state: State, transient: Transient, transition: int, branch: int, assignment_index: int, target_state: State, target_transient: Transient) -> None:
		if assignment_index == 0:
			location = state.Main_location
			if location == 0:
				if transition == 0:
					if branch == 0:
						target_state.t = 0
						target_state.ts = 0
						target_state.battery = min(119808000, ((state.battery - (2989 * (-state.t))) + (5700 * (-state.ts))))
						target_state.Main_location = 120
				elif transition == 1:
					if branch == 0:
						target_state.Main_location = 1
			elif location == 1:
				if transition == 0:
					if branch == 0:
						target_state.t = max(state.t, 1113)
						target_state.ts = max(state.ts, 0)
						target_state.battery = min(119808000, ((state.battery - (2989 * (max(state.t, 1113) - state.t))) + (5700 * (max(0, state.ts) - state.ts))))
						target_state.Main_location = 2
			elif location == 2:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 3
			elif location == 3:
				if transition == 0:
					if branch == 0:
						target_state.t = 1113
						target_state.ts = 0
						target_state.battery = min(119808000, ((state.battery - (2989 * (1113 - state.t))) + (5700 * (-state.ts))))
						target_state.Main_location = 119
			elif location == 4:
				if transition == 0:
					if branch == 0:
						target_state.t = max(state.t, 6813)
						target_state.ts = max(state.ts, 3503)
						target_state.battery = min(119808000, ((state.battery - (2989 * (max(state.t, 6813) - state.t))) + (5700 * (max(3503, state.ts) - state.ts))))
						target_state.Main_location = 5
			elif location == 5:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 6
			elif location == 6:
				if transition == 0:
					if branch == 0:
						target_state.t = 6813
						target_state.ts = 3503
						target_state.battery = min(119808000, ((state.battery - (2989 * (6813 - state.t))) + (5700 * (3503 - state.ts))))
						target_state.Main_location = 118
			elif location == 7:
				if transition == 0:
					if branch == 0:
						target_state.t = max(state.t, 12533)
						target_state.ts = max(state.ts, 7009)
						target_state.battery = min(119808000, ((state.battery - (2989 * (max(state.t, 12533) - state.t))) + (5700 * (max(7009, state.ts) - state.ts))))
						target_state.Main_location = 8
			elif location == 8:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 9
			elif location == 9:
				if transition == 0:
					if branch == 0:
						target_state.t = 12533
						target_state.ts = 7009
						target_state.battery = min(119808000, ((state.battery - (2989 * (12533 - state.t))) + (5700 * (7009 - state.ts))))
						target_state.Main_location = 117
			elif location == 10:
				if transition == 0:
					if branch == 0:
						target_state.t = max(state.t, 18343)
						target_state.ts = max(state.ts, 10618)
						target_state.battery = min(119808000, ((state.battery - (2989 * (max(state.t, 18343) - state.t))) + (5700 * (max(10618, state.ts) - state.ts))))
						target_state.Main_location = 11
			elif location == 11:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 12
			elif location == 12:
				if transition == 0:
					if branch == 0:
						target_state.t = 18343
						target_state.ts = 10618
						target_state.battery = min(119808000, ((state.battery - (2989 * (18343 - state.t))) + (5700 * (10618 - state.ts))))
						target_state.Main_location = 116
			elif location == 13:
				if transition == 0:
					if branch == 0:
						target_state.t = max(state.t, 19722)
						target_state.ts = max(state.ts, 11996)
						target_state.battery = min(119808000, ((state.battery - (2989 * (max(state.t, 19722) - state.t))) + (5700 * (max(11996, state.ts) - state.ts))))
						target_state.Main_location = 14
			elif location == 14:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 15
			elif location == 15:
				if transition == 0:
					if branch == 0:
						target_state.t = 19722
						target_state.ts = 11996
						target_state.battery = min(119808000, ((state.battery - (2989 * (19722 - state.t))) + (5700 * (11996 - state.ts))))
						target_state.Main_location = 115
				elif transition == 1:
					if branch == 0:
						target_state.Main_location = 16
			elif location == 16:
				if transition == 0:
					if branch == 0:
						target_state.t = max(state.t, 22395)
						target_state.ts = max(state.ts, 14030)
						target_state.battery = min(119808000, ((state.battery - (2989 * (max(state.t, 22395) - state.t))) + (5700 * (max(14030, state.ts) - state.ts))))
						target_state.Main_location = 17
			elif location == 17:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 18
			elif location == 18:
				if transition == 0:
					if branch == 0:
						target_state.t = 22395
						target_state.ts = 14030
						target_state.battery = min(119808000, ((state.battery - (2989 * (22395 - state.t))) + (5700 * (14030 - state.ts))))
						target_state.Main_location = 114
				elif transition == 1:
					if branch == 0:
						target_state.Main_location = 19
			elif location == 19:
				if transition == 0:
					if branch == 0:
						target_state.t = max(state.t, 24080)
						target_state.ts = max(state.ts, 14363)
						target_state.battery = min(119808000, ((state.battery - (2989 * (max(state.t, 24080) - state.t))) + (5700 * (max(14363, state.ts) - state.ts))))
						target_state.Main_location = 20
			elif location == 20:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 21
			elif location == 21:
				if transition == 0:
					if branch == 0:
						target_state.t = 24080
						target_state.ts = 14363
						target_state.battery = min(119808000, ((state.battery - (2989 * (24080 - state.t))) + (5700 * (14363 - state.ts))))
						target_state.Main_location = 113
				elif transition == 1:
					if branch == 0:
						target_state.Main_location = 22
			elif location == 22:
				if transition == 0:
					if branch == 0:
						target_state.t = max(state.t, 25488)
						target_state.ts = max(state.ts, 15771)
						target_state.battery = min(119808000, ((state.battery - (2989 * (max(state.t, 25488) - state.t))) + (5700 * (max(15771, state.ts) - state.ts))))
						target_state.Main_location = 23
			elif location == 23:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 24
			elif location == 24:
				if transition == 0:
					if branch == 0:
						target_state.t = 25488
						target_state.ts = 15771
						target_state.battery = min(119808000, ((state.battery - (2989 * (25488 - state.t))) + (5700 * (15771 - state.ts))))
						target_state.Main_location = 112
				elif transition == 1:
					if branch == 0:
						target_state.Main_location = 25
			elif location == 25:
				if transition == 0:
					if branch == 0:
						target_state.t = max(state.t, 28175)
						target_state.ts = max(state.ts, 17545)
						target_state.battery = min(119808000, ((state.battery - (2989 * (max(state.t, 28175) - state.t))) + (5700 * (max(17545, state.ts) - state.ts))))
						target_state.Main_location = 26
			elif location == 26:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 27
			elif location == 27:
				if transition == 0:
					if branch == 0:
						target_state.t = 28175
						target_state.ts = 17545
						target_state.battery = min(119808000, ((state.battery - (2989 * (28175 - state.t))) + (5700 * (17545 - state.ts))))
						target_state.Main_location = 111
				elif transition == 1:
					if branch == 0:
						target_state.Main_location = 28
			elif location == 28:
				if transition == 0:
					if branch == 0:
						target_state.t = max(state.t, 31213)
						target_state.ts = max(state.ts, 19506)
						target_state.battery = min(119808000, ((state.battery - (2989 * (max(state.t, 31213) - state.t))) + (5700 * (max(19506, state.ts) - state.ts))))
						target_state.Main_location = 29
			elif location == 29:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 30
			elif location == 30:
				if transition == 0:
					if branch == 0:
						target_state.t = 31213
						target_state.ts = 19506
						target_state.battery = min(119808000, ((state.battery - (2989 * (31213 - state.t))) + (5700 * (19506 - state.ts))))
						target_state.Main_location = 110
				elif transition == 1:
					if branch == 0:
						target_state.Main_location = 31
			elif location == 31:
				if transition == 0:
					if branch == 0:
						target_state.t = max(state.t, 33902)
						target_state.ts = max(state.ts, 21062)
						target_state.battery = min(119808000, ((state.battery - (2989 * (max(state.t, 33902) - state.t))) + (5700 * (max(21062, state.ts) - state.ts))))
						target_state.Main_location = 32
			elif location == 32:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 33
			elif location == 33:
				if transition == 0:
					if branch == 0:
						target_state.t = 33902
						target_state.ts = 21062
						target_state.battery = min(119808000, ((state.battery - (2989 * (33902 - state.t))) + (5700 * (21062 - state.ts))))
						target_state.Main_location = 109
				elif transition == 1:
					if branch == 0:
						target_state.Main_location = 34
			elif location == 34:
				if transition == 0:
					if branch == 0:
						target_state.t = max(state.t, 35398)
						target_state.ts = max(state.ts, 21704)
						target_state.battery = min(119808000, ((state.battery - (2989 * (max(state.t, 35398) - state.t))) + (5700 * (max(21704, state.ts) - state.ts))))
						target_state.Main_location = 35
			elif location == 35:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 36
			elif location == 36:
				if transition == 0:
					if branch == 0:
						target_state.t = 35398
						target_state.ts = 21704
						target_state.battery = min(119808000, ((state.battery - (2989 * (35398 - state.t))) + (5700 * (21704 - state.ts))))
						target_state.Main_location = 108
				elif transition == 1:
					if branch == 0:
						target_state.Main_location = 37
			elif location == 37:
				if transition == 0:
					if branch == 0:
						target_state.t = max(state.t, 36954)
						target_state.ts = max(state.ts, 23260)
						target_state.battery = min(119808000, ((state.battery - (2989 * (max(state.t, 36954) - state.t))) + (5700 * (max(23260, state.ts) - state.ts))))
						target_state.Main_location = 38
			elif location == 38:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 39
			elif location == 39:
				if transition == 0:
					if branch == 0:
						target_state.t = 36954
						target_state.ts = 23260
						target_state.battery = min(119808000, ((state.battery - (2989 * (36954 - state.t))) + (5700 * (23260 - state.ts))))
						target_state.Main_location = 107
				elif transition == 1:
					if branch == 0:
						target_state.Main_location = 40
			elif location == 40:
				if transition == 0:
					if branch == 0:
						target_state.t = max(state.t, 39634)
						target_state.ts = max(state.ts, 24582)
						target_state.battery = min(119808000, ((state.battery - (2989 * (max(state.t, 39634) - state.t))) + (5700 * (max(24582, state.ts) - state.ts))))
						target_state.Main_location = 41
			elif location == 41:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 42
			elif location == 42:
				if transition == 0:
					if branch == 0:
						target_state.t = 39634
						target_state.ts = 24582
						target_state.battery = min(119808000, ((state.battery - (2989 * (39634 - state.t))) + (5700 * (24582 - state.ts))))
						target_state.Main_location = 106
				elif transition == 1:
					if branch == 0:
						target_state.Main_location = 43
			elif location == 43:
				if transition == 0:
					if branch == 0:
						target_state.t = max(state.t, 41073)
						target_state.ts = max(state.ts, 25395)
						target_state.battery = min(119808000, ((state.battery - (2989 * (max(state.t, 41073) - state.t))) + (5700 * (max(25395, state.ts) - state.ts))))
						target_state.Main_location = 44
			elif location == 44:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 45
			elif location == 45:
				if transition == 0:
					if branch == 0:
						target_state.t = 41073
						target_state.ts = 25395
						target_state.battery = min(119808000, ((state.battery - (2989 * (41073 - state.t))) + (5700 * (25395 - state.ts))))
						target_state.Main_location = 105
				elif transition == 1:
					if branch == 0:
						target_state.Main_location = 46
			elif location == 46:
				if transition == 0:
					if branch == 0:
						target_state.t = max(state.t, 42783)
						target_state.ts = max(state.ts, 27105)
						target_state.battery = min(119808000, ((state.battery - (2989 * (max(state.t, 42783) - state.t))) + (5700 * (max(27105, state.ts) - state.ts))))
						target_state.Main_location = 47
			elif location == 47:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 48
			elif location == 48:
				if transition == 0:
					if branch == 0:
						target_state.t = 42783
						target_state.ts = 27105
						target_state.battery = min(119808000, ((state.battery - (2989 * (42783 - state.t))) + (5700 * (27105 - state.ts))))
						target_state.Main_location = 104
				elif transition == 1:
					if branch == 0:
						target_state.Main_location = 49
			elif location == 49:
				if transition == 0:
					if branch == 0:
						target_state.t = max(state.t, 45440)
						target_state.ts = max(state.ts, 28105)
						target_state.battery = min(119808000, ((state.battery - (2989 * (max(state.t, 45440) - state.t))) + (5700 * (max(28105, state.ts) - state.ts))))
						target_state.Main_location = 50
			elif location == 50:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 51
			elif location == 51:
				if transition == 0:
					if branch == 0:
						target_state.t = 45440
						target_state.ts = 28105
						target_state.battery = min(119808000, ((state.battery - (2989 * (45440 - state.t))) + (5700 * (28105 - state.ts))))
						target_state.Main_location = 103
				elif transition == 1:
					if branch == 0:
						target_state.Main_location = 52
			elif location == 52:
				if transition == 0:
					if branch == 0:
						target_state.t = max(state.t, 48794)
						target_state.ts = max(state.ts, 31135)
						target_state.battery = min(119808000, ((state.battery - (2989 * (max(state.t, 48794) - state.t))) + (5700 * (max(31135, state.ts) - state.ts))))
						target_state.Main_location = 53
			elif location == 53:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 54
			elif location == 54:
				if transition == 0:
					if branch == 0:
						target_state.t = 48794
						target_state.ts = 31135
						target_state.battery = min(119808000, ((state.battery - (2989 * (48794 - state.t))) + (5700 * (31135 - state.ts))))
						target_state.Main_location = 102
				elif transition == 1:
					if branch == 0:
						target_state.Main_location = 55
			elif location == 55:
				if transition == 0:
					if branch == 0:
						target_state.t = max(state.t, 51418)
						target_state.ts = max(state.ts, 31782)
						target_state.battery = min(119808000, ((state.battery - (2989 * (max(state.t, 51418) - state.t))) + (5700 * (max(31782, state.ts) - state.ts))))
						target_state.Main_location = 56
			elif location == 56:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 57
			elif location == 57:
				if transition == 0:
					if branch == 0:
						target_state.t = 51418
						target_state.ts = 31782
						target_state.battery = min(119808000, ((state.battery - (2989 * (51418 - state.t))) + (5700 * (31782 - state.ts))))
						target_state.Main_location = 101
				elif transition == 1:
					if branch == 0:
						target_state.Main_location = 58
			elif location == 58:
				if transition == 0:
					if branch == 0:
						target_state.t = max(state.t, 54877)
						target_state.ts = max(state.ts, 35162)
						target_state.battery = min(119808000, ((state.battery - (2989 * (max(state.t, 54877) - state.t))) + (5700 * (max(35162, state.ts) - state.ts))))
						target_state.Main_location = 59
			elif location == 59:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 60
			elif location == 60:
				if transition == 0:
					if branch == 0:
						target_state.t = 54877
						target_state.ts = 35162
						target_state.battery = min(119808000, ((state.battery - (2989 * (54877 - state.t))) + (5700 * (35162 - state.ts))))
						target_state.Main_location = 100
				elif transition == 1:
					if branch == 0:
						target_state.Main_location = 61
			elif location == 61:
				if transition == 0:
					if branch == 0:
						target_state.t = max(state.t, 57513)
						target_state.ts = max(state.ts, 35902)
						target_state.battery = min(119808000, ((state.battery - (2989 * (max(state.t, 57513) - state.t))) + (5700 * (max(35902, state.ts) - state.ts))))
						target_state.Main_location = 62
			elif location == 62:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 63
			elif location == 63:
				if transition == 0:
					if branch == 0:
						target_state.t = 57513
						target_state.ts = 35902
						target_state.battery = min(119808000, ((state.battery - (2989 * (57513 - state.t))) + (5700 * (35902 - state.ts))))
						target_state.Main_location = 99
				elif transition == 1:
					if branch == 0:
						target_state.Main_location = 64
			elif location == 64:
				if transition == 0:
					if branch == 0:
						target_state.t = max(state.t, 60797)
						target_state.ts = max(state.ts, 38694)
						target_state.battery = min(119808000, ((state.battery - (2989 * (max(state.t, 60797) - state.t))) + (5700 * (max(38694, state.ts) - state.ts))))
						target_state.Main_location = 65
			elif location == 65:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 66
			elif location == 66:
				if transition == 0:
					if branch == 0:
						target_state.t = 60797
						target_state.ts = 38694
						target_state.battery = min(119808000, ((state.battery - (2989 * (60797 - state.t))) + (5700 * (38694 - state.ts))))
						target_state.Main_location = 98
				elif transition == 1:
					if branch == 0:
						target_state.Main_location = 67
			elif location == 67:
				if transition == 0:
					if branch == 0:
						target_state.t = max(state.t, 63463)
						target_state.ts = max(state.ts, 39880)
						target_state.battery = min(119808000, ((state.battery - (2989 * (max(state.t, 63463) - state.t))) + (5700 * (max(39880, state.ts) - state.ts))))
						target_state.Main_location = 68
			elif location == 68:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 69
			elif location == 69:
				if transition == 0:
					if branch == 0:
						target_state.t = 63463
						target_state.ts = 39880
						target_state.battery = min(119808000, ((state.battery - (2989 * (63463 - state.t))) + (5700 * (39880 - state.ts))))
						target_state.Main_location = 97
				elif transition == 1:
					if branch == 0:
						target_state.Main_location = 70
			elif location == 70:
				if transition == 0:
					if branch == 0:
						target_state.t = max(state.t, 66584)
						target_state.ts = max(state.ts, 42229)
						target_state.battery = min(119808000, ((state.battery - (2989 * (max(state.t, 66584) - state.t))) + (5700 * (max(42229, state.ts) - state.ts))))
						target_state.Main_location = 71
			elif location == 71:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 72
			elif location == 72:
				if transition == 0:
					if branch == 0:
						target_state.t = 66584
						target_state.ts = 42229
						target_state.battery = min(119808000, ((state.battery - (2989 * (66584 - state.t))) + (5700 * (42229 - state.ts))))
						target_state.Main_location = 96
				elif transition == 1:
					if branch == 0:
						target_state.Main_location = 73
			elif location == 73:
				if transition == 0:
					if branch == 0:
						target_state.t = max(state.t, 69264)
						target_state.ts = max(state.ts, 43712)
						target_state.battery = min(119808000, ((state.battery - (2989 * (max(state.t, 69264) - state.t))) + (5700 * (max(43712, state.ts) - state.ts))))
						target_state.Main_location = 74
			elif location == 74:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 75
			elif location == 75:
				if transition == 0:
					if branch == 0:
						target_state.t = 69264
						target_state.ts = 43712
						target_state.battery = min(119808000, ((state.battery - (2989 * (69264 - state.t))) + (5700 * (43712 - state.ts))))
						target_state.Main_location = 95
				elif transition == 1:
					if branch == 0:
						target_state.Main_location = 76
			elif location == 76:
				if transition == 0:
					if branch == 0:
						target_state.t = max(state.t, 72314)
						target_state.ts = max(state.ts, 45768)
						target_state.battery = min(119808000, ((state.battery - (2989 * (max(state.t, 72314) - state.t))) + (5700 * (max(45768, state.ts) - state.ts))))
						target_state.Main_location = 77
			elif location == 77:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 78
			elif location == 78:
				if transition == 0:
					if branch == 0:
						target_state.t = 72314
						target_state.ts = 45768
						target_state.battery = min(119808000, ((state.battery - (2989 * (72314 - state.t))) + (5700 * (45768 - state.ts))))
						target_state.Main_location = 94
				elif transition == 1:
					if branch == 0:
						target_state.Main_location = 79
			elif location == 79:
				if transition == 0:
					if branch == 0:
						target_state.t = max(state.t, 77913)
						target_state.ts = max(state.ts, 49309)
						target_state.battery = min(119808000, ((state.battery - (2989 * (max(state.t, 77913) - state.t))) + (5700 * (max(49309, state.ts) - state.ts))))
						target_state.Main_location = 80
			elif location == 80:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 81
			elif location == 81:
				if transition == 0:
					if branch == 0:
						target_state.t = 77913
						target_state.ts = 49309
						target_state.battery = min(119808000, ((state.battery - (2989 * (77913 - state.t))) + (5700 * (49309 - state.ts))))
						target_state.Main_location = 93
			elif location == 82:
				if transition == 0:
					if branch == 0:
						target_state.t = max(state.t, 82392)
						target_state.ts = max(state.ts, 52854)
						target_state.battery = min(119808000, ((state.battery - (2989 * (max(state.t, 82392) - state.t))) + (5700 * (max(52854, state.ts) - state.ts))))
						target_state.Main_location = 83
			elif location == 83:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 84
			elif location == 84:
				if transition == 0:
					if branch == 0:
						target_state.t = 82392
						target_state.ts = 52854
						target_state.battery = min(119808000, ((state.battery - (2989 * (82392 - state.t))) + (5700 * (52854 - state.ts))))
						target_state.Main_location = 92
				elif transition == 1:
					if branch == 0:
						target_state.Main_location = 85
			elif location == 85:
				if transition == 0:
					if branch == 0:
						target_state.t = max(state.t, 83530)
						target_state.ts = max(state.ts, 52854)
						target_state.battery = min(119808000, ((state.battery - (2989 * (max(state.t, 83530) - state.t))) + (5700 * (max(52854, state.ts) - state.ts))))
						target_state.Main_location = 86
			elif location == 86:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 87
			elif location == 87:
				if transition == 0:
					if branch == 0:
						target_state.t = 83530
						target_state.ts = 52854
						target_state.battery = min(119808000, ((state.battery - (2989 * (83530 - state.t))) + (5700 * (52854 - state.ts))))
						target_state.Main_location = 91
			elif location == 88:
				if transition == 0:
					if branch == 0:
						target_state.t = max(state.t, 84093)
						target_state.ts = max(state.ts, 52854)
						target_state.battery = min(119808000, ((state.battery - (2989 * (max(84093, state.t) - state.t))) + (5700 * (max(52854, state.ts) - state.ts))))
						target_state.endOfday = True
						target_state.Main_location = 89
			elif location == 89:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 90
			elif location == 91:
				if transition == 0:
					if branch == 0:
						target_state.t = 84093
						target_state.ts = 52854
						target_state.battery = min(119808000, (state.battery - 3163497))
						target_state.IDofLastJob = 29
						target_state.jobDone = (state.jobDone + 1)
						target_state.endOfday = True
						target_state.Main_location = 89
			elif location == 92:
				if transition == 0:
					if branch == 0:
						target_state.t = 82980
						target_state.ts = 52854
						target_state.battery = min(119808000, (state.battery - 8781192))
						target_state.IDofLastJob = 28
						target_state.jobDone = (state.jobDone + 1)
						target_state.Main_location = 86
			elif location == 93:
				if transition == 0:
					if branch == 0:
						target_state.t = 78356
						target_state.ts = 49309
						target_state.battery = min(119808000, (state.battery - 2489217))
						target_state.IDofLastJob = 27
						target_state.jobDone = (state.jobDone + 1)
						target_state.Main_location = 83
			elif location == 94:
				if transition == 0:
					if branch == 0:
						target_state.t = 77884
						target_state.ts = 49309
						target_state.battery = min(119808000, (state.battery - 17559140))
						target_state.IDofLastJob = 26
						target_state.jobDone = (state.jobDone + 1)
						target_state.Main_location = 80
			elif location == 95:
				if transition == 0:
					if branch == 0:
						target_state.t = 74834
						target_state.ts = 47316
						target_state.battery = min(119808000, (state.battery - 17174840))
						target_state.IDofLastJob = 25
						target_state.jobDone = (state.jobDone + 1)
						target_state.Main_location = 77
			elif location == 96:
				if transition == 0:
					if branch == 0:
						target_state.t = 72154
						target_state.ts = 45768
						target_state.battery = min(119808000, (state.battery - 17571340))
						target_state.IDofLastJob = 24
						target_state.jobDone = (state.jobDone + 1)
						target_state.Main_location = 74
			elif location == 97:
				if transition == 0:
					if branch == 0:
						target_state.t = 69033
						target_state.ts = 43481
						target_state.battery = min(119808000, (state.battery - 17193140))
						target_state.IDofLastJob = 23
						target_state.jobDone = (state.jobDone + 1)
						target_state.Main_location = 71
			elif location == 98:
				if transition == 0:
					if branch == 0:
						target_state.t = 66367
						target_state.ts = 42229
						target_state.battery = min(119808000, (state.battery - 17595740))
						target_state.IDofLastJob = 22
						target_state.jobDone = (state.jobDone + 1)
						target_state.Main_location = 68
			elif location == 99:
				if transition == 0:
					if branch == 0:
						target_state.t = 63083
						target_state.ts = 39500
						target_state.battery = min(119808000, (state.battery - 17211440))
						target_state.IDofLastJob = 21
						target_state.jobDone = (state.jobDone + 1)
						target_state.Main_location = 65
			elif location == 100:
				if transition == 0:
					if branch == 0:
						target_state.t = 60447
						target_state.ts = 38694
						target_state.battery = min(119808000, (state.battery - 17614040))
						target_state.IDofLastJob = 20
						target_state.jobDone = (state.jobDone + 1)
						target_state.Main_location = 62
			elif location == 101:
				if transition == 0:
					if branch == 0:
						target_state.t = 56988
						target_state.ts = 35377
						target_state.battery = min(119808000, (state.battery - 17229740))
						target_state.IDofLastJob = 19
						target_state.jobDone = (state.jobDone + 1)
						target_state.Main_location = 59
			elif location == 102:
				if transition == 0:
					if branch == 0:
						target_state.t = 54364
						target_state.ts = 34728
						target_state.battery = min(119808000, (state.battery - 17241940))
						target_state.IDofLastJob = 18
						target_state.jobDone = (state.jobDone + 1)
						target_state.Main_location = 56
			elif location == 103:
				if transition == 0:
					if branch == 0:
						target_state.t = 51010
						target_state.ts = 31632
						target_state.battery = min(119808000, (state.battery - 17644540))
						target_state.IDofLastJob = 17
						target_state.jobDone = (state.jobDone + 1)
						target_state.Main_location = 53
			elif location == 104:
				if transition == 0:
					if branch == 0:
						target_state.t = 48353
						target_state.ts = 30694
						target_state.battery = min(119808000, (state.battery - 17266340))
						target_state.IDofLastJob = 16
						target_state.jobDone = (state.jobDone + 1)
						target_state.Main_location = 50
			elif location == 105:
				if transition == 0:
					if branch == 0:
						target_state.t = 41631
						target_state.ts = 25953
						target_state.battery = min(119808000, (state.battery - 5152572))
						target_state.IDofLastJob = 15
						target_state.jobDone = (state.jobDone + 1)
						target_state.Main_location = 47
			elif location == 106:
				if transition == 0:
					if branch == 0:
						target_state.t = 45204
						target_state.ts = 28105
						target_state.battery = min(119808000, (state.battery - 17668940))
						target_state.IDofLastJob = 14
						target_state.jobDone = (state.jobDone + 1)
						target_state.Main_location = 44
			elif location == 107:
				if transition == 0:
					if branch == 0:
						target_state.t = 42524
						target_state.ts = 26846
						target_state.battery = min(119808000, (state.battery - 17284640))
						target_state.IDofLastJob = 13
						target_state.jobDone = (state.jobDone + 1)
						target_state.Main_location = 41
			elif location == 108:
				if transition == 0:
					if branch == 0:
						target_state.t = 35868
						target_state.ts = 22174
						target_state.battery = min(119808000, (state.battery - 4339980))
						target_state.IDofLastJob = 12
						target_state.jobDone = (state.jobDone + 1)
						target_state.Main_location = 38
			elif location == 109:
				if transition == 0:
					if branch == 0:
						target_state.t = 39472
						target_state.ts = 24582
						target_state.battery = min(119808000, (state.battery - 17687240))
						target_state.IDofLastJob = 11
						target_state.jobDone = (state.jobDone + 1)
						target_state.Main_location = 35
			elif location == 110:
				if transition == 0:
					if branch == 0:
						target_state.t = 36783
						target_state.ts = 23089
						target_state.battery = min(119808000, (state.battery - 17302940))
						target_state.IDofLastJob = 10
						target_state.jobDone = (state.jobDone + 1)
						target_state.Main_location = 32
			elif location == 111:
				if transition == 0:
					if branch == 0:
						target_state.t = 33745
						target_state.ts = 21062
						target_state.battery = min(119808000, (state.battery - 17705540))
						target_state.IDofLastJob = 9
						target_state.jobDone = (state.jobDone + 1)
						target_state.Main_location = 29
			elif location == 112:
				if transition == 0:
					if branch == 0:
						target_state.t = 31058
						target_state.ts = 19351
						target_state.battery = min(119808000, (state.battery - 17321240))
						target_state.IDofLastJob = 8
						target_state.jobDone = (state.jobDone + 1)
						target_state.Main_location = 26
			elif location == 113:
				if transition == 0:
					if branch == 0:
						target_state.t = 24349
						target_state.ts = 14631
						target_state.battery = min(119808000, (state.battery - 2489646))
						target_state.IDofLastJob = 7
						target_state.jobDone = (state.jobDone + 1)
						target_state.Main_location = 23
			elif location == 114:
				if transition == 0:
					if branch == 0:
						target_state.t = 27965
						target_state.ts = 17545
						target_state.battery = min(119808000, (state.battery - 17717740))
						target_state.IDofLastJob = 6
						target_state.jobDone = (state.jobDone + 1)
						target_state.Main_location = 20
			elif location == 115:
				if transition == 0:
					if branch == 0:
						target_state.t = 25292
						target_state.ts = 15574
						target_state.battery = min(119808000, (state.battery - 17333440))
						target_state.IDofLastJob = 5
						target_state.jobDone = (state.jobDone + 1)
						target_state.Main_location = 17
			elif location == 116:
				if transition == 0:
					if branch == 0:
						target_state.t = 18556
						target_state.ts = 10831
						target_state.battery = min(119808000, (state.battery - -17253))
						target_state.IDofLastJob = 4
						target_state.jobDone = (state.jobDone + 1)
						target_state.Main_location = 14
			elif location == 117:
				if transition == 0:
					if branch == 0:
						target_state.t = 13049
						target_state.ts = 7319
						target_state.battery = min(119808000, (state.battery - 1132404))
						target_state.IDofLastJob = 3
						target_state.jobDone = (state.jobDone + 1)
						target_state.Main_location = 11
			elif location == 118:
				if transition == 0:
					if branch == 0:
						target_state.t = 7393
						target_state.ts = 3660
						target_state.battery = min(119808000, (state.battery - 2364120))
						target_state.IDofLastJob = 2
						target_state.jobDone = (state.jobDone + 1)
						target_state.Main_location = 8
			elif location == 119:
				if transition == 0:
					if branch == 0:
						target_state.t = 1693
						target_state.ts = 0
						target_state.battery = min(119808000, (state.battery - 3259020))
						target_state.IDofLastJob = 1
						target_state.jobDone = (state.jobDone + 1)
						target_state.Main_location = 5
			elif location == 120:
				if transition == 0:
					if branch == 0:
						target_state.t = 575
						target_state.ts = 0
						target_state.battery = min(119808000, (state.battery - 8587050))
						target_state.IDofLastJob = 0
						target_state.jobDone = (state.jobDone + 1)
						target_state.Main_location = 2

class PropertyExpression(object):
	__slots__ = ("op", "args")
	
	def __init__(self, op: str, args: List[Union[int, PropertyExpression]]):
		self.op = op
		self.args = args
	
	def __str__(self):
		result = self.op + "("
		needComma = False
		for arg in self.args:
			if needComma:
				result += ", "
			else:
				needComma = True
			result += str(arg)
		return result + ")"

class Property(object):
	__slots__ = ("name", "exp")
	
	def __init__(self, name: str, exp: PropertyExpression):
		self.name = name
		self.exp = exp
	
	def __str__(self):
		return self.name + ": " + str(self.exp)

class Transition(object):
	__slots__ = ("sync_vector", "label", "transitions")
	
	def __init__(self, sync_vector: int, label: int = 0, transitions: List[int] = [-1]):
		self.sync_vector = sync_vector
		self.label = label
		self.transitions = transitions

class Branch(object):
	__slots__ = ("probability", "branches")
	
	def __init__(self, probability = 0.0, branches = [0]):
		self.probability = probability
		self.branches = branches

class Network(object):
	__slots__ = ("network", "components", "transition_labels", "sync_vectors", "properties", "variables", "_initial_transient", "_aut_Main")
	
	def __init__(self):
		self.network = self
		self.transition_labels = { 0: "τ", 1: "doJob", 2: "skipJob", 3: "endSkipJob", 4: "endJob" }
		self.sync_vectors = [[0, 0], [1, 1], [2, 2], [3, 3], [4, 4]]
		self.properties = []
		self.variables = [VariableInfo("endOfday", None, "bool"), VariableInfo("battery", None, "int"), VariableInfo("t", None, "int"), VariableInfo("ts", None, "int"), VariableInfo("IDofLastJob", None, "int", -1, 29), VariableInfo("jobDone", None, "int"), VariableInfo("Main_location", 0, "int", 0, 120)]
		self._aut_Main = MainAutomaton(self)
		self.components = [self._aut_Main]
		self._initial_transient = self._get_initial_transient()
	
	def get_initial_state(self) -> State:
		state = State()
		state.endOfday = False
		state.battery = 119808000
		state.t = 0
		state.ts = 0
		state.IDofLastJob = -1
		state.jobDone = 0
		self._aut_Main.set_initial_values(state)
		return state
	
	def _get_initial_transient(self) -> Transient:
		transient = Transient()
		self._aut_Main.set_initial_transient_values(transient)
		return transient
	
	def get_expression_value(self, state: State, expression: int):
		raise IndexError
	
	def _get_jump_expression_value(self, state: State, transient: Transient, expression: int):
		raise IndexError
	
	def _get_transient_value(self, state: State, transient_variable: str):
		# Query the automata for the current value of the transient variable
		result = self._aut_Main.get_transient_value(state, transient_variable)
		if result is not None:
			return result
		# No automaton has a value: return the transient variable's (cached) initial value
		return getattr(self._initial_transient, transient_variable)
	
	def get_transitions(self, state: State) -> List[Transition]:
		# Collect all automaton transitions, gathered by label
		transitions = []
		trans_Main = [[], [], [], [], []]
		transition_count = self._aut_Main.get_transition_count(state)
		for i in range(transition_count):
			if self._aut_Main.get_guard_value(state, i):
				trans_Main[self._aut_Main.get_transition_label(state, i)].append(i)
		# Match automaton transitions onto synchronisation vectors
		for svi in range(len(self.sync_vectors)):
			sv = self.sync_vectors[svi]
			synced = [[-1, -1]]
			# Main
			if synced is not None:
				if sv[0] != -1:
					if len(trans_Main[sv[0]]) == 0:
						synced = None
					else:
						existing = len(synced)
						for i in range(existing):
							synced[i][0] = trans_Main[sv[0]][0]
						for i in range(1, len(trans_Main[sv[0]])):
							for j in range(existing):
								synced.append(synced[j][:])
								synced[-1][0] = trans_Main[sv[0]][i]
			if synced is not None:
				for sync in synced:
					sync[-1] = sv[-1]
					sync.append(svi)
				transitions.extend(filter(lambda s: s[-2] != -1, synced))
		# Convert to Transition instances
		for i in range(len(transitions)):
			transitions[i] = Transition(transitions[i][-1], transitions[i][-2], transitions[i])
			del transitions[i].transitions[-1]
			del transitions[i].transitions[-1]
		# Done
		return transitions
	
	def get_branches(self, state: State, transition: Transition) -> List[Branch]:
		combs = [[-1]]
		probs = [1.0]
		if transition.transitions[0] != -1:
			existing = len(combs)
			branch_count = self._aut_Main.get_branch_count(state, transition.transitions[0])
			for i in range(1, branch_count):
				probability = self._aut_Main.get_probability_value(state, transition.transitions[0], i)
				for j in range(existing):
					combs.append(combs[j][:])
					combs[-1][0] = i
					probs.append(probs[j] * probability)
			probability = self._aut_Main.get_probability_value(state, transition.transitions[0], 0)
			for i in range(existing):
				combs[i][0] = 0
				probs[i] *= probability
		# Convert to Branch instances
		for i in range(len(combs)):
			combs[i] = Branch(probs[i], combs[i])
		# Done
		return list(filter(lambda b: b.probability > 0.0, combs))
	
	def jump(self, state: State, transition: Transition, branch: Branch, expressions: List[int] = []) -> State:
		transient = self._get_initial_transient()
		for i in range(0, 1):
			target_state = State()
			state.copy_to(target_state)
			target_transient = Transient()
			transient.copy_to(target_transient)
			if transition.transitions[0] != -1:
				self._aut_Main.jump(state, transient, transition.transitions[0], branch.branches[0], i, target_state, target_transient)
			state = target_state
			transient = target_transient
		for i in range(len(expressions)):
			expressions[i] = self._get_jump_expression_value(state, transient, expressions[i])
		return state
	
	def jump_np(self, state: State, transition: Transition, expressions: List[int] = []) -> State:
		return self.jump(state, transition, self.get_branches(state, transition)[0], expressions)
