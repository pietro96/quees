from datetimerange import DateTimeRange
import datetime
from datetime import timedelta
import sys
import pandas as pd
import random
import argparse


parser = argparse.ArgumentParser()
parser.add_argument("--day")
parser.add_argument("--month")
parser.add_argument("--year")
args = parser.parse_args()

GLOBAL_BPOWER = 2989 # mW Background power consumption
GLOBAL_BATTERY_CAPACITY = 149760000 # mJ

START_BATTERY_CAPACITY = 0.8 * GLOBAL_BATTERY_CAPACITY
MAX_BATTERY_CAPACITY = 0.8 * GLOBAL_BATTERY_CAPACITY
MIN_BATTERY_CAPACITY = 0.4 * GLOBAL_BATTERY_CAPACITY

# Job power consumption
GLOBAL_UHF_POWER = 2630 #mW
GLOBAL_XBAND_POWER = 11945 #mW
GLOBAL_LBAND_POWER = 3863 #mW
GLOBAL_SLEW_POWER = 414 #mW
GLOBAL_PREHEAT_POWER =  414 #mW

# The power provided by solar panels during insolation is attitude-dependent 
# and thus changes depending on the currently active job

GLOBAL_XBAND_RPOWER = 5700 #mW
GLOBAL_LBAND_RPOWER = 6100 #mW
GLOBAL_RPOWER = 5700 #mW

select_year = int(args.year)
select_month = int(args.month)
select_day = int(args.day) #20, 21, 22


def copyfile(fin, fout):
	for line in fin:
		fout.write(line)

def replaceinFile(fin, fout, keywords, keywords_value):
	for line in fin:
		newline =line
		for (keyword, keyword_value) in zip(keywords, keywords_value):
			newline = newline.replace(keyword, keyword_value)
		fout.write(newline)

def getJobByDate(data, start_id ,jobType ,select_year, select_month, select_day, file2write):
	job_id = start_id
	for start_time, stop_time, duration in zip(data['Start Time (UTCG)'], data['Stop Time (UTCG)'], data['Duration (sec)']):
		date = datetime.datetime.strptime(start_time, datetimeFormat)
		if (date.month == select_month and date.year == select_year and date.day == select_day):
			what2write=str(job_id)+','+jobType+','+str(start_time)+','+str(stop_time)+','+str(duration)+'\n'
			file2write.write(what2write)
			job_id = job_id + 1
	return job_id


def nicePrintSet(the_set):
	text=''
	for elem in the_set:
		if(text==''):
			text = str(elem)
		else:
			text=text+" or "+str(elem)
	return text

def jobEnergy(job_type, start, stop, sun_start, sun_stop):
	#all the times are in [s], the returned energy is in [mJ]
	global GLOBAL_BPOWER  
	global GLOBAL_UHF_POWER  
	global GLOBAL_XBAND_POWER 
	global GLOBAL_LBAND_POWER  
	global GLOBAL_SLEW_POWER  
	global GLOBAL_PREHEAT_POWER   
	global GLOBAL_XBAND_RPOWER  
	global GLOBAL_LBAND_RPOWER 
	global GLOBAL_RPOWER 
	delta_sun = sun_stop - sun_start
	delta = stop - start
	if(job_type == "lband3" or job_type == "lband2"):
		return round(GLOBAL_LBAND_POWER*delta + GLOBAL_BPOWER*delta - GLOBAL_LBAND_RPOWER*delta_sun + (20*60)*GLOBAL_SLEW_POWER + (20*60)*GLOBAL_PREHEAT_POWER) 
 
	if(job_type =="xband"):
		return round(GLOBAL_XBAND_POWER*delta + GLOBAL_BPOWER*delta - GLOBAL_XBAND_RPOWER*delta_sun)

	if(job_type =="uhf") :
		return round(GLOBAL_UHF_POWER*delta + GLOBAL_BPOWER*delta - GLOBAL_RPOWER*delta_sun)


#https://data36.com/pandas-tutorial-1-basics-reading-data-files-dataframes-data-selection/
#load csv data
x_band_toulouse = pd.read_csv('./Satellite-Data/X-Band-Toulouse.csv', delimiter=',', skiprows=1, names = ['Access','Start Time (UTCG)','Stop Time (UTCG)','Duration (sec)'])
x_band_kourou = pd.read_csv('./Satellite-Data/X-Band-Kourou.csv', delimiter=',', skiprows=1, names = ['Access','Start Time (UTCG)','Stop Time (UTCG)','Duration (sec)'])
l_band_3f2 = pd.read_csv('./Satellite-Data/L-Band-Inmarsat-3F2.csv', delimiter=',', skiprows=1, names = ['Access','Start Time (UTCG)','Stop Time (UTCG)','Duration (sec)'])
l_band_3f3 = pd.read_csv('./Satellite-Data/L-Band-Inmarsat-3F3.csv', delimiter=',', skiprows=1, names = ['Access','Start Time (UTCG)','Stop Time (UTCG)','Duration (sec)'])
uhf = pd.read_csv('./Satellite-Data/UHF.csv', delimiter=',', skiprows=1, names = ['Access','Start Time (UTCG)','Stop Time (UTCG)','Duration (sec)'])

sun = pd.read_csv('./Satellite-Data/Sun.csv', delimiter=',', skiprows=1, names = ["Start Time (UTCG)","Stop Time (UTCG)","Duration (sec)"])

datetimeFormat = '%Y/%m/%d %H:%M:%S.%f' # define date format

fileName = str(select_year)+"_"+str(select_month)+"_"+str(select_day)+'.csv'
tempFile = open(fileName, "w")
tempFile.write('ID,Job Type,Start Time (UTCG),Stop Time (UTCG),Duration (sec)\n')
start_id = getJobByDate(x_band_toulouse, 0 ,'xband' ,select_year, select_month, select_day, tempFile)
start_id = getJobByDate(x_band_kourou, start_id,'xband', select_year, select_month, select_day, tempFile)
start_id = getJobByDate(l_band_3f2, start_id, 'lband2', select_year, select_month, select_day, tempFile)
start_id = getJobByDate(l_band_3f3, start_id, 'lband3', select_year, select_month, select_day, tempFile)
last_id=getJobByDate(uhf, start_id,'uhf' ,select_year, select_month, select_day, tempFile)
tempFile.close()


#load temp file and order jobs according to start time
day_jobs = pd.read_csv(fileName, delimiter=',', skiprows=1, names = ['ID','Job Type','Start Time (UTCG)','Stop Time (UTCG)','Duration (sec)'])
jobs2exclude = set()
jobs2explore = set(range(last_id))
ordered_jobs_id=[]
while len(jobs2exclude)< last_id:
	candidate_id = random.choice(tuple(jobs2explore))
	candidate_date = day_jobs['Start Time (UTCG)'][candidate_id]
	candidate_date = datetime.datetime.strptime(candidate_date, datetimeFormat)
	next_jobs_id=[candidate_id]
	for job_id, start_time in zip(day_jobs['ID'],day_jobs['Start Time (UTCG)'] ):
		if(int(job_id)!=candidate_id and int(job_id) not in jobs2exclude):
			date2compare = datetime.datetime.strptime(start_time, datetimeFormat)
			if (date2compare < candidate_date):
				candidate_date = date2compare
				next_jobs_id=[int(job_id)]
			elif(date2compare==candidate_date):
				next_jobs_id=next_jobs_id+[int(job_id)]

	ordered_jobs_id=ordered_jobs_id+next_jobs_id
	for next_job_id in next_jobs_id:
		jobs2explore.remove(next_job_id)
		jobs2exclude.add(next_job_id)


jobsFile = open(fileName, "w")
jobsFile.write('ID,Job Type,Start Time (UTCG),Stop Time (UTCG),Duration (sec)\n')
i=0
uhf_jobs_id =[]
lband_jobs_id=[]
xband_jobs_id=[]
for job_id in ordered_jobs_id:
	start_time = day_jobs['Start Time (UTCG)'][job_id] 
	stop_time = day_jobs['Stop Time (UTCG)'][job_id]
	job_type = day_jobs['Job Type'][job_id]
	duration = day_jobs['Duration (sec)'][job_id]
	what2write = str(i)+','+job_type+','+start_time+','+stop_time+','+str(duration)+"\n"

	if(job_type=='uhf'):
		uhf_jobs_id = uhf_jobs_id + [i]

	jobsFile.write(what2write)
	i=i+1
jobsFile.close()

ordered_jobs_len = i

#load ordered jobs and remove jobs overlapping with uhf jobs
ordered_jobs = pd.read_csv(fileName, delimiter=',', skiprows=1, names = ['ID','Job Type','Start Time (UTCG)','Stop Time (UTCG)','Duration (sec)'])

print("The following UHF jobs are scheduled:")

for job_type , start_time, stop_time, duration in zip(ordered_jobs['Job Type'], ordered_jobs['Start Time (UTCG)'], ordered_jobs['Stop Time (UTCG)'], ordered_jobs['Duration (sec)'] ):
	if(job_type=='uhf'):
		print("	*) " ,job_type ,' ',start_time,' ',stop_time,' ',duration)


jobs2remove=set()
for uhf_job_id in uhf_jobs_id:
	uhf_time_range = DateTimeRange(ordered_jobs['Start Time (UTCG)'][uhf_job_id], ordered_jobs['Stop Time (UTCG)'][uhf_job_id])
	for job_id,start_time, stop_time in zip(ordered_jobs['ID'],ordered_jobs['Start Time (UTCG)'],ordered_jobs['Stop Time (UTCG)'] ):
		if (job_id not in jobs2remove and job_id not in uhf_jobs_id):
			job_time_range= DateTimeRange(start_time,stop_time)
			if(str(uhf_time_range.intersection(job_time_range))!='NaT - NaT'):
				jobs2remove.add(job_id)


print("The following "+str(len(jobs2remove))+" jobs are impossible to schedule because they overlap with UHF jobs!")
print(round(100*len(jobs2remove)/ordered_jobs_len,2), "% of requested jobs removed!")
for job2remove in jobs2remove:
	start_time = ordered_jobs['Start Time (UTCG)'][job2remove] 
	stop_time = ordered_jobs['Stop Time (UTCG)'][job2remove]
	job_type = ordered_jobs['Job Type'][job2remove]
	duration = ordered_jobs['Duration (sec)'][job2remove]
	job = job_type+','+start_time+','+stop_time+','+str(duration)
	print("  *)", job)


jobsFile = open(fileName, "w")
jobsFile.write('ID,Job Type,Start Time (UTCG),Stop Time (UTCG),Duration (sec)\n')
i=0
for job_id, job_type, start_time, stop_time , duration in zip(ordered_jobs['ID'],ordered_jobs['Job Type'],ordered_jobs['Start Time (UTCG)'],ordered_jobs['Stop Time (UTCG)'], ordered_jobs['Duration (sec)']):
	if job_id not in jobs2remove:
		what2write = str(i)+','+job_type+','+start_time+','+stop_time+','+str(duration)+"\n"
		jobsFile.write(what2write)
		i=i+1
jobsFile.close()
ordered_jobs_len = i


ordered_jobs = pd.read_csv(fileName, delimiter=',', skiprows=1, names = ['ID','Job Type','Start Time (UTCG)','Stop Time (UTCG)','Duration (sec)'])
i=0
# find jobs overlapping with previous jobs 
jobs_overlap_previous = {}
for i in reversed(range(ordered_jobs_len)):
	reference_time_range=DateTimeRange(ordered_jobs['Start Time (UTCG)'][i], ordered_jobs['Stop Time (UTCG)'][i])
	jobs_overlap_previous[i]=set()
	for k in reversed(range(0,i)):
		job_time_range= DateTimeRange(ordered_jobs['Start Time (UTCG)'][k],ordered_jobs['Stop Time (UTCG)'][k])
		range_intersection = reference_time_range.intersection(job_time_range)
		if(str(range_intersection)!='NaT - NaT'):
			jobs_overlap_previous[i].add(k)



# find insulation
sunFileName = 'SUN'+str(select_year)+"_"+str(select_month)+"_"+str(select_day)+'.txt'
sunFile = open(sunFileName, "w")
sunFile.write('ID, Job Type,Start Time (UTCG),Stop Time (UTCG),Duration (sec)\n')
getJobByDate(sun,0,'sun' ,select_year, select_month, select_day, sunFile)
sunFile.close()

sun = pd.read_csv(sunFileName, delimiter=',', skiprows=1, names = ['ID','Job Type','Start Time (UTCG)','Stop Time (UTCG)','Duration (sec)'])
jobs_time_insulation = ordered_jobs_len*[0]
for sun_start, sun_stop in zip(sun['Start Time (UTCG)'], sun['Stop Time (UTCG)']):
	sun_time_range = DateTimeRange(sun_start, sun_stop)
	for job_id, job_start, job_stop in zip(ordered_jobs['ID'],ordered_jobs['Start Time (UTCG)'],ordered_jobs['Stop Time (UTCG)']):
		job_time_range = DateTimeRange(job_start, job_stop)
		intersection = job_time_range.intersection(sun_time_range)
		if(str(intersection)!='NaT - NaT'):
			datetimeFormat = '%Y-%m-%dT%H:%M:%S'
			start_intersection = str(intersection).split(' - ')[0]
			stop_intersection = str(intersection).split(' - ')[1]
			diff = datetime.datetime.strptime(stop_intersection, datetimeFormat)- datetime.datetime.strptime(start_intersection, datetimeFormat)
			jobs_time_insulation[job_id] = jobs_time_insulation[job_id] + diff.seconds


jobs_time_insulation=list(map(round, jobs_time_insulation))

#find relative start and stop time and duration
begin_of_time = ordered_jobs['Start Time (UTCG)'][0]
relative_start_time = ordered_jobs_len*[0]
relative_stop_time = ordered_jobs_len*[0]
jobs_duration = ordered_jobs_len*[0]
datetimeFormat = '%Y/%m/%d %H:%M:%S.%f'
for job_id, start_time, stop_time in zip(ordered_jobs['ID'],ordered_jobs['Start Time (UTCG)'], ordered_jobs['Stop Time (UTCG)']):
	duration = datetime.datetime.strptime(stop_time, datetimeFormat)- datetime.datetime.strptime(start_time, datetimeFormat)
	diff1 = datetime.datetime.strptime(start_time, datetimeFormat)- datetime.datetime.strptime(begin_of_time, datetimeFormat)
	diff2 = datetime.datetime.strptime(stop_time, datetimeFormat)- datetime.datetime.strptime(begin_of_time, datetimeFormat)
	relative_start_time[job_id] = round(diff1.seconds)
	relative_stop_time[job_id] = round(diff2.seconds)
	jobs_duration[job_id] = round(duration.seconds)


#find time exposed to sun till start_job and time exposed to sun till stop_job

exposed2SunTillStartJob = ordered_jobs_len*[0]
exposed2SunTillStopJob = ordered_jobs_len*[0]

for job_id, start_time, in zip(ordered_jobs['ID'],ordered_jobs['Start Time (UTCG)']):
	time_range = DateTimeRange(begin_of_time, start_time)
	total_time_exposed2sun = 0
	for sun_start, sun_stop in zip(sun['Start Time (UTCG)'], sun['Stop Time (UTCG)']):
		sun_time_range= DateTimeRange(sun_start, sun_stop)
		intersection = time_range.intersection(sun_time_range)
		if(str(intersection)!='NaT - NaT'):
			datetimeFormat = '%Y-%m-%dT%H:%M:%S'
			start_intersection = str(intersection).split(' - ')[0]
			stop_intersection = str(intersection).split(' - ')[1]
			diff = datetime.datetime.strptime(stop_intersection, datetimeFormat)- datetime.datetime.strptime(start_intersection, datetimeFormat)
			total_time_exposed2sun = total_time_exposed2sun + diff.seconds

	exposed2SunTillStartJob[job_id] = total_time_exposed2sun
			

for job_id, stop_time, in zip(ordered_jobs['ID'],ordered_jobs['Stop Time (UTCG)']):
	time_range = DateTimeRange(begin_of_time, stop_time)
	total_time_exposed2sun = 0
	for sun_start, sun_stop in zip(sun['Start Time (UTCG)'], sun['Stop Time (UTCG)']):
		sun_time_range= DateTimeRange(sun_start, sun_stop)
		intersection = time_range.intersection(sun_time_range)
		if(str(intersection)!='NaT - NaT'):
			datetimeFormat = '%Y-%m-%dT%H:%M:%S'
			start_intersection = str(intersection).split(' - ')[0]
			stop_intersection = str(intersection).split(' - ')[1]
			diff = datetime.datetime.strptime(stop_intersection, datetimeFormat)- datetime.datetime.strptime(start_intersection, datetimeFormat)
			total_time_exposed2sun = total_time_exposed2sun + diff.seconds

	exposed2SunTillStopJob[job_id] = total_time_exposed2sun


exposed2SunTillStartJob =list(map(round, exposed2SunTillStartJob))
exposed2SunTillStopJob=list(map(round, exposed2SunTillStopJob))

jobsFile = open(fileName, "w")
jobsFile.write('ID, Job Type,Start Time (UTCG),Stop Time (UTCG),Relative Start Time(s) ,Relative Stop Time(s), Duration (sec),Exposed to sun till start job(sec),Exposed to sun till stop job(sec) ,Insulation Duration in Exec (sec)  , Skip If, Job Energy [mJ]\n')
for job_id, job_type, start_time, stop_time in zip(ordered_jobs['ID'],ordered_jobs['Job Type'],ordered_jobs['Start Time (UTCG)'],ordered_jobs['Stop Time (UTCG)']):
	what2write = str(job_id)+','+job_type+','+start_time+','+stop_time+','+str(relative_start_time[job_id])+','+ str(relative_stop_time[job_id])+','+str(jobs_duration[job_id])+','+str(exposed2SunTillStartJob[job_id])+','+str(exposed2SunTillStopJob[job_id])+','+str(jobs_time_insulation[job_id])+','+nicePrintSet(jobs_overlap_previous[job_id])
	job_energy = jobEnergy(job_type, relative_start_time[job_id], relative_stop_time[job_id], exposed2SunTillStartJob[job_id], exposed2SunTillStopJob[job_id])
	what2write= what2write +','+str(job_energy)+"\n"
	jobsFile.write(what2write)

jobsFile.close()


#generate modest file
fileNameModest = str(select_year)+"_"+str(select_month)+"_"+str(select_day)+'.modest'
fout = open(fileNameModest, "wt")
#Create the header of the Modest model
fin = open("./modest_template/header.template", "rt")
keywords = ['#GLOBAL_BPOWER#' , '#GLOBAL_RPOWER#' , '#MAX_BATTERY_CAPACITY#', '#MIN_BATTERY_CAPACITY#', '#START_BATTERY_CAPACITY#']
keywords_value = [str(round(GLOBAL_BPOWER)) , str(round(GLOBAL_RPOWER)) , str(round(MAX_BATTERY_CAPACITY)), str(round(MIN_BATTERY_CAPACITY)), str(round(START_BATTERY_CAPACITY))]
replaceinFile(fin, fout, keywords, keywords_value)

#load jobs in modest
ordered_jobs = pd.read_csv(fileName, delimiter=',', skiprows=1, names = ['ID' ,'Job Type','Start Time (UTCG)','Stop Time (UTCG)','Relative Start Time(s)' ,'Relative Stop Time(s)', 'Duration (sec)', 'Exposed to sun till start job(sec)','Exposed to sun till stop job(sec)' ,'Insulation Duration in Exec (sec)'  , 'Skip If', 'Job Energy [mJ]'])

#kewords 2 replace
#%start_job% %end_job% %sun_start_job% %sun_end_job% %next_sun_start_job% %next_start_job% %job_id% %jobenergy%

#ordered_jobs_len= GLOBAL_JOBS2SCHEDULE


keywords =[ '#start_job#' , '#end_job#' , '#sun_start_job#', '#sun_end_job#', '#next_sun_start_job#', '#next_start_job#' , '#job_id#', '#jobenergy#', '#can_skip#']

for i in range(ordered_jobs_len-1): #we want to exclude the last job
	fin = open("./modest_template/job.template", "rt")
	start_job = str(ordered_jobs['Relative Start Time(s)'][i])
	end_job = str(ordered_jobs['Relative Stop Time(s)'][i])

	sun_start_job = str(ordered_jobs['Exposed to sun till start job(sec)'][i])
	sun_end_job = str(ordered_jobs['Exposed to sun till stop job(sec)'][i])
	
	next_sun_start_job = str(ordered_jobs['Exposed to sun till start job(sec)'][i+1])
	
	next_start_job = str(ordered_jobs['Relative Start Time(s)'][i+1])
	
	job_id = str(ordered_jobs['ID'][i])
	jobenergy = str(ordered_jobs['Job Energy [mJ]'][i])

	if (ordered_jobs['Job Type'][i]=="uhf"):
		can_skip="false"
	else:
		can_skip="true"

	keywords_value =[start_job,end_job, sun_start_job, sun_end_job, next_sun_start_job, next_start_job , job_id, jobenergy, can_skip]
	replaceinFile(fin, fout, keywords, keywords_value)
	fin.close()

#load the last job in modest
fin = open("./modest_template/lastjob.template", "rt")
keywords =[ '#start_job#' , '#end_job#' , '#sun_start_job#', '#sun_end_job#', '#job_id#', '#jobenergy#', '#can_skip#']
i=ordered_jobs_len-1

start_job = str(ordered_jobs['Relative Start Time(s)'][i])
end_job = str(ordered_jobs['Relative Stop Time(s)'][i])

sun_start_job = str(ordered_jobs['Exposed to sun till start job(sec)'][i])
sun_end_job = str(ordered_jobs['Exposed to sun till stop job(sec)'][i])

job_id = str(ordered_jobs['ID'][i])
jobenergy = str(ordered_jobs['Job Energy [mJ]'][i])

if (ordered_jobs['Job Type'][i]=="uhf"):
	can_skip="false"
else:
	can_skip="true"

keywords_value =[start_job,end_job, sun_start_job, sun_end_job, job_id, jobenergy, can_skip]
replaceinFile(fin, fout, keywords, keywords_value)
fin.close()
fout.close()