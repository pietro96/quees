def copyfile(fin, fout):
	for line in fin:
		fout.write(line)

def replaceinFile(fin, fout, keywords, keywords_value):
	for line in fin:
		newline =line
		for (keyword, keyword_value) in zip(keywords, keywords_value):
			newline = newline.replace(keyword, keyword_value)
		fout.write(newline)

fout = open("demo.modest", "wt")

fin = open("header.template", "rt")
copyfile(fin, fout)

fin = open("job.template", "rt")
keywords = ['%job_id%', '%jobenergy%', '%start_job%']
keywords_value = ['0', '4342', '333']

replaceinFile(fin, fout, keywords, keywords_value)

fin = open("lastjob.template", "rt")
keywords = ['%job_id%', '%jobenergy%', '%start_job%']
keywords_value = ['1', '1342', '133']
replaceinFile(fin, fout, keywords, keywords_value)

fin.close()
fout.close()
