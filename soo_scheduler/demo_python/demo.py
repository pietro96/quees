import re

state='(endOfday = True, battery = 68417489, t = 71839, ts = 46414, IDofLastJob = 21, jobDone = 10, Main_location = 78,)'
state_variables = ['endOfday', 'battery', 't', 'ts', 'IDofLastJob', 'jobDone', 'Main_location']

for state_variable in state_variables:
	value = re.search(state_variable+' =(.*),', state).group(1)
	value=value.split(',')[0]
	print(state_variable,': ',value)
