# Requires Python 3.7 or newer
import sys
from importlib import util
import os
import random
import time
import re
import numpy
import matplotlib.pyplot as plt

def explore_neighbours(current_state, transitions, visited_states, states_queue, reward_exps = [], reward_dictionary={}):
	add2_queue = set()
	states_queue = set(states_queue)
	for transition in transitions:
		transition_reward_exps = reward_exps.copy()
		next_state = network.jump_np(current_state, transition, transition_reward_exps)
		add2_queue.add(next_state)
		reward_dictionary.update({next_state:transition_reward_exps})

	keys2remove = set(add2_queue.intersection(visited_states) | add2_queue.intersection(states_queue))
	list(map(reward_dictionary.pop, keys2remove))
	add2_queue = add2_queue - add2_queue.intersection(visited_states) - add2_queue.intersection(states_queue)
	return list(add2_queue)


def rw_expressions(properties):
	rw_exps = []
	for prop in properties:
		is_reward = prop.exp is not None and prop.exp.op == "xmin" and prop.exp.args[1].op == "ap"
		if is_reward:
			rw_exps = rw_exps+[prop.exp.args[0]]
	return rw_exps

# get a reward for a path
def path_rw(path,reward_dictionary): # path_rw is Deprecated (for DEBUG ONLY)
	list_of_rewards = []
	for i in range(len(path)-1):
		list_of_rewards.append(reward_dictionary[(path[i],path[i+1])])
	return [sum(x) for x in zip(*list_of_rewards)]


def properties_dictionary(properties):
	properties_dictionary=[]
	reward_cnt = 0 
	for prop in properties:
		is_eg_prop = prop.exp.op == "exists" and prop.exp.args[0].op == "always"
		is_ef_prop = prop.exp.op == "exists" and prop.exp.args[0].op == "eventually"
		is_reward = prop.exp is not None and prop.exp.op == "xmin" and prop.exp.args[1].op == "ap"
		prop_dic = {'text':prop ,'ap':None , 'type':None, 'ctl_type': None, 'reward_id':None}
		if(is_reward):
			reward_cnt = reward_cnt + 1
			prop_dic['type'] = 'reward'
			prop_dic['ap'] = int(prop.exp.args[1].args[0])
			prop_dic['reward_id'] = reward_cnt 
		else:
			prop_dic['type'] = 'ctl'
			prop_dic['ap'] = int(prop.exp.args[0].args[0].args[0])
			if(is_eg_prop):
				prop_dic['ctl_type'] = 'eg'
			else:
				prop_dic['ctl_type'] = 'ef'

		properties_dictionary.append(prop_dic)
	return properties_dictionary


# get all ap 
def get_ap (properties):
	ap_set=set()
	for prop in properties:
		is_eg_prop = prop.exp.op == "exists" and prop.exp.args[0].op == "always"
		is_ef_prop = prop.exp.op == "exists" and prop.exp.args[0].op == "eventually"
		is_reward = prop.exp is not None and prop.exp.op == "xmin" and prop.exp.args[1].op == "ap"
		if (is_eg_prop or is_ef_prop):
			ap=int(prop.exp.args[0].args[0].args[0])
			ap_set.add(ap)

		if(is_reward):
			ap=int(prop.exp.args[1].args[0])
			ap_set.add(ap)

	ap_set = list(ap_set)
	return ap_set


def reachable_states(initial_state, print_states=False):
	print("[SOO] Exploring reachable  states...")
	current_state=initial_state
	transitions = network.get_transitions(current_state)
	visited_states={current_state}
	current_state_id=1
	state_dictionary =	{
		1: current_state
	}
	reward_dictionary={}
	neighbours_reward_dictionary={}
	transition_dictionary ={}
	rtransition_dictionary={} # reverse transition dictionary
	reward_exps=rw_expressions(network.properties)
	aps = get_ap(network.properties)
	aps_dictionary = {} # dictionary template { apID:{set of states ID where it holds} }
	raps_dictionary = {} # dictionary template { stateID: {set of apID that are true in stateID} }
	for ap in aps: # init ap dictionary
		aps_dictionary[ap] = set()
	neighbours=explore_neighbours(current_state, transitions, visited_states,[], reward_exps, neighbours_reward_dictionary)
	end_states = set() # set of states with NO neighbours
	states_queue=neighbours
	max_state_id = 1 + len(neighbours)

	while (len(states_queue)!=0 or is_last2explore ):		
		raps_dictionary[current_state_id]=set()
		for ap in aps:
			ap_evaluation = network.get_expression_value(current_state, ap)
			if ap_evaluation:
				aps_dictionary[ap].add(current_state_id)
				raps_dictionary[current_state_id].add(ap)


		if (len(transitions) == 0):
			end_states.add(current_state_id)
		for i in range(len(neighbours)):
			state_id=max_state_id-i
			state_dictionary[state_id]=neighbours[i]

			reward_dictionary[(current_state_id, state_id)] = neighbours_reward_dictionary[neighbours[i]]
			try:
				transition_dictionary[current_state_id].add(state_id)
			except:
				transition_dictionary[current_state_id]={state_id}

			try:
				rtransition_dictionary[state_id].add(current_state_id)
			except:
				rtransition_dictionary[state_id]={current_state_id}

		is_last2explore = False
		if(len(states_queue)!=0):
			current_state = states_queue.pop()
			visited_states.add(current_state)
			current_state_id=current_state_id+1
			transitions = network.get_transitions(current_state)
			neighbours_reward_dictionary={}
			neighbours=explore_neighbours(current_state, transitions, visited_states, states_queue, reward_exps, neighbours_reward_dictionary)
			states_queue = neighbours + states_queue
			max_state_id = max_state_id + len(neighbours)
			is_last2explore = len(states_queue)==0

	
	if(print_states):
		for i in range(len(state_dictionary)):
			print("* The current state is:", state_dictionary[i+1])

	return transition_dictionary, rtransition_dictionary, state_dictionary, reward_dictionary, aps_dictionary, raps_dictionary, end_states



# given a state_id (in the following we will refer to this state as target state) 
# the reverese transition dictionary (rtransition_dictionary) & the state dictionary (state_dictionary),
# return a set that contains all possible path to that state
# the idea behind this function is to buil all possible paths to
# a  the target state by READING the reverse transition dictionary in the following way:
# 	1) rtransition_dictionary[target_state_id] returns a set of states which have as neighbour 
#		the targe states. This means that there are at least a number of traces equal to the 
#		number of elements returned by rtransition_dictionary[target_state_id]
#
#   2) By READING the rtransition_dictionary you can find all the states which have as a neighbour 
#		the states prev. found 
#   3) repeat 2) till you arrive at the initial state (state_id =1)



def paths2state(state_id, state_dictionary, rtransition_dictionary, *args, **kwargs):

	reward_dictionary = kwargs.get('reward_dictionary', None) # if reward_dictionary is given a dictionary associated to paths containing array with reward will be returned.
	reward_id = kwargs.get('reward_id', None) 
	ap = kwargs.get('ap', None) # if ap is given all states of all paths should respect ap

	if(state_id !=1):

		if ap and reward_dictionary:
			eg_paths_reward={}
		elif ap:
			eg_paths={}
			z=0 # eg_paths key
		elif reward_dictionary:
			paths_reward={}


		paths2state = {}
		lastadded ={}
		i=-1
		stop_cond = True
		for parent in rtransition_dictionary[state_id]:
			i=i+1
			paths2state[i]={(parent, state_id)}
			lastadded[i] = parent
			if reward_dictionary:
				if reward_id != None:
					paths_reward[i]=reward_dictionary[(parent, state_id)][reward_id]
				else:
					paths_reward[i]=reward_dictionary[(parent, state_id)]
			if (parent != 1):
				stop_cond =False

		while not stop_cond:
			stop_cond = True
			len_paths2state = len(paths2state)
			for j in range(len_paths2state):
				if ap:
					is_path_not_ready = lastadded[j]!=1 and network.get_expression_value(state_dictionary[lastadded[j]], ap)
				else:
					is_path_not_ready = lastadded[j]!=1
				if(is_path_not_ready):
					son = lastadded[j]
					parents = rtransition_dictionary[son]
					k=0
					for parent in parents:
						if reward_dictionary:
							if reward_id != None:
								reward = reward_dictionary[(parent, son)][reward_id]
							else:
								reward = reward_dictionary[(parent, son)]


						if(k!=0):
							i=i+1
							paths2state[i]= paths2state[j].add((parent,son))
							lastadded[i] = parent
							if reward_dictionary:
								if reward_id != None:
									paths_reward[i] = root_reward +reward
								else:
									paths_reward[i]=[x + y for x, y in zip(root_reward, reward)]

							if (eg_paths and parent==1):
								eg_paths[z] = paths2state[i]
								if reward_dictionary:
									eg_paths_reward[z] = paths_reward[i]
								z=z+1

						else:
							paths2state[j].add((parent,son))
							lastadded[j] = parent
							if reward_dictionary:
								if reward_id != None:
									root_reward = paths_reward[j]
									paths_reward[j]=root_reward + reward
								else:
									root_reward = paths_reward[j]
									paths_reward[j]=[x + y for x, y in zip(root_reward, reward)]


							if (ap and parent==1):
								eg_paths[z] = paths2state[j]
								if reward_dictionary:
									eg_paths_reward[z] = paths_reward[j]
								z=z+1
							k=1

						if (parent !=1):
							stop_cond = False

		if ap and reward_dictionary:
			return eg_paths, eg_paths_reward
		elif ap:
			return eg_paths
		elif reward_dictionary:
			return paths2state, paths_reward
		else:
			return paths2state

	else:
		if (ap and reward_dictionary) or reward_dictionary:
			return None, None
		else:
			return None


def tupleremove(n): 
    return n[1] 

def SetFromPath(path):
	path = set(path) 
	result = set(map(tupleremove, path))
	result.add(1)
	return result 


def greater_then(test_list,num):
	res = [] 
	for idx in range(0, len(test_list)) : 
		if test_list[idx] > num: 
			res.append(idx) 
	return res

def less_then(test_list,num):
	res = [] 
	for idx in range(0, len(test_list)) : 
		if test_list[idx] < num: 
			res.append(idx) 
	return res
  


############################ Run Modest Model and gen. graph #################################
# Define Modest installation
modest = "/usr/bin/Modest-linux-x64-20191214/Modest/modest"   # 4th Version
print("[SOO - DEGUG] Modest installation: ", modest)
modest_file=os.path.basename(sys.argv[1])
modest_file_name=os.path.splitext(modest_file)[0]
dot_file = modest_file_name+'.dot'
pdf_file = modest_file_name+'.pdf'
python_file=modest_file_name+'.py'

start_modest = time.time()
os.system(modest+" export-to-dot "+sys.argv[1]+" -O "+dot_file+" -Y --dot pdf "+pdf_file+" --no-time-progress")
os.system(modest+" export-to-python "+sys.argv[1]+" -O "+ python_file +" -Y")
end_modest = time.time()
#############################################################################################

################################## Start Load the model #####################################
if len(sys.argv) < 2:
	print("Error: No model specified.")
	quit()
print("Loading model from \"{0}\"...".format(python_file), end = "", flush = True)
spec = util.spec_from_file_location("model", python_file)
model = util.module_from_spec(spec)
spec.loader.exec_module(model)
network = model.Network() # create network instance
print(" done.")
################################## End Load the model #######################################
initial_state=network.get_initial_state()
start_rstates = time.time()
transition_dictionary, rtransition_dictionary, state_dictionary, reward_dictionary, aps_dictionary, raps_dictionary, end_states=reachable_states(initial_state,True)
end_rstates  = time.time()

print("[SOO] State space exploration done in (s): ", end_rstates - start_rstates)
print("[SOO] N. of states explored", len(state_dictionary) )
print("\n=============== Timing Debug ===================")
print("[SOO] No. of states explored:", len(state_dictionary) )
print("[SOO] state space explored in (s):", end_rstates - start_rstates)
print("===============================================\n")
################################## Start Scheduling #################################################################
print('[SOO] Starting scheduler ... ')
print('[SOO] There are at least ',len(end_states), 'way to schedule jobs')

#load end states inside a dictionary


#for end_state in end_states:
#	print(state_dictionary[end_state])

#get state variables
state_variables = state_dictionary[1]
state_variables = str(state_variables)
state_variables=re.sub(r'=.+?,', '', state_variables)
state_variables=state_variables.replace(')','#')
state_variables=state_variables.replace('(','')
state_variables=re.sub(r'=.+?#', '', state_variables)
state_variables = state_variables.split()

#print(state_variables)
state_dictionary2={}
endstate_dictionary2={}

endstate_dictionary2['#StateID']=[]
state_dictionary2['#StateID']=[]

for state_variable in state_variables:
	state_dictionary2[state_variable]=[]
	endstate_dictionary2[state_variable]=[]

#create result dictionary with array of different state variables
for i in range(len(state_dictionary)):
	state_dictionary2['#StateID']=state_dictionary2['#StateID'] + [i+1]
	if (i+1) in end_states:
		endstate_dictionary2['#StateID']=endstate_dictionary2['#StateID'] + [i+1]
	state = str(state_dictionary[i+1])
	state= state.replace(')',',)')
	for state_variable in state_variables:
		value = re.search(state_variable+' =(.*),', state).group(1)
		value=value.split(',')[0]
		if(value.strip().isdigit()):
			value=int(value)
		state_dictionary2[state_variable]=state_dictionary2[state_variable] + [value]
		if (i+1) in end_states:
			endstate_dictionary2[state_variable]=endstate_dictionary2[state_variable] + [value]


#jobDone
main_stop_condition = False
while(not main_stop_condition):
	indexOfMaxjobDone=numpy.argmax(endstate_dictionary2['jobDone'])
	#check if the selected end state is valid
	if(endstate_dictionary2['endOfday'][indexOfMaxjobDone].strip()!='True'):
		stop_cond = endstate_dictionary2['endOfday'][indexOfMaxjobDone].strip()=='True'
		while (len(endstate_dictionary2['jobDone'])!=0 and (not stop_cond)):
			endstate_dictionary2['#StateID'].remove(indexOfMaxjobDone)
			for state_variable in state_variables:
				endstate_dictionary2[state_variable].remove(indexOfMaxjobDone)

			indexOfMaxjobDone=numpy.argmax(endstate_dictionary2['jobDone'])
			stop_cond = endstate_dictionary2['endOfday'][indexOfMaxjobDone].strip()=='True'

		if(len(endstate_dictionary2['jobDone'])==0):
			indexOfMaxjobDone=None
			main_stop_condition = True

	if(indexOfMaxjobDone):
		executed_jobs_id = set()
		selected_state_id = endstate_dictionary2['#StateID'][indexOfMaxjobDone]
		print("[SOO] The state id of this end state is" ,selected_state_id, ":: ", state_dictionary[selected_state_id])
		paths=paths2state(selected_state_id, state_dictionary, rtransition_dictionary)
		path = paths[0] #we select the first path
		path = SetFromPath(path)
		battery_below_limit = False
		battery_level = []
		relative_time = []
		relative_time_sun = []
		for state_id in path:
			if(int(state_dictionary2['battery'][state_id-1]) >= 59904000):
				if(str(state_dictionary2['IDofLastJob'][state_id-1]).strip()!='-1'):
					executed_jobs_id.add(state_dictionary2['IDofLastJob'][state_id-1])
					battery_level.append(int(state_dictionary2['battery'][state_id-1]))
					relative_time.append(int(state_dictionary2['t'][state_id-1]))
					relative_time_sun.append(int(state_dictionary2['ts'][state_id-1]))
			else:
				battery_below_limit = True
				endstate_dictionary2['#StateID'].remove(indexOfMaxjobDone)
				for state_variable in state_variables:
					endstate_dictionary2[state_variable].remove(indexOfMaxjobDone)
				break

		if(not battery_below_limit):
			main_stop_condition = True



if(indexOfMaxjobDone):
	print("[SOO] Maximum jobs that can be scheduled: ",endstate_dictionary2['jobDone'][indexOfMaxjobDone])
	print('[SOO] Jobs id fulfilling energy requirements:')
	print('[SOO]', executed_jobs_id)
	danger_level = [59904000]*len(relative_time)
	plt.scatter(relative_time, battery_level, linestyle='-', marker='o')
	plt.plot(relative_time,danger_level,linestyle='-', color='red')
	plt.title('Battery Capacity over time')
	plt.xlabel('Relative time [s]')
	plt.ylabel('Energy [mJ]')
	#plt.xscale('log')
	plt.savefig('energy.pdf')
	print('[SOO] Max battery level [mJ]: ',max(battery_level))
	print('[SOO] Min battery level [mJ]: ',min(battery_level))
	#create a file with the scheduler
	f = open("scheduler.txt", "w")
	f.write(str(executed_jobs_id))
	f.close()

else:
	print('[SOO] A scheduler could not be found')





sys.exit()
#DEGUG TRACE
print(":::::::::::::::::::::")
for state_id in path:
	print('state_id :', state_id ,state_dictionary[state_id])
	transitions = network.get_transitions(state_dictionary[state_id])
	for transition in transitions:
		print('		transition :', network.jump_np(state_dictionary[state_id], transition))

print("*******************")
print(state_dictionary[selected_state_id])
print("*******************")

print(len(greater_then(state_dictionary2['battery'],119808000)))
print(len(less_then(state_dictionary2['battery'],59904000)))