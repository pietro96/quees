import matplotlib.pyplot as plt
#https://matplotlib.org/examples/pylab_examples/broken_barh.html
fig, ax = plt.subplots()
ax.broken_barh([(110, 30), (150, 10)], (10, 9), facecolors='blue')
ax.broken_barh([(10, 50), (100, 20), (130, 10)], (20, 9),
               facecolors=('red', 'yellow', 'green'))
ax.set_ylim(5, 35)
ax.set_xlim(0, 200)
ax.set_xlabel('seconds since start')
ax.set_yticks([15, 25])
ax.set_yticklabels(['LBand', 'XBand'])
ax.grid(True)


plt.savefig('demo.pdf')
