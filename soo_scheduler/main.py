import os 
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--day")
parser.add_argument("--month")
parser.add_argument("--year")
args = parser.parse_args()
print("Selected day: ",args.year,'/',args.month,'/',args.day)
os.system("python scheduler.py --day "+args.day+" --month "+args.month+" --year "+args.year)
fileNameModest = args.year+"_"+args.month+"_"+args.day+'.modest'
os.system("python soo.py "+fileNameModest)
os.system("python scheduler_plot.py --day "+args.day+" --month "+args.month+" --year "+args.year)