import matplotlib.pyplot as plt
import pandas as pd
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--day")
parser.add_argument("--month")
parser.add_argument("--year")
args = parser.parse_args()

select_year = args.year
select_month = args.month
select_day = args.day

fileName = select_year+"_"+select_month+"_"+select_day+'.csv'
ordered_jobs = pd.read_csv(fileName, delimiter=',', skiprows=1, names = ['ID','Job Type','Start Time (UTCG)','Stop Time (UTCG)','Relative Start Time(s)','Relative Stop Time(s)','Duration (sec)','Exposed to sun till start job(sec)','Exposed to sun till stop job(sec)','Insulation Duration in Exec (sec)','Skip If','Job Energy [mJ]'
])

scheduler_file = open("scheduler.txt", "r")
scheduler=""
for line in scheduler_file:
	scheduler=scheduler+line
scheduler_file.close()
scheduler = scheduler.replace('{','')
scheduler = scheduler.replace('}','')
scheduler=scheduler.split(',')
scheduler=list(map(int, scheduler))

xband_barh=[]
lband2_barh=[]
lband3_barh=[]
uhf_barh=[]

for job_id, job_type, relative_start, duration in zip(ordered_jobs['ID'], ordered_jobs['Job Type'], ordered_jobs['Relative Start Time(s)'], ordered_jobs['Duration (sec)']):
	if job_id in scheduler:
		if job_type=="lband2":
			lband2_barh.append((relative_start, duration))

		if job_type=="lband3":
			lband3_barh.append((relative_start, duration))

		if job_type=="xband":
			xband_barh.append((relative_start, duration))

		if job_type=="uhf":
			uhf_barh.append((relative_start, duration))

#https://matplotlib.org/examples/pylab_examples/broken_barh.html
fig, ax = plt.subplots()
ax.broken_barh(uhf_barh, (5, 2.5), facecolors='blue') #uhf
ax.broken_barh(lband2_barh, (10, 2.5), facecolors=('orange')) #lband2
ax.broken_barh( lband3_barh, (15, 2.5), facecolors='green') #lband3
ax.broken_barh(xband_barh, (20, 2.5), facecolors=('purple')) # xband

#[(10, 50), (100, 20), (130, 10)]

#ax.set_ylim(5, 35)
#ax.set_xlim(0, 200)
ax.set_xlabel('Time since start ('+str(ordered_jobs['Start Time (UTCG)'][0])+') [s]')
ax.set_yticks([5, 10,15,20])
ax.set_yticklabels(['UHF', 'LBand2','LBand3', 'XBand'])
ax.grid(False)
plt.title("Jobs Schedule for: "+select_year+"/"+select_month+"/"+select_day )
plt.savefig(select_year+"_"+select_month+"_"+select_day+'Graph.pdf')
