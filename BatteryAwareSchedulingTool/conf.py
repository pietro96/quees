import datetime as dt
import pandas as pd
import argparse
import tools

p = argparse.ArgumentParser(
		description = 'A Demo of the GomX-3 scheduling tool',
		formatter_class=argparse.ArgumentDefaultsHelpFormatter
	)
p.add_argument('--start', type = str, default = '20/03/2016 07:00:00',
help = "The starting point of the scheduling interval in UTC of the form 'DD/MM/YYYY hh:mm:ss'. >>> Mind the quotes <<<")

p.add_argument('--end', type = str, default = '21/03/2016 07:00:00',
help = "The end point of the scheduling interval in UTC of the form 'DD/MM/YYYY hh:mm:ss'. >>> Mind the quotes <<<")

p.add_argument('--tables', type = str, default = './tables/',
help = 'Directory of the operational window CSV tables')

p.add_argument('--export', type = str, default = './output/',
help = 'Directory of where the filled out CSV tables and the plots are saved to')

p.add_argument('--binaries', type = str, default = './binaries/',
help = 'The tool will search for the Uppaal Cora executable (verifyta) as well as the StoKiBaM tool executable (KibamScheduleRunner) in this directory.')

p.add_argument('--model-file', type = str, default = './models/gomx3.xta',
help = 'Path to the model instance file. If --temporary is set, this will ignored')

p.add_argument('--query-file', type = str, default = './models/gomx3.q',
help = 'Path to the query instance file. If --temporary is set, this will ignored')

p.add_argument('--model-tmpl', type = str, default ='./templates/gomx3.mtmpl',
help = 'Path to the model template file')

p.add_argument('--query-tmpl', type = str, default ='./templates/gomx3.qtmpl',
help = 'Path to the query template file')

p.add_argument('--capacity', type = int, default = 149760000,
help = 'The capacity of the battery in mJ/s')

p.add_argument('--init-soc', type = float, default = 0.9,
help = 'The initial SoC factor. Initial SoC will be capacity * init-soc')

p.add_argument('--cora-safe-mode', type = float, default = 0.55,
help = 'The safe mode threshold factor used in Uppaal Cora. Safe Mode threshold will be capacity * cora-safe-mode')

p.add_argument('--rem-soc', type = float, default = 0.6,
help = 'The factor dictating how much charge should remain at the end of the scheduling horizon. Remaining SoC at the end of the scheduling horizon will be at least capacity * rem-soc')

p.add_argument('--soc-penalty-factor', type = int, default = 0,
help = 'In the last model checking step, penalize a low SoC level with penalty (capacity - soc) / soc-penalty-factor. Set to 0 to disable.')

p.add_argument('--no-ratios', action = 'store_true',
help = 'Disables the ratio-heuristic. If set, --ratio-L and --ratio-X will have no effect.')

p.add_argument('--no-force-take', action = 'store_true',
help = 'Disables the take-if-almost-full heuristic.')

p.add_argument('--ratio-X', type = int, default = 2,
help = 'X-Band part of the heuristic guiding the ratio between X-Band and L-Band jobs.')

p.add_argument('--ratio-L', type = int, default = 1,
help = 'L-Band part of the heuristic guiding the ratio between X-Band and L-Band jobs.')

p.add_argument('--max-load', type = int, default = 15000,
help = "Heuristic limiting the maximal load on the battery allowed. To 'disable' this, set to a high value, like 50000 mJ/s")

p.add_argument('--kibam-safe-mode', type = float, default = 0.4,
help = 'The safe mode threshold factor used in the StoKiBaM validation. Safe Mode threshold will be capacity * kibam-safe-mode')

p.add_argument('--discretization', type = int, default = 1000,
help = 'The discretization rate of the StoKiBaM tool. The tool will use matrices of the size discretization X discretization. Thus higher is more precise, but also slower and more memory-heavy.')

p.add_argument('--c', type = float, default = 0.5, choices = [0.5],
help = 'Width of the available charge well')

p.add_argument('--p', type = float, default = 0.00002,
help = 'Diffusion constant of the KiBaM')

p.add_argument('--depl-thresh', type = float, default = 0.1,
help = 'The acceptance threshold of the validation step. Set to value greater than 1 to always pass.')

p.add_argument('--temporary', action = 'store_true',
help = 'A flag saying the instantiated models and intermediate file should be stored in memory to reduce disk-IO')

## Do some sanity checks
def sanity_check():
	if param.rem_soc < 0 or param.rem_soc > 1:
		p.error("remaining SoC factor must be between 0 and 1")
	if not param.rem_soc >= param.cora_safe_mode :
		p.error("--rem-soc must not be less than --cora-safe-mode")
	if param.depl_thresh < 0:
		p.error("Schedule acceptance condition --depl-thresh must be strictly greater than 0.")
	if param.max_load < 5700 and param.no_force_take:
		p.error("The maximal load heuristic must be invoked with integers of at least 5700 mJ/s such that UHF-jobs and the background load can be supported if --no_force_take is set")
	if param.max_load < 6900 and not param.no_force_take:
		p.error("The maximal load heuristic must be invoked with integers of at least 6900 mJ/s such that forced jobs due to full battery can be supported. Either increase the value or set --no-force-take")
	if param.c <= 0 or param.c >= 1:
		p.error("The available charge width must be a float between 0 and 1")
	if param.p <= 0 :
		p.error("The KiBaM diffusion rate must be positive (in mJ/s)")
	if param.discretization <= 0 :
		p.error("The discretization factor must be strictly positive (> 0). Recommended is at least 500.")
	if not param.init_soc >= param.cora_safe_mode :
		p.error("Initial Uppaal Cora SoC factor must be greater than safe mode factor --cora-safe-mode")
	if not param.init_soc >= param.kibam_safe_mode :
		p.error("Initial StoKiBaM SoC factor must be greater than safe mode factor --kibam-safe-mode")
	if param.init_soc > 1:
		p.error("Initial SoC factor must be between --safe-mode and 1")
	if param.capacity < 0:
		p.error("The battery capacity must of course be positive (in mJ/s)")
	if param.model_tmpl == param.model_file:
		p.error("--model-tmpl and --model-file paths must be different")
	if param.query_tmpl == param.query_file:
		p.error("--query-tmpl and --query-file must be different paths")
	if param.tables == param.export:
		p.error("--tables and --export must be different directories")
	if tools.unix(param.start) > tools.unix(param.end):
		p.error("--end must be a strictly later date than --start")
	if param.ratio_X <= 0:
		p.error("--ratio-X must be strictly greater than 0.")
	if param.ratio_L <= 0:
		p.error("--ratio-L must be strictly greater than 0.")
	if param.soc_penalty_factor < 0:
		p.error('--soc-penalty-factor must be strictly positive.')


param = {
	# 'start' 			: '20/03/2016 07:00:00',
	# 'end'				: '21/03/2016 07:00:00',
# #	'start' 			: '20/03/2016 07:00:00',
# #	'end'				: '21/03/2016 19:00:00',
	# 'import'			: './tables/',
	# 'export'			: './output/',
# #	'export'			: '/tmp/',
	# 'model tmpl'	: './templates/gomx3.mtmpl',
	# 'query tmpl'	: './templates/gomx3.qtmpl',
	# 'model file'	: './models/gomx3.xta',
	# 'query file'	: './models/gomx3.q',
	# 'temporary' 	: False,
# #	'cora seed' 	: 1511348176,
	# 'capacity'		: 149760000, # mJ
	# 'init. SoC'		: 0.9, # initial SoC factor
	# 'safe mode'		: 0.55, # 'capacity' * 'safe mode' is safemode threshold
	# 'rem. SoC'		: 0.6, # capacity * rem. SoC needs to remain
	# 'max. load'		: 15000, # mJ/s -- this is a heuristic
	# 'disc. rate'	: 400,
	# 'c'				: 0.5,
	# 'p'				: 0.00005,
	# 'depl. thresh.': 0.01, # 'capacity' * 'safe mode' is safemode threshold
}

#param['between'] = pd.date_range(param['start'], param['end'], freq = '12H')

times = {}


def dump(dict):
	return '\n'.join(k + ': ' + str(v) for k,v in dict.items())
