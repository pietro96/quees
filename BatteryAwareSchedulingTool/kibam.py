from numpy import arange, amin, amax, logical_and, logical_not,isinf, isfinite, zeros, array, multiply
from pandas import read_table, DataFrame, read_csv
from sys import exit
from subprocess import CalledProcessError, STDOUT, Popen, PIPE
from tempfile import NamedTemporaryFile
import tools
import conf
'''
C++ Variables
	std::string source = argv[1];
	std::string dest = argv[2];
	int capacity = std::stoi(argv[3]);
	double initSoC = std::stod(argv[4]);
	double c = std::stod(argv[5]);
	double p = std::stod(argv[6]);
	double drate = std::stod(argv[7]);
'''
def validate(data):
	now = tools.start_timer() # start timer

	df1,_ = tools.filter(data,'gc', which = 'first')
	df2,mask2 = tools.filter(df1,'B.load', which = 'last')
	df2.loc[:,'gc'] = df2.loc[:,'gc'].diff()
	tmp = NamedTemporaryFile(delete=False,mode='w')
	df2.iloc[1:].T.astype(int).to_csv(
		path_or_buf = tmp.name,
		sep = ',',
		index = False,
		float_format = '%.3f'
	)
	tmp.close()

	try:
		Popen([
			conf.param.binaries + 'KibamScheduleRunner',
			tmp.name, #1
			'/tmp/' if conf.param.temporary else conf.param.export, #2
			str(conf.param.capacity), #3
			str(conf.param.init_soc), #4
			str(conf.param.c), #5
			str(conf.param.p), #6
			str(conf.param.discretization), #7
			str(conf.param.kibam_safe_mode)  #8
		], stderr=PIPE).communicate()

		from os import unlink
		unlink(tmp.name)

		conf.times['Validation'] = tools.stop_timer(now) # stop timer

		return (
			('/tmp/' if conf.param.temporary else conf.param.export) + 'full.csv',
			('/tmp/' if conf.param.temporary else conf.param.export) + 'Matrix.csv',
			('/tmp/' if conf.param.temporary else conf.param.export) + 'dead.csv',
			df1[mask2]
		)

	except CalledProcessError as e:
		print('KiBaM tool failed with exit code:' + str(e.returncode))
		exit(e.output.decode("utf-8"))


def densFull(ax, fname, delta):
	import matplotlib.pyplot as plt
	import warnings
	warnings.filterwarnings("ignore", module="matplotlib")

	data = read_csv(fname,dtype=float,squeeze=True,header=None)
	#data[isinf(data)] = data[isfinite(data)].min

	full, = ax.plot(data,lw=2)
	size = data.shape[0]
	sm = conf.param.cora_safe_mode
	ax.set(
		adjustable = 'box-forced',
		xlim = [0,size],
		xticks = [a * size for a in arange(0.1,1,0.1)],
		xticklabels = [str(round(a,1)) for a in arange(0.1,1,0.1)],
		ylim = [amin(data),amax(data)],
		yticks = [amin(data),amax(data)]
	)
	ax.tick_params(labelsize = 6)
	return full

def densDead(ax, fname, delta, cm, cb):
	import matplotlib.pyplot as plt

	data = read_csv(fname,dtype=float,squeeze=True,header=None)
	#data[isinf(data)] = data[isfinite(data)].min
	sumdead = sum(data)
	depletion = sumdead * (delta**2)
	rel_depl = depletion*cb.get_clim()[1]
	dead = ax.imshow(
		zeros((2,2)) + rel_depl,
		cmap = cm,
		vmin = cb.get_clim()[0],
		vmax = cb.get_clim()[1],
		aspect = 'auto')
	ax.set(
		adjustable = 'box-forced',
		xticks = [],
		ylim = [0,1.5],
		yticks = [],
		xlabel= 'bound $\\rightarrow$'
	)

	# if cb.get_clim()[1] - depletion < depletion - cb.get_clim()[0]:
		# color = 'white'
	# else:
		# color = "black"

	# if depletion > cb.get_clim()[1]: color = 'white'
	# if depletion < cb.get_clim()[0]: color = 'black'

	color = 'white' if depletion > 0.5 else 'black'

	ax.text(0.5,0.5, str(round(depletion*100,3)) + ' \\%',
		color=color,
		ha='center',
		va='center',
		transform = ax.transAxes
	)
	return depletion,dead

def densBetween(ax, fname, delta, cm):
	import matplotlib.pyplot as plt

	data = read_csv(fname,dtype=float,squeeze=True,header=None)
	#data[isinf(data)] = data[isfinite(data)].min

	between = ax.imshow(
		multiply(data,delta**2),
		aspect = 'auto',
		origin ="lower",
		cmap = cm,
	)
	size = data.shape[0]
	sm = conf.param.cora_safe_mode
	ax.set(
		adjustable = 'box-forced',
		xlim = [0,size],
		ylim = [0,size],
		xticks = [a * size for a in arange(0.1,1,0.1)],
		yticks = [a * size for a in arange(0.1,1,0.1)],
		#xticklabels = arange(0,1,0.1),
		xticklabels = [str(round(a,1)) for a in arange(0.1,1,0.1)],
		#yticklabels = arange(0,1,0.1),
		yticklabels = [str(round(a,1)) for a in arange(0.1,1,0.1)],
		ylabel= 'available $\\rightarrow$'
	)
	ax.tick_params(labelsize = 6)
	return between

def cbar(ax, between):
	import matplotlib.pyplot as plt

	cb = plt.colorbar(between, cax = ax)
	ax.yaxis.get_offset_text().set_position((2, 5))
	cb.solids.set_rasterized(True)
	ax.set(adjustable='box-forced')
	return cb

def density(full, between, dead):
	import matplotlib.pyplot as plt
	from matplotlib.gridspec import GridSpec

	print('Plotting SoC density.')

	fig = plt.figure(figsize=[9,9])
	gs = GridSpec(3, 2, width_ratios=[8,1], wspace=0.025,hspace=0.05, height_ratios=[1,8,1])

	cm = plt.cm.get_cmap('hot_r')
	delta = (conf.param.capacity * conf.param.c * (1-conf.param.cora_safe_mode))/conf.param.discretization

	densFull(plt.subplot(gs[0,0]), full, delta)
	b = densBetween(plt.subplot(gs[1,0]), between, delta, cm)
	c = cbar(plt.subplot(gs[1,1]), b)
	(depletion,_) = densDead(plt.subplot(gs[2,0]), dead, delta, cm, c)

	return depletion,fig
