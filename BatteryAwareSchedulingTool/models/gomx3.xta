/*
start: 20/03/2016 07:00:00
end: 21/03/2016 07:00:00
tables: ./tables/
export: ./output/
binaries: ./binaries/
model_file: ./models/gomx3.xta
query_file: ./models/gomx3.q
model_tmpl: ./templates/gomx3.mtmpl
query_tmpl: ./templates/gomx3.qtmpl
capacity: 149760000
init_soc: 0.9
cora_safe_mode: 0.55
rem_soc: 0.6
soc_penalty_factor: 0
no_ratios: False
no_force_take: False
ratio_X: 2
ratio_L: 1
max_load: 15000
kibam_safe_mode: 0.4
discretization: 1000
c: 0.5
p: 2e-05
depl_thresh: 0.1
temporary: False
*/

clock gc; //global clock

typedef int[-6000,900000000] Int;

//int n; //Orbit count
//const int N = 24;

const int orbit_period = 5570; // 5572
//const int eclipse_period = 2160;
const int insolation_period = 3410; //orbit_period-eclipse_period;

/*
Int orbitEndTime(){
    return (n+1)*orbit_period;
}
*/

typedef int[-100000,100000] load_t;

typedef int[0,15] module_id;

/*
    0    NanoMind A3200 OBC
    1    NanoPower P31US
    2    NanoPower BP4 Heaters
    3    NanoCom AX100 Rx
    4    NanoCom Ax100 Tx
    5    NanoMind A3200 ADCS
    6    ADCS Sensors
    7    Magnetorquers
    8    Reaction Wheels + 4WDE
    9    GPS
    10    ADS-B Payload
    11    SOFT RF Receive
    12    SOFT Data Transmit
    13    X-band Transmitter
    14    UHF Antenna Release
    15    Helix Antenna Release
*/


//0, 	1, 	2, 	3, 	4, 	5, 	6, 	7, 	8,		9, 	10, 	11, 	12, 	13, 	14, 		15
const load_t power_m[module_id]={
276,	127,	3360,	276,	2630,	276,	190,	584,	992,	1436,	552,	3863,	2825,	9120,	14080,	14080
};//mJ/s
//0, 	1, 	2, 	3, 	4, 	5, 	6, 	7, 	8,		9, 	10, 	11, 	12, 	13, 	14, 		15
const load_t bg_dc[module_id]={
5570,	5570,	0,		5570,	0,		5570,	5570,	1393,	5570,	600,	5570,	0,		0,		0,		0,			0
};//background duty cicle

const int default_bg_load_po=power_m[0]*bg_dc[0] + power_m[1]*bg_dc[1] + power_m[2]*bg_dc[2] + power_m[3]*bg_dc[3] + power_m[4]*bg_dc[4] + power_m[5]*bg_dc[5] + power_m[6]*bg_dc[6] + power_m[7]*bg_dc[7] + power_m[8]*bg_dc[8] + power_m[9]*bg_dc[9] + power_m[10]*bg_dc[10] + power_m[11]*bg_dc[11] + power_m[12]*bg_dc[12] + power_m[13]*bg_dc[13] + power_m[14]*bg_dc[14] + power_m[15]*bg_dc[15];


const load_t default_bg_load=default_bg_load_po/orbit_period; //mJ/s

typedef int[0, 149760000] soc_t;

const soc_t capacity = 149760000; // mJ
soc_t soc = 134784000;
const soc_t safe_threshold = 82368000;
const load_t max_load = 15000;
const bool force_if_almost_full = true;

const int N_attitudes = 5;
typedef int[0,N_attitudes-1] attitude_t;
const attitude_t a_N = 1, a_Z = 2, a_Y = 3, a_I = 4;
/*
    0    dummy attitude
    1    nominal
    2    +Z to nadir
    3    +Y to nadir
    4    aligned with inmarsat
*/

attitude_t a = 1;  //nominal attitude

const load_t power_g[attitude_t]={ 0, 5700, 6100, 5700, 6100 };

bool ac_lock = 0;
chan alignTo[attitude_t]; //attitude control
chan reached; //reached attitude

const int pp[attitude_t][attitude_t]= // preheating period [from][to]
{
{   -1,   -1,   -1,   -1,   -1},
{   -1,   -1,    0,    0, 1200},
{   -1,    0,   -1,   -1,   -1},
{   -1,    0,   -1,   -1,   -1},
{   -1,    0,   -1,   -1,   -1}
};

const int slp[attitude_t][attitude_t]= // slewing period [from][to]
{
{  -1,  -1,  -1,  -1,  -1},
{  -1,  -1, 600, 600, 600},
{  -1, 600,  -1,  -1,  -1},
{  -1, 600,  -1,  -1,  -1},
{  -1, 600,  -1,  -1,  -1}
};

int sp(const attitude_t a_from, const attitude_t a_to){
    return pp[a_from][a_to]+slp[a_from][a_to];
}

const load_t power_slewing = 1406 - power_m[8]; //mJ/s

chan bUpdate;

const int N_payloads=7;
typedef int[0,N_payloads-1] p_id;
/*
    0    gom access
    1    kourou acces
    2    toulouse acces
    3    inmarsat access 3F2
    4    inmarsat access 3F2
    5    inmarsat access 3F3
    6    inmarsat access 3F3
*/

const int a_nfe[p_id]={0, a_Y, a_Y, a_I, a_I, a_I, a_I}; //attitudes needed for the experiments
                                        // 0 means independent

const load_t power_p[p_id] = {power_m[4], power_m[12]+power_m[13], power_m[12]+power_m[13], power_m[11], power_m[11], power_m[11], power_m[11]};//mJ/s

int c[p_id] = {0,0,0,0,0,0,0}; // counter
int nee[p_id] = {0,0,0,0,0,0,0};  //number of experiment executions
const bool ratio_heur = true;
int ratio = 0;
const int ratio_X = 2;
const int ratio_L = 1;

broadcast chan exp_done[p_id];
chan preHeat[p_id], available[p_id], not_available[p_id];
//broadcast chan preHeat[p_id], available[p_id], not_available[p_id];
//no broadcast needed when correctly implemented

bool slewing = 0;
bool insolation = 0;
bool pa[p_id] = {0,0,0,0,0,0,0};

Int new_time = 0;

const Int access_preheat[p_id][8] = {
{53717,59229,64887,70587,76307,82117,86401,86401},
{16116,21951,57540,63173,86401,86401,86401,86401},
{52792,58402,64154,69937,75684,81415,86401,86401},
{7951,19430,31374,43289,54772,66349,86401,86401},
{2204,13674,25307,37421,49049,60513,72376,86401},
{5261,16756,28749,40614,52090,63711,75817,86401},
{10986,22664,34768,46366,57845,69765,86401,86401}
};

const Int access_start[p_id][8] = {
{53717,59229,64887,70587,76307,82117,86401,86401},
{16716,22551,58140,63773,86401,86401,86401,86401},
{53392,59002,64754,70537,76284,82015,86401,86401},
{9751,21230,33174,45089,56572,68149,86401,86401},
{4004,15474,27107,39221,50849,62313,74176,86401},
{7061,18556,30549,42414,53890,65511,77617,86401},
{12786,24464,36568,48166,59645,71565,86401,86401}
};

const Int access_end[p_id][8] = {
{53941,59747,65467,71166,76823,82330,86401,86401},
{17304,22899,58551,64349,86401,86401,86401,86401},
{53825,59603,65332,71091,76875,82600,86401,86401},
{15321,26800,38744,50659,62142,73719,86401,86401},
{9574,21044,32677,44791,56419,67883,79746,86401},
{12631,24126,36119,47984,59460,71081,83187,86401},
{18356,30034,42138,53736,65215,77135,86401,86401}
};

const Int eclipse_end[17] = { 0,4959,10463,15968,21472,26976,32480,37984,43488,48992,54496,60000,65505,71009,76513,82017,86401 };
const Int insolation_end[17] = { 2927,8433,13940,19446,24953,30460,35967,41473,46980,52487,57994,63501,69008,74515,80022,85529,86401 };

Int expPreheatTime(const p_id pid){
    return access_preheat[pid][c[pid]];
}

Int expStartTime(const p_id pid){
    return access_start[pid][c[pid]];
}

Int expEndTime(const p_id pid){
    return access_end[pid][c[pid]];
}

int sun_c = 0;
Int eclipseEndTime(){
    return eclipse_end[sun_c];
}

Int insolationEndTime(){
    return insolation_end[sun_c];
}

int experimentDuration(const p_id pid){
    return expEndTime(pid) - expStartTime(pid);
}

int abs(const int v){
    if(v < 0){
        return -1*v;
    }
    return v;
}

int costRate(const p_id pid){
    if(pid == 0) return -1;
    if(pid == 1) return 23;
    if(pid == 2) return 21;
    if(pid == 3) return 1;
    if(pid == 4) return 1;
    if(pid == 5) return 1;
    if(pid == 6) return 1;
}

bool exp_possible(const p_id pid){
   if(costRate(pid) == 0) return false;
   if(pid == 0) return true;
   if(ac_lock) return false;
	if(ratio_heur) {
		if(pid == 1 || pid == 2){
		    if(ratio < ratio_L) return false;
		}
		if(pid > 2){
		    if(ratio >= (ratio_X + ratio_L) * ratio_X) return false;
		    if(pid == 3 || pid == 4){
		        if((nee[3]+nee[4])>(nee[5]+nee[6]+1)) return false;
		    }
		    if(pid == 5 || pid == 6){
		        if((nee[5]+nee[6])>(nee[3]+nee[4]+1)) return false;
		    }
		}
	}
   return true;
}

bool skipable(const p_id pid){
   if(costRate(pid) == -1) return false;
	if(force_if_almost_full){
		if(exp_possible(pid)){
		   if(soc > 135000000) return false;
		   else return true;
		}
	}
	return true;
}

bool isAligned(const p_id pid){
    if (a_nfe[pid] == 0) return true; //independent
    return a == a_nfe[pid];
}

bool hasToSlewBack(const p_id pid){
    if (a_nfe[pid] == 0 || a == a_N) return false;
    else return true;
}

void lockIfNeeded(const p_id pid){
    if(a_nfe[pid] != 0) ac_lock = true;
    if (pid == 1 || pid == 2) {
        ratio -= ratio_L;
        //lastXband = true;
    }
    if (pid > 2) {
        ratio += ratio_X;
        //lastXband = false;
    }
}

void unlockIfNeeded(const p_id pid){
    if(a_nfe[pid] != 0) ac_lock = false;
}

void startExperiment(const p_id pid){
    pa[pid] = true;
    if(pid == 1 || pid == 2){
        //if(!insolation) soc = 0;
    }
}

void endExperiment(const p_id pid){
    pa[pid] = false;
    nee[pid]++;
}

void update(const p_id pid){
    new_time = expEndTime(pid);
    c[pid]++;
    if(new_time > expPreheatTime(pid)){
        a=1/0;
    }
}

process AttitudeControl() {
Int slewingEnd_time = 0;
attitude_t a_dst = 0;

void init_slewing(const attitude_t a_tmp){
    a_dst = a_tmp;
    slewing = true;
    slewingEnd_time = new_time + sp(a,a_tmp);
}

void end_slewing(){
    a = a_dst;
    a_dst = 0; //reduces state space
    new_time = slewingEnd_time;
    slewingEnd_time = 0; //reduces state space
    slewing = false;
}
state
    Log2,
    Log1,
    Slewing {gc <=
slewingEnd_time},
    Idle;
commit
    Log2,
    Log1;
init Idle;
trans
    Log2 -> Idle { sync bUpdate!;  },
    Log1 -> Slewing { sync bUpdate!;  },
    Idle -> Log1 { select a_tmp : attitude_t; sync alignTo[a_tmp]?; assign init_slewing(a_tmp);  },
    Slewing -> Log2 { guard gc == slewingEnd_time; sync reached!; assign end_slewing();  };
}

process Experiment0() {
const p_id pid=0;
state
    End,
    Penalty {cost'==
costRate(pid)},
    Skip,
    SlewingBack,
    Check_Attitude,
    Available,
    Idle,
    Align,
    Start,
    Slewing,
    Correct_Attitude;
commit
    End,
    Check_Attitude,
    Available,
    Align;
init Idle;
trans
    End -> Check_Attitude { sync bUpdate!;  },
    Check_Attitude -> Idle { guard !hasToSlewBack(pid);  },
    Penalty -> Idle { sync not_available[pid]?;  },
    Idle -> Skip { guard skipable(pid); sync preHeat[pid]?;  },
    Skip -> Penalty { sync available[pid]?;  },
    Start -> End { sync not_available[pid]?; assign endExperiment(pid);  },
    SlewingBack -> Idle { sync reached?; assign unlockIfNeeded(pid);  },
    Check_Attitude -> SlewingBack { guard hasToSlewBack(pid); sync alignTo[a_N]!;  },
    Correct_Attitude -> Available { sync available[pid]?; assign startExperiment(pid);  },
    Idle -> Align { guard exp_possible(pid); sync preHeat[pid]?; assign lockIfNeeded(pid);  },
    Align -> Correct_Attitude { guard isAligned(pid);  },
    Available -> Start { sync bUpdate!;  },
    Slewing -> Correct_Attitude { sync reached?;  },
    Align -> Slewing { guard !isAligned(pid); sync alignTo[a_nfe[pid]]!;  };
}

process Experiment1() {
const p_id pid=1;
state
    End,
    Penalty {cost'==
costRate(pid)},
    Skip,
    SlewingBack,
    Check_Attitude,
    Available,
    Idle,
    Align,
    Start,
    Slewing,
    Correct_Attitude;
commit
    End,
    Check_Attitude,
    Available,
    Align;
init Idle;
trans
    End -> Check_Attitude { sync bUpdate!;  },
    Check_Attitude -> Idle { guard !hasToSlewBack(pid);  },
    Penalty -> Idle { sync not_available[pid]?;  },
    Idle -> Skip { guard skipable(pid); sync preHeat[pid]?;  },
    Skip -> Penalty { sync available[pid]?;  },
    Start -> End { sync not_available[pid]?; assign endExperiment(pid);  },
    SlewingBack -> Idle { sync reached?; assign unlockIfNeeded(pid);  },
    Check_Attitude -> SlewingBack { guard hasToSlewBack(pid); sync alignTo[a_N]!;  },
    Correct_Attitude -> Available { sync available[pid]?; assign startExperiment(pid);  },
    Idle -> Align { guard exp_possible(pid); sync preHeat[pid]?; assign lockIfNeeded(pid);  },
    Align -> Correct_Attitude { guard isAligned(pid);  },
    Available -> Start { sync bUpdate!;  },
    Slewing -> Correct_Attitude { sync reached?;  },
    Align -> Slewing { guard !isAligned(pid); sync alignTo[a_nfe[pid]]!;  };
}

process Experiment2() {
const p_id pid=2;
state
    End,
    Penalty {cost'==
costRate(pid)},
    Skip,
    SlewingBack,
    Check_Attitude,
    Available,
    Idle,
    Align,
    Start,
    Slewing,
    Correct_Attitude;
commit
    End,
    Check_Attitude,
    Available,
    Align;
init Idle;
trans
    End -> Check_Attitude { sync bUpdate!;  },
    Check_Attitude -> Idle { guard !hasToSlewBack(pid);  },
    Penalty -> Idle { sync not_available[pid]?;  },
    Idle -> Skip { guard skipable(pid); sync preHeat[pid]?;  },
    Skip -> Penalty { sync available[pid]?;  },
    Start -> End { sync not_available[pid]?; assign endExperiment(pid);  },
    SlewingBack -> Idle { sync reached?; assign unlockIfNeeded(pid);  },
    Check_Attitude -> SlewingBack { guard hasToSlewBack(pid); sync alignTo[a_N]!;  },
    Correct_Attitude -> Available { sync available[pid]?; assign startExperiment(pid);  },
    Idle -> Align { guard exp_possible(pid); sync preHeat[pid]?; assign lockIfNeeded(pid);  },
    Align -> Correct_Attitude { guard isAligned(pid);  },
    Available -> Start { sync bUpdate!;  },
    Slewing -> Correct_Attitude { sync reached?;  },
    Align -> Slewing { guard !isAligned(pid); sync alignTo[a_nfe[pid]]!;  };
}

process Experiment3() {
const p_id pid=3;
state
    End,
    Penalty {cost'==
costRate(pid)},
    Skip,
    SlewingBack,
    Check_Attitude,
    Available,
    Idle,
    Align,
    Start,
    Slewing,
    Correct_Attitude;
commit
    End,
    Check_Attitude,
    Available,
    Align;
init Idle;
trans
    End -> Check_Attitude { sync bUpdate!;  },
    Check_Attitude -> Idle { guard !hasToSlewBack(pid);  },
    Penalty -> Idle { sync not_available[pid]?;  },
    Idle -> Skip { guard skipable(pid); sync preHeat[pid]?;  },
    Skip -> Penalty { sync available[pid]?;  },
    Start -> End { sync not_available[pid]?; assign endExperiment(pid);  },
    SlewingBack -> Idle { sync reached?; assign unlockIfNeeded(pid);  },
    Check_Attitude -> SlewingBack { guard hasToSlewBack(pid); sync alignTo[a_N]!;  },
    Correct_Attitude -> Available { sync available[pid]?; assign startExperiment(pid);  },
    Idle -> Align { guard exp_possible(pid); sync preHeat[pid]?; assign lockIfNeeded(pid);  },
    Align -> Correct_Attitude { guard isAligned(pid);  },
    Available -> Start { sync bUpdate!;  },
    Slewing -> Correct_Attitude { sync reached?;  },
    Align -> Slewing { guard !isAligned(pid); sync alignTo[a_nfe[pid]]!;  };
}

process Experiment4() {
const p_id pid=4;
state
    End,
    Penalty {cost'==
costRate(pid)},
    Skip,
    SlewingBack,
    Check_Attitude,
    Available,
    Idle,
    Align,
    Start,
    Slewing,
    Correct_Attitude;
commit
    End,
    Check_Attitude,
    Available,
    Align;
init Idle;
trans
    End -> Check_Attitude { sync bUpdate!;  },
    Check_Attitude -> Idle { guard !hasToSlewBack(pid);  },
    Penalty -> Idle { sync not_available[pid]?;  },
    Idle -> Skip { guard skipable(pid); sync preHeat[pid]?;  },
    Skip -> Penalty { sync available[pid]?;  },
    Start -> End { sync not_available[pid]?; assign endExperiment(pid);  },
    SlewingBack -> Idle { sync reached?; assign unlockIfNeeded(pid);  },
    Check_Attitude -> SlewingBack { guard hasToSlewBack(pid); sync alignTo[a_N]!;  },
    Correct_Attitude -> Available { sync available[pid]?; assign startExperiment(pid);  },
    Idle -> Align { guard exp_possible(pid); sync preHeat[pid]?; assign lockIfNeeded(pid);  },
    Align -> Correct_Attitude { guard isAligned(pid);  },
    Available -> Start { sync bUpdate!;  },
    Slewing -> Correct_Attitude { sync reached?;  },
    Align -> Slewing { guard !isAligned(pid); sync alignTo[a_nfe[pid]]!;  };
}

process Experiment5() {
const p_id pid=5;
state
    End,
    Penalty {cost'==
costRate(pid)},
    Skip,
    SlewingBack,
    Check_Attitude,
    Available,
    Idle,
    Align,
    Start,
    Slewing,
    Correct_Attitude;
commit
    End,
    Check_Attitude,
    Available,
    Align;
init Idle;
trans
    End -> Check_Attitude { sync bUpdate!;  },
    Check_Attitude -> Idle { guard !hasToSlewBack(pid);  },
    Penalty -> Idle { sync not_available[pid]?;  },
    Idle -> Skip { guard skipable(pid); sync preHeat[pid]?;  },
    Skip -> Penalty { sync available[pid]?;  },
    Start -> End { sync not_available[pid]?; assign endExperiment(pid);  },
    SlewingBack -> Idle { sync reached?; assign unlockIfNeeded(pid);  },
    Check_Attitude -> SlewingBack { guard hasToSlewBack(pid); sync alignTo[a_N]!;  },
    Correct_Attitude -> Available { sync available[pid]?; assign startExperiment(pid);  },
    Idle -> Align { guard exp_possible(pid); sync preHeat[pid]?; assign lockIfNeeded(pid);  },
    Align -> Correct_Attitude { guard isAligned(pid);  },
    Available -> Start { sync bUpdate!;  },
    Slewing -> Correct_Attitude { sync reached?;  },
    Align -> Slewing { guard !isAligned(pid); sync alignTo[a_nfe[pid]]!;  };
}

process Experiment6() {
const p_id pid=6;
state
    End,
    Penalty {cost'==
costRate(pid)},
    Skip,
    SlewingBack,
    Check_Attitude,
    Available,
    Idle,
    Align,
    Start,
    Slewing,
    Correct_Attitude;
commit
    End,
    Check_Attitude,
    Available,
    Align;
init Idle;
trans
    End -> Check_Attitude { sync bUpdate!;  },
    Check_Attitude -> Idle { guard !hasToSlewBack(pid);  },
    Penalty -> Idle { sync not_available[pid]?;  },
    Idle -> Skip { guard skipable(pid); sync preHeat[pid]?;  },
    Skip -> Penalty { sync available[pid]?;  },
    Start -> End { sync not_available[pid]?; assign endExperiment(pid);  },
    SlewingBack -> Idle { sync reached?; assign unlockIfNeeded(pid);  },
    Check_Attitude -> SlewingBack { guard hasToSlewBack(pid); sync alignTo[a_N]!;  },
    Correct_Attitude -> Available { sync available[pid]?; assign startExperiment(pid);  },
    Idle -> Align { guard exp_possible(pid); sync preHeat[pid]?; assign lockIfNeeded(pid);  },
    Align -> Correct_Attitude { guard isAligned(pid);  },
    Available -> Start { sync bUpdate!;  },
    Slewing -> Correct_Attitude { sync reached?;  },
    Align -> Slewing { guard !isAligned(pid); sync alignTo[a_nfe[pid]]!;  };
}

process Exp_Provider0() {
const p_id pid=0;
state
    Idle {gc <= expPreheatTime(pid)},
    Available {gc <= expEndTime(pid)},
    PreHeat {gc <= expStartTime(pid)};
init Idle;
trans
    Idle -> PreHeat { guard gc == expPreheatTime(pid); sync preHeat[pid]!; assign new_time = expPreheatTime(pid);  },
    PreHeat -> Available { guard gc == expStartTime(pid); sync available[pid]!; assign new_time = expStartTime(pid);  },
    Available -> Idle { guard gc == expEndTime(pid); sync not_available[pid]!; assign update(pid);  };
}

process Exp_Provider1() {
const p_id pid=1;
state
    Idle {gc <= expPreheatTime(pid)},
    Available {gc <= expEndTime(pid)},
    PreHeat {gc <= expStartTime(pid)};
init Idle;
trans
    Idle -> PreHeat { guard gc == expPreheatTime(pid); sync preHeat[pid]!; assign new_time = expPreheatTime(pid);  },
    PreHeat -> Available { guard gc == expStartTime(pid); sync available[pid]!; assign new_time = expStartTime(pid);  },
    Available -> Idle { guard gc == expEndTime(pid); sync not_available[pid]!; assign update(pid);  };
}

process Exp_Provider2() {
const p_id pid=2;
state
    Idle {gc <= expPreheatTime(pid)},
    Available {gc <= expEndTime(pid)},
    PreHeat {gc <= expStartTime(pid)};
init Idle;
trans
    Idle -> PreHeat { guard gc == expPreheatTime(pid); sync preHeat[pid]!; assign new_time = expPreheatTime(pid);  },
    PreHeat -> Available { guard gc == expStartTime(pid); sync available[pid]!; assign new_time = expStartTime(pid);  },
    Available -> Idle { guard gc == expEndTime(pid); sync not_available[pid]!; assign update(pid);  };
}

process Exp_Provider3() {
const p_id pid=3;
state
    Idle {gc <= expPreheatTime(pid)},
    Available {gc <= expEndTime(pid)},
    PreHeat {gc <= expStartTime(pid)};
init Idle;
trans
    Idle -> PreHeat { guard gc == expPreheatTime(pid); sync preHeat[pid]!; assign new_time = expPreheatTime(pid);  },
    PreHeat -> Available { guard gc == expStartTime(pid); sync available[pid]!; assign new_time = expStartTime(pid);  },
    Available -> Idle { guard gc == expEndTime(pid); sync not_available[pid]!; assign update(pid);  };
}

process Exp_Provider4() {
const p_id pid=4;
state
    Idle {gc <= expPreheatTime(pid)},
    Available {gc <= expEndTime(pid)},
    PreHeat {gc <= expStartTime(pid)};
init Idle;
trans
    Idle -> PreHeat { guard gc == expPreheatTime(pid); sync preHeat[pid]!; assign new_time = expPreheatTime(pid);  },
    PreHeat -> Available { guard gc == expStartTime(pid); sync available[pid]!; assign new_time = expStartTime(pid);  },
    Available -> Idle { guard gc == expEndTime(pid); sync not_available[pid]!; assign update(pid);  };
}

process Exp_Provider5() {
const p_id pid=5;
state
    Idle {gc <= expPreheatTime(pid)},
    Available {gc <= expEndTime(pid)},
    PreHeat {gc <= expStartTime(pid)};
init Idle;
trans
    Idle -> PreHeat { guard gc == expPreheatTime(pid); sync preHeat[pid]!; assign new_time = expPreheatTime(pid);  },
    PreHeat -> Available { guard gc == expStartTime(pid); sync available[pid]!; assign new_time = expStartTime(pid);  },
    Available -> Idle { guard gc == expEndTime(pid); sync not_available[pid]!; assign update(pid);  };
}

process Exp_Provider6() {
const p_id pid=6;
state
    Idle {gc <= expPreheatTime(pid)},
    Available {gc <= expEndTime(pid)},
    PreHeat {gc <= expStartTime(pid)};
init Idle;
trans
    Idle -> PreHeat { guard gc == expPreheatTime(pid); sync preHeat[pid]!; assign new_time = expPreheatTime(pid);  },
    PreHeat -> Available { guard gc == expStartTime(pid); sync available[pid]!; assign new_time = expStartTime(pid);  },
    Available -> Idle { guard gc == expEndTime(pid); sync not_available[pid]!; assign update(pid);  };
}

process Battery(const soc_t lb, const soc_t ub) {
load_t load = 0;
Int old_time = 0;
//meta int par;

void update(){
    soc -= load * (new_time - old_time);
    if (soc > ub){    //align to bound
        soc = ub;
    }
    old_time = new_time;
    load = default_bg_load;
    //par = 0;
    for(pid:p_id){
        load += pa[pid] ? power_p[pid] : 0;
        //par += pa[pid] ? 1 : 0;
    }
    //if (par > 1) soc = 0;
    load += slewing ? power_slewing : 0;
    load -= insolation ? power_g[a] : 0;
    if (load > max_load) soc = 0; //deadlock
}


bool depleted(){
    return soc <= lb;
}
state
    Init,
    Check,
    Depletion,
    Idle;
commit
    Init,
    Check,
    Depletion;
init Init;
trans
    Init -> Idle { assign update();  },
    Idle -> Check { sync bUpdate?; assign update();  },
    Check -> Idle { guard soc > lb;  },
    Check -> Depletion { guard soc <= lb;  };
}

process Sun() {
bool updateAfterInsolationEnd = 0 ;

void eclipseEnd(){
    insolation = true;
    new_time = eclipseEndTime();
    if(!updateAfterInsolationEnd) sun_c++;
}

void insolationEnd(){
    insolation = false;
    new_time = insolationEndTime();
    if(updateAfterInsolationEnd) sun_c++;
}
state
    Init,
    Eclipse {gc<=eclipseEndTime()},
    Insolation {gc <= insolationEndTime()};
commit
    Init;
init Init;
trans
    Init -> Insolation { guard eclipseEndTime() > insolationEndTime(); assign insolation=true,
updateAfterInsolationEnd=false;  },
    Init -> Eclipse { guard eclipseEndTime() <= insolationEndTime(); assign updateAfterInsolationEnd=true;  },
    Eclipse -> Insolation { guard gc == eclipseEndTime(); sync bUpdate!; assign eclipseEnd();  },
    Insolation -> Eclipse { guard gc == insolationEndTime(); sync bUpdate!; assign insolationEnd();  };
}

process Termination() {
void last(){
	new_time = 86400 ;
}

state
    Idle {gc <= 86400},
    Inter,
    Lock;
commit Lock, Inter;
init Idle;
trans
    Idle -> Inter { guard gc == 86400; sync bUpdate!; assign last(); },
    Inter -> Lock { assign cost += (0 == 0) ? 0 : ((capacity - soc) / 0); };
}

AC = AttitudeControl();
B = Battery(safe_threshold, capacity);
//OC = OrbitCounter();
E0 = Experiment0();
E1 = Experiment1();
E2 = Experiment2();
E3 = Experiment3();
E4 = Experiment4();
E5 = Experiment5();
E6 = Experiment6();
EP0 = Exp_Provider0();
EP1 = Exp_Provider1();
EP2 = Exp_Provider2();
EP3 = Exp_Provider3();
EP4 = Exp_Provider4();
EP5 = Exp_Provider5();
EP6 = Exp_Provider6();
T = Termination();

// List one or more processes to be composed into a system.
system E0, E1, E2, E3, E4, E5, E6, EP0, EP1, EP2, EP3, EP4, EP5, EP6, Sun, AC, B, T;
