from numpy import zeros, logical_and, greater_equal, less_equal
from tools import unix
from pandas import read_csv, to_datetime
from numbers import Number
import conf
# filename -- the file name of a csv table
# start -- a string representation of a datetime object or an integer timestamp
# end -- a string representation of a datetime object or an integer timestamp
# col -- the string matching a column.

# crop filename w.r.t. the column col_start and col_end

def crop(filename, col_start, col_end, start_offset, end_offset):
	start = unix(conf.param.start) + start_offset
	end = unix(conf.param.end) - end_offset
	data = read_csv(filename, parse_dates=['Start Time (UTCG)','Stop Time (UTCG)'])
	mask = logical_and(
		greater_equal(unix(data[col_start]), start),
		less_equal(unix(data[col_end]), end)
	)
	return (data,mask)

def readTable(fname, ident, idx, warmup = 0, cooldown = 0):
	entry = {}
	# start = conf.param.start
	# end = conf.param.end
	col_start = 'Start Time (UTCG)'
	col_end	 = 'Stop Time (UTCG)'
	if 'Sun' in fname:
		col_start = col_end
		col_end	 = col_start

	entry['id'] = ident
	entry['warm'] = warmup
	entry['cool'] = cooldown
	entry['idx'] = idx
	entry['file'] = fname
	entry['wins'], entry['mask'] = crop(
		filename  = entry['file'],
		start_offset = entry['warm'],
		end_offset = entry['cool'],
		col_start = col_start,
		col_end	 = col_end
	)

	if 'Sun' not in fname:
		entry['wins']['Scheduled'] = zeros(entry['mask'].shape[0])
	return entry
