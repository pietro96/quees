import conf
import tools
import numpy as np
import os

def exportTables(data):
	print('Exporting CSV tables ...\n')
	os.chdir(conf.param.export)
	now = tools.start_timer()

	for val in data.values():
		val['wins'].to_csv( # Write csv files
				path_or_buf = val['file'],
				sep = ',',
				index = False,
				float_format = '%.3f',
				date_format = '%d %b %Y %H:%M:%S.%f'
		)

	conf.times['Writing CSVs'] = tools.stop_timer(now)
	os.chdir('../')

def fillTables(data, trace):
	print('Fill CSV tables ...\n')
	now = tools.start_timer()

	for val in data.values():
		mask = np.zeros(val['wins'].shape[0])
		for k in ['nee[' + str(i) + ']' for i in val['idx']]:
			mask = np.logical_or(
						mask,
						tools.contains(
							data = val['wins'],
							timestamps = trace.drop_duplicates([k])['gc'][1:],
							col1 = 'Start Time (UTCG)',
							col2 = 'Stop Time (UTCG)'
						)
					)
			val['wins'].loc[:,('Scheduled')] = np.logical_or(
				val['wins']['Scheduled'],
				mask
			).astype(int) # Make sure boolean are printed as 0,1

	conf.times['Fill CSVs'] = tools.stop_timer(now)
