from numpy import amin, amax, arange, zeros, ones, logical_or, logical_and, array, greater_equal, less_equal, not_equal, append, sort
#from time import *
from pandas import read_csv, to_datetime, DataFrame
from numbers import Number
import datetime as dt
from timeit import default_timer as timer

# interpolate every 0-entry with the previous value
def interpolate(X,idx):
	Y = zeros((idx[-1]+1,))
	Y[idx] = X
	for i in arange(0,idx[-1]):
		if Y[i] == 0:
			Y[i] = Y[i-1]
	return Y

# transform a datetime string into a unix timestamp
# def unix(X):
	# return int(mktime(to_datetime(X,utc=True).timetuple()))

# def unix(X):
	# return (X - dt.datetime(1970, 1, 1)) / dt.timedelta(seconds=1)

def unix(X):
	if isinstance(X, dt.datetime):
		return int((X - dt.datetime(1970, 1, 1)) / dt.timedelta(seconds=1))
	elif isinstance(X, str):
		return int((to_datetime(X) - dt.datetime(1970, 1, 1)) / dt.timedelta(seconds=1))
	else:
		return ((X - dt.datetime(1970, 1, 1)) / dt.timedelta(seconds=1)).astype(int)



# scale vector X such that its minmum is mapped to a and its maximum to b
def scale_bounds(X,a,b):
	return (b-a)*(X - amin(X)) / (amax(X) - amin(X)) + a

# scale vector X in a conventional way
def scale(X):
	return X/amax(X)

# data -- Dataframe
# timestamps -- a list of unix timestamps
# col1 -- string of left boundaries
# col2 -- string of right boundaries

# iff any t in timestamps is in the interval of col1 and col2

def contains(data, timestamps, col1, col2):
	mask = zeros(data.shape[0])
	for t in timestamps:
		mask = logical_or(
				mask,
				logical_and(
					less_equal(unix(data[col1]), t),
					less_equal(t, unix(data[col2]))
				))
	return mask

# This function filters UPPAAL Cora output
# to remove intermediate Uppaal states.
# Necessary to plot the SoC properly.

# def filter(data, column):
	# df = data.copy()
	# df.loc[:,(column)] = data[column].diff()
	# return (df,df[column] != 0.0)
def filter(data, column, which='first'):
	if which == 'first':
		n = 1
	elif which == 'last':
		n = -1
	else:
		exit('which should be first or last')
	# if which == 'first':
		# import tools
		# import conf
		# exit(not_equal( data[column], data[column].shift(n) ))
	mask = not_equal( data[column], data[column].shift(n) )
	return (data.loc[mask,:], mask)

def arraystr(l, sep):
	return '{' + sep.join(map(str,l)) + '}'

# def matrixstr(dict,key):
	# return ',\n'.join(arraystr(v[key],',') for v in dict.values())

def matrixstr(dict,key,order):
	return ',\n'.join(arraystr(dict[k][key],',') for k in order if k in dict)


def plotWindows(ax, df, i):
	sun = 'Scheduled' not in df
	ax.hlines(
		(0 if sun else i) + zeros(df.shape[0]),
		unix(df['Start Time (UTCG)']),
		unix(df['Stop Time (UTCG)']),
		color = 'orange' if sun else array(['silver','black'])[df['Scheduled']],
		alpha = 0.7,
		lw = 25
	)
	return ax

def savefig(fig, fname):
	fig.patch.set_facecolor('black')
	fig.patch.set_alpha(0)
	fig.savefig(fname,bbox_inches='tight',
		facecolor=fig.get_facecolor()
		)

def start_timer():
	return timer()

def stop_timer(now):
	return round(timer() - now,3)

if __name__ == "__main__":
	exit()
