#!/usr/bin/env python3

from sys import exit
from numpy import logical_or, logical_and, arange, amax, zeros, array, pad, subtract, logical_not
import datetime as dt
import pandas as pd
import tools

# ======== PLOTTING ================
import matplotlib as mpl
pgf_with_custom_preamble = {
		"font.size": 30,
		"text.usetex": True,	# use inline math for ticks
		"pgf.rcfonts": False,   # don't setup fonts from rc parameters
		"pgf.preamble": [
			 r"\usepackage[utf8x]{inputenc}",
			 r"\usepackage[T1]{fontenc}",
			]
}
mpl.rcParams.update(pgf_with_custom_preamble)
mpl.use('Agg')
import matplotlib.pyplot as plt
from matplotlib import gridspec
plt.style.use('bmh')
mpl.rcParams['lines.linewidth'] = 3

import conf

conf.param = conf.p.parse_args()
conf.sanity_check()

horizon	= tools.unix(conf.param.end) - tools.unix(conf.param.start)

print('Scheduling from {start} to {end}, which are {orbits} orbits'.format(
	start =	conf.param.start,
	end =		conf.param.end,
	orbits =	horizon/5570
))

# ================================================
print('Parsing Input ... \n')
# ================================================

from glob import iglob
from os import chdir
import preprocessing as pre

now = tools.start_timer()
LBand_split = 2

chdir(conf.param.tables) # cd into param.tables
data = {}
for fname in iglob('*.csv'):
	entry = None
	if 'Sun' in fname:
		entry = pre.readTable(fname,'Sun',[])
	if 'Facility-GOM' in fname:
		entry = pre.readTable(fname,'UHF',[0])
	if 'Inmarsat3F2' in fname:
		entry = pre.readTable(fname,'IS_1',[3,4],1800,600)
	if 'Inmarsat3F3' in fname:
		entry = pre.readTable(fname,'IS_2',[5,6],1800,600)
	if 'Kourou' in fname:
		entry = pre.readTable(fname,'X_K',[1],600,600)
	if 'Toulouse' in fname:
		entry = pre.readTable(fname,'X_T',[2],600,600)
	data[entry['id']] = entry

chdir('../')

conf.times['Preprocessing'] = tools.stop_timer(now)

# ================================================
print('Generating UPPAAL CORA arrays ... \n')
# ================================================

def relativate(X,start):
	return (tools.unix(X).add(-tools.unix(start))).clip_lower(0)

# returns only those windows that are contained in the scheduling horizon
select = lambda val: val['wins'][val['mask']]

now = tools.start_timer()

# arrays = {'uhf' : { [start array], [end array], length, string} } ...
jobs = {}
sun = {}

for key,val in data.items():
	entry = {}
	if 'IS' in key: # Special treatment for Lband since they need splitting
		for i in arange(LBand_split):
			tmp_data = (select(val))[select(val).index % LBand_split == i]
			entry['acc'] = relativate(tmp_data['Start Time (UTCG)'],conf.param.start)
			entry['pre'] = subtract(entry['acc'], val['warm'])
			entry['end'] = relativate(tmp_data['Stop Time (UTCG)'],conf.param.start)
			entry['len'] = entry['acc'].size
			jobs[key + '_' + str(i+1)] = entry
			entry = {}
	elif 'Sun' in key: # Sun has a few None entries because they don't matter
		entry['acc'] = relativate(select(val)['Start Time (UTCG)'],conf.param.start)
		entry['end'] = relativate(select(val)['Stop Time (UTCG)'],conf.param.start)
		entry['len'] = select(val).shape[0]
		sun = entry
	else: # Every other job type is similar
		entry['acc'] = relativate((select(val))['Start Time (UTCG)'],conf.param.start)
		entry['pre'] = subtract(entry['acc'], val['warm'])
		entry['end'] = relativate(select(val)['Stop Time (UTCG)'],conf.param.start)
		entry['len'] = select(val).shape[0]
		jobs[key] = entry

# Add padding
max_len = amax([v['len'] for v in jobs.values()])

for val in jobs.values():
	for key in ['acc','end','pre']:
		val[key] = pad(val[key],mode='constant',pad_width=(0,((max_len+1) - val['len'])), constant_values=horizon+1)
	val['len'] = (max_len+1)

for key in ['acc','end']:
	sun[key] = pad(sun[key],mode='constant',pad_width=(0,1), constant_values=horizon+1)
sun['len'] += 1

# Build Strings

job_strs = {}
for key in {'pre', 'acc', 'end'}:
	job_strs[key] = tools.matrixstr(
		dict = jobs,
		key = key,
		order = ['UHF', 'X_K', 'X_T'] +
			['IS_1_' + str(i) for i in range(1,LBand_split+1)] +
			['IS_2_' + str(i) for i in range(1,LBand_split+1)]
	)

sun_strs = {}
for key in {'acc', 'end'}:
	sun_strs[key] = ','.join(str(e) for e in sun[key])

conf.times['Generating Arrays'] = tools.stop_timer(now)

# ================================================
print('Instantiating Model & Query Templates ...')
# ================================================

from string import Template
from tempfile import NamedTemporaryFile
import uppaal
from io import StringIO
import kibam

# Need this to allow '.' in placeholders, i.e. 'B.r'
class MyTemplate(Template):
    delimiter = '$'
    pattern = r'''
    \$(?:
      (?P<escaped>\$) |
      (?P<named>[_a-z][._a-z0-9]*) |
      {(?P<braced>[_a-z][._a-z0-9]*)} |
      (?P<invalid>)
     )
    '''

subst = {
'watermark' : conf.dump(conf.param.__dict__),
'horizon' : horizon,
'nr_job_win' : jobs['UHF']['len'],
'access_preheat' : job_strs['pre'],
'access_start' : job_strs['acc'],
'access_end' : job_strs['end'],
'nr_sun_win' : sun['len'],
'sun_start' : sun_strs['acc'],
'sun_end' : sun_strs['end'],
'max_load' : conf.param.max_load,
'ratio_heur' : 'false' if conf.param.no_ratios else 'true',
'force_take' : 'false' if conf.param.no_force_take else 'true',
'r_X' : conf.param.ratio_X,
'r_L' : conf.param.ratio_L,
'capacity' : conf.param.capacity,
'safe_threshold' : int(conf.param.capacity * conf.param.cora_safe_mode),
'soc_penalty' : conf.param.soc_penalty_factor,
'soc': int(conf.param.capacity * conf.param.init_soc),
'a': 1,
'ac_lock': 0,
'c': tools.arraystr([0]*7, ','),
'nee': tools.arraystr([0]*7, ','),
'ratio': 0,
'slewing': 0,
'insolation': 0,
'pa': tools.arraystr([0]*7, ','),
'new_time': 0,
'sun_c': 0,
'Sun.updateAfterInsolationEnd': 0,
'AC.slewingEnd_time': 0,
'AC.a_dst': 0,
'B.load': 0,
#'B.old_time': 0,
'E0': 'Idle',
'E1': 'Idle',
'E2': 'Idle',
'E3': 'Idle',
'E4': 'Idle',
'E5': 'Idle',
'E6': 'Idle',
'EP0': 'Idle',
'EP1': 'Idle',
'EP2': 'Idle',
'EP3': 'Idle',
'EP4': 'Idle',
'EP5': 'Idle',
'EP6': 'Idle',
'Sun': 'Init',
'AC': 'Idle',
'B': 'Init'
}

with open(conf.param.model_tmpl, 'r') as file :
	model_code = MyTemplate(file.read()).substitute(subst)

with open(conf.param.query_tmpl, 'r') as file :
	query_code = MyTemplate(file.read()).substitute(
		horizon = str(horizon),
		leftover = str(int(conf.param.capacity * conf.param.rem_soc))
	)

if conf.param.temporary:
	print(' -- Using temporary files', end=':\n')
	model_file = NamedTemporaryFile(delete=False, mode='w')
	conf.param.model_file = model_file.name
	query_file = NamedTemporaryFile(delete=False, mode='w')
	conf.param.query_file = query_file.name
else:
	print(' -- Writing on disk', end=':\n')
	model_file = open(conf.param.model_file, 'w')
	query_file = open(conf.param.query_file, 'w')

print(' --- ' + model_file.name, end=', ')
print(query_file.name)

model_file.write(model_code)
model_file.close()
query_file.write(query_code)
query_file.close()

# ================================================
print('Calling UPPAAL CORA ... \n')
# ================================================

out,err = uppaal.cora()

# First line is a blank
trace = uppaal.parse(StringIO(err.decode()[1:]))
#times['Postprocessing'] = tools.rectime(now)

trace.loc[:,('gc')] += tools.unix(conf.param.start)

# ================================================
print('Run Validation ...\n')
# ================================================

(dead,between,full,load_filtered) = kibam.validate(trace[['gc','B.load']])

(depletion,fig) = kibam.density(dead,between,full)

tools.savefig(fig,conf.param.export+'kibam.pdf')

if depletion >= conf.param.depl_thresh:
	exit("The shedule didn't meet the requirements. Depletion risk is too high. Try increasing the safe mode threshold or just run the tool again. Uppaal will use a different seed.")

## Fill CSVs

import postprocessing as post

post.fillTables(data, trace)

# ================================================
print('Plotting schedule ...\n')
# ================================================

import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec
from pandas import date_range
import uppaal

f = plt.figure(figsize=[25,12])
gs = gridspec.GridSpec(2, 1, height_ratios=[4,2],hspace=0.1)

xticks 	= date_range(conf.param.start, conf.param.end, freq = '1h')
xticks	= xticks[1:] # for layout reasons remove first tick

SoC_filtered,_ = tools.filter(trace[['gc','soc']], 'soc', which='first' )

ax = plt.subplot(gs[0])
soc = uppaal.plotSoC( # plotting SoC curve of Uppaal
	ax = ax,
	data = SoC_filtered,
	xticks = xticks
)

ax = plt.subplot(gs[1])
load = uppaal.plotLoad( # plotting Load curve of Uppaal
	ax = ax,
	data = load_filtered,
	xticks = xticks
)

ax1 = ax.twinx()
i = 1
yticklabels = []

ax1.grid(False)
for df in data.values(): # plotting timewindows
	tools.plotWindows(ax1, select(df), i)
	yticklabels.insert(
		0 if df['id'] == 'Sun' else i,
		'$\\mathit{' + df['id'] + '}$'
	)
	i += 0 if df['id'] == 'Sun' else 1

ax1.set(
	xlim = [tools.unix(conf.param.start), tools.unix(conf.param.end)],
	ylim = [-0.5,len(data)-0.5],
	yticks = arange(0,len(data)),
	yticklabels = yticklabels
)

s, = plt.plot([],[],'silver',lw=25)
s2, = plt.plot([],[],'black',lw=25)
#sun, = plt.plot([],[],'orange',lw=20)

f.legend(
	(soc, load, s2, s, sun),
	('SoC','load','scheduled','skipped'),
	loc = "upper center",
	labelspacing = 0.04,
	ncol = 4
)

f.autofmt_xdate()
tools.savefig(f,conf.param.export + 'schedule.pdf')

## Export CSV tables
post.exportTables(data)

print('== SCHEDULE DIAGNOSTICS === ')
print('=== Operational Windows === ')
for key,val in data.items():
	print('- {0}:\t {1}'.format(key,select(val).shape[0]))
print('=== Scheduling Results ==== ')
for key,val in uppaal.aggregate(trace, data.values()).items():
	if 'SoC' in key:
		print('- {0}:\t {1:.1f}%'.format(key,val/conf.param.capacity * 100))
	else:
		print('- {0}:\t {1}'.format(key,val))
print('')

# ================================================
print('Runtimes in seconds:\n' + conf.dump(conf.times))
# ================================================
