int battery = 149760000; //mJ
const int forty_batt = 59904000; //149760000*0.4

//common job constants
const int t_ph = 20;
const int t_rot = 10;

const int cons_ph = 496800; //pow_ph*t_ph
const int cons_rot = 248400; //pow_rot*t_rot


// ---------------*******--------------- Variables  ---------------*******---------------


int done_uhf = 0;
int done_lb_f2 = 0;
int done_lb_f3 = 0;
int done_xband = 0;

//model actions and clocks:
clock d;
clock t;
clock cs;
action sun_a, sun_b, sun_c, sun_d, sun_e, sun_f, sun_g, sun_h, update, end, event, skip, preheat, start_rotate, rot_start_job, norot_start_job, df_rotate, nodf_rotate, rotated_back;

bool time_elapsed = false; //goal (end of time)

int saved = 0;


// ---------------*******--------------- PROPERTIES  ---------------*******---------------

//p r o p e r t y E < > time_elapsed;
property Xmin(S(saved),time_elapsed);

// ---------------*******--------------- process uhf  ---------------*******---------------

process choose_uhf (int n){
	//choose to skip or not
	alt{
	:: skip {= saved = cons_uhf[n]=}
	:: when(battery >= forty_batt + cons_uhf[n]) preheat {= t = 0, battery = battery - cons_ph =};
		//setup_no-rotate
		invariant(t <= t_ph) when(t == t_ph) norot_start_job {= battery = battery - cons_uhf[n], t = 0 =};
		
			//job, directly exit to default without rotate
			invariant(t <= dur_uhf[n]) when(t == dur_uhf[n]) nodf_rotate {= done_uhf ++, t = 0 =}
	}
}

// ---------------*******--------------- process lb_f2 ---------------*******---------------

process choose_lb_f2 (int n){
	//choose to skip or not
	alt{
	:: skip {= saved = cons_lb_f2[n] =}
	:: when(battery >= forty_batt + cons_lb_f2[n]) preheat {= t = 0, battery = battery - cons_ph =};

		//setup_rotate
		invariant(t <= t_ph) when(t == t_ph) start_rotate {= t = 0, battery = battery - cons_rot =}; 
		   invariant(t <= t_rot) when(t == t_rot) rot_start_job {= battery = battery - cons_lb_f2[n], t = 0 =}; 

		//job, rotate back to default
			invariant(t <= dur_lb_f2[n]) when(t == dur_lb_f2[n]) df_rotate {= done_lb_f2 ++, battery = battery - cons_rot, t = 0 =};

				//rotate back
				invariant(t <= t_rot) when(t == t_rot) rotated_back {= t = 0 =}
	}
}


// ---------------*******--------------- process lb_f3 ---------------*******---------------

process choose_lb_f3 (int n){
	//choose to skip or not
	alt{
	:: skip {= saved = cons_lb_f3[n] =}
	:: when(battery >= forty_batt + cons_lb_f3[n]) preheat {= t = 0, battery = battery - cons_ph =};

		//setup_rotate
		invariant(t <= t_ph) when(t == t_ph) start_rotate {= t = 0, battery = battery - cons_rot =}; 
		   invariant(t <= t_rot) when(t == t_rot) rot_start_job {= battery = battery - cons_lb_f3[n], t = 0 =}; 

		//job, rotate back to default
			invariant(t <= dur_lb_f3[n]) when(t == dur_lb_f3[n]) df_rotate {= done_lb_f3 ++, battery = battery - cons_rot, t = 0 =};

				//rotate back
				invariant(t <= t_rot) when(t == t_rot) rotated_back {= t = 0 =}
	}
}

// ---------------*******--------------- process xband  ---------------*******---------------

process choose_xband (int n){
	//choose to skip or not
	alt{
	:: skip {= saved = cons_xband[n]=}
	:: when(battery >= forty_batt + cons_xband[n]) preheat {= t = 0, battery = battery - cons_ph =};

		//setup_rotate
		
		invariant(t <= t_ph) when(t == t_ph) start_rotate {= t = 0, battery = battery - cons_rot =}; 
		   invariant(t <= t_rot) when(t == t_rot) rot_start_job {= battery = battery - cons_xband[n], t = 0 =}; 

		//job, rotate back to default
			invariant(t <= dur_xband[n]) when(t == dur_xband[n]) df_rotate {= done_xband ++, battery = battery - cons_rot, t = 0 =};

				//rotate back
				invariant(t <= t_rot) when(t == t_rot) rotated_back {= t = 0 =}
	}
}

// ---------------*******--------------- ARRAYS  ---------------*******---------------
//durations
transient int(0..10) [] dur_xband = [6, 10];
transient int(0..10) [] dur_uhf = [4, 8, 10];
transient int(0..100) [] dur_lb_f2 = [93];
transient int(0..100) [] dur_lb_f3 = [93];

//consumption for jobs
transient int(0..1600000) [] cons_uhf = [631200, 1262400, 1578000];
transient int(0..22000000) [] cons_lb_f2 = [21555540];
transient int(0..22000000) [] cons_lb_f3 = [21555540];
transient int(0..8000000) [] cons_xband = [4300200, 7167000];


//transient int(0..150) [] events = [46, 45, 144, 74, 18, 75, 19];

//sun toggling times
//transient int(0..4000) [] sun = [64, 59, 33, 58, 34, 48, 44, 58];

//sun updates
//transient int [] sun_charge = [-11624221, 9466812, -6013868, 9474945, -6004901, 9480367, -5998923, 9488500];
// no sun - @64 - sun - @59 - no sun - @33 - sun ...etc


// ---------------*******--------------- Do Loop --- Idle  ---------------*******---------------


par{
:: invariant(cs <= 64) when(cs == 64) sun_a {= cs = 0, battery = battery + -11624221 =};
   invariant(cs <= 59) when(cs == 59) sun_b {= cs = 0, battery = battery + 9466812 =};
   invariant(cs <= 33) when(cs == 33) sun_c {= cs = 0, battery = battery + -6013868 =};
   invariant(cs <= 58) when(cs == 58) sun_d {= cs = 0, battery = battery + 9474945 =};
   invariant(cs <= 34) when(cs == 34) sun_e {= cs = 0, battery = battery + -6004901 =};
   invariant(cs <= 48) when(cs == 48) sun_f {= cs = 0, battery = battery + 9480367 =};
   invariant(cs <= 44) when(cs == 44) sun_g {= cs = 0, battery = battery + -5998923 =};
   invariant(cs <= 58) when(cs == 58) sun_h {= cs = 0, battery = battery + 9488500 =}
   
:: 	invariant(d <= 46) when(d == 46) event {= d = 0 =}; choose_lb_f3(0);
	invariant(d <= 45) when(d == 45) event {= d = 0 =}; choose_lb_f2(0);
	invariant(d <= 144) when(d == 144) event {= d = 0 =}; choose_uhf(0);
	invariant(d <= 74) when(d == 74) event {= d = 0 =}; choose_xband(0);
	invariant(d <= 18) when(d == 18) event {= d = 0 =}; choose_uhf(1);
	invariant(d <= 75) when(d == 75) event {= d = 0 =}; choose_xband(1);
	invariant(d <= 19) when(d == 19) event {= d = 0 =}; choose_uhf(2);
	invariant(d <= 59) when(d == 59) end {= time_elapsed = true, d = 0 =}
	
}


