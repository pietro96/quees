# riccardo

from __future__ import annotations
from typing import List, Union, Optional

class VariableInfo(object):
	__slots__ = ("name", "component", "type", "minValue", "maxValue")
	
	def __init__(self, name: str, component: Optional[int], type: str, minValue = None, maxValue = None):
		self.name = name
		self.component = component
		self.type = type
		self.minValue = minValue
		self.maxValue = maxValue

# States
class State(object):
	__slots__ = ("battery", "done_uhf", "done_lb_f2", "done_lb_f3", "done_xband", "d", "t", "cs", "time_elapsed", "saved", "Main_location", "Main_location_1", "n_n_n")
	
	def get_variable_value(self, variable: int):
		if variable == 0:
			return self.battery
		elif variable == 1:
			return self.done_uhf
		elif variable == 2:
			return self.done_lb_f2
		elif variable == 3:
			return self.done_lb_f3
		elif variable == 4:
			return self.done_xband
		elif variable == 5:
			return self.d
		elif variable == 6:
			return self.t
		elif variable == 7:
			return self.cs
		elif variable == 8:
			return self.time_elapsed
		elif variable == 9:
			return self.saved
		elif variable == 10:
			return self.Main_location
		elif variable == 11:
			return self.Main_location_1
		elif variable == 12:
			return self.n_n_n
	
	def copy_to(self, other: State):
		other.battery = self.battery
		other.done_uhf = self.done_uhf
		other.done_lb_f2 = self.done_lb_f2
		other.done_lb_f3 = self.done_lb_f3
		other.done_xband = self.done_xband
		other.d = self.d
		other.t = self.t
		other.cs = self.cs
		other.time_elapsed = self.time_elapsed
		other.saved = self.saved
		other.Main_location = self.Main_location
		other.Main_location_1 = self.Main_location_1
		other.n_n_n = self.n_n_n
	
	def __eq__(self, other):
		return isinstance(other, self.__class__) and self.battery == other.battery and self.done_uhf == other.done_uhf and self.done_lb_f2 == other.done_lb_f2 and self.done_lb_f3 == other.done_lb_f3 and self.done_xband == other.done_xband and self.d == other.d and self.t == other.t and self.cs == other.cs and self.time_elapsed == other.time_elapsed and self.saved == other.saved and self.Main_location == other.Main_location and self.Main_location_1 == other.Main_location_1 and self.n_n_n == other.n_n_n
	
	def __ne__(self, other):
		return not self.__eq__(other)
	
	def __hash__(self):
		result = 75619
		result = (((101 * result) & 0xFFFFFFFF) + hash(self.battery)) & 0xFFFFFFFF
		result = (((101 * result) & 0xFFFFFFFF) + hash(self.done_uhf)) & 0xFFFFFFFF
		result = (((101 * result) & 0xFFFFFFFF) + hash(self.done_lb_f2)) & 0xFFFFFFFF
		result = (((101 * result) & 0xFFFFFFFF) + hash(self.done_lb_f3)) & 0xFFFFFFFF
		result = (((101 * result) & 0xFFFFFFFF) + hash(self.done_xband)) & 0xFFFFFFFF
		result = (((101 * result) & 0xFFFFFFFF) + hash(self.d)) & 0xFFFFFFFF
		result = (((101 * result) & 0xFFFFFFFF) + hash(self.t)) & 0xFFFFFFFF
		result = (((101 * result) & 0xFFFFFFFF) + hash(self.cs)) & 0xFFFFFFFF
		result = (((101 * result) & 0xFFFFFFFF) + hash(self.time_elapsed)) & 0xFFFFFFFF
		result = (((101 * result) & 0xFFFFFFFF) + hash(self.saved)) & 0xFFFFFFFF
		result = (((101 * result) & 0xFFFFFFFF) + hash(self.Main_location)) & 0xFFFFFFFF
		result = (((101 * result) & 0xFFFFFFFF) + hash(self.Main_location_1)) & 0xFFFFFFFF
		result = (((101 * result) & 0xFFFFFFFF) + hash(self.n_n_n)) & 0xFFFFFFFF
		return result
	
	def __str__(self):
		result = "("
		result += "battery = " + str(self.battery)
		result += ", done_uhf = " + str(self.done_uhf)
		result += ", done_lb_f2 = " + str(self.done_lb_f2)
		result += ", done_lb_f3 = " + str(self.done_lb_f3)
		result += ", done_xband = " + str(self.done_xband)
		result += ", d = " + str(self.d)
		result += ", t = " + str(self.t)
		result += ", cs = " + str(self.cs)
		result += ", time_elapsed = " + str(self.time_elapsed)
		result += ", saved = " + str(self.saved)
		result += ", Main_location = " + str(self.Main_location)
		result += ", Main_location_1 = " + str(self.Main_location_1)
		result += ", n_n_n = " + str(self.n_n_n)
		result += ")"
		return result

# Transients
class Transient(object):
	__slots__ = ("Property_1_edge_reward", "dur_xband", "dur_uhf", "dur_lb_f2", "dur_lb_f3", "cons_uhf", "cons_lb_f2", "cons_lb_f3", "cons_xband")
	
	def copy_to(self, other: Transient):
		other.Property_1_edge_reward = self.Property_1_edge_reward
		other.dur_xband = list(self.dur_xband)
		other.dur_uhf = list(self.dur_uhf)
		other.dur_lb_f2 = list(self.dur_lb_f2)
		other.dur_lb_f3 = list(self.dur_lb_f3)
		other.cons_uhf = list(self.cons_uhf)
		other.cons_lb_f2 = list(self.cons_lb_f2)
		other.cons_lb_f3 = list(self.cons_lb_f3)
		other.cons_xband = list(self.cons_xband)
	
	def __eq__(self, other):
		return isinstance(other, self.__class__) and self.Property_1_edge_reward == other.Property_1_edge_reward and self.dur_xband == other.dur_xband and self.dur_uhf == other.dur_uhf and self.dur_lb_f2 == other.dur_lb_f2 and self.dur_lb_f3 == other.dur_lb_f3 and self.cons_uhf == other.cons_uhf and self.cons_lb_f2 == other.cons_lb_f2 and self.cons_lb_f3 == other.cons_lb_f3 and self.cons_xband == other.cons_xband
	
	def __ne__(self, other):
		return not self.__eq__(other)
	
	def __hash__(self):
		result = 75619
		result = (((101 * result) & 0xFFFFFFFF) + hash(self.Property_1_edge_reward)) & 0xFFFFFFFF
		for x in self.dur_xband:
			result = (((101 * result) & 0xFFFFFFFF) + hash(x)) & 0xFFFFFFFF
		for x in self.dur_uhf:
			result = (((101 * result) & 0xFFFFFFFF) + hash(x)) & 0xFFFFFFFF
		for x in self.dur_lb_f2:
			result = (((101 * result) & 0xFFFFFFFF) + hash(x)) & 0xFFFFFFFF
		for x in self.dur_lb_f3:
			result = (((101 * result) & 0xFFFFFFFF) + hash(x)) & 0xFFFFFFFF
		for x in self.cons_uhf:
			result = (((101 * result) & 0xFFFFFFFF) + hash(x)) & 0xFFFFFFFF
		for x in self.cons_lb_f2:
			result = (((101 * result) & 0xFFFFFFFF) + hash(x)) & 0xFFFFFFFF
		for x in self.cons_lb_f3:
			result = (((101 * result) & 0xFFFFFFFF) + hash(x)) & 0xFFFFFFFF
		for x in self.cons_xband:
			result = (((101 * result) & 0xFFFFFFFF) + hash(x)) & 0xFFFFFFFF
		return result
	
	def __str__(self):
		result = "("
		result += "Property_1_edge_reward = " + str(self.Property_1_edge_reward)
		result += ", dur_xband = " + str(self.dur_xband)
		result += ", dur_uhf = " + str(self.dur_uhf)
		result += ", dur_lb_f2 = " + str(self.dur_lb_f2)
		result += ", dur_lb_f3 = " + str(self.dur_lb_f3)
		result += ", cons_uhf = " + str(self.cons_uhf)
		result += ", cons_lb_f2 = " + str(self.cons_lb_f2)
		result += ", cons_lb_f3 = " + str(self.cons_lb_f3)
		result += ", cons_xband = " + str(self.cons_xband)
		result += ")"
		return result

# Automaton: Main
class MainAutomaton(object):
	__slots__ = ("network", "transition_counts", "transition_labels", "branch_counts")
	
	def __init__(self, network: Network):
		self.network = network
		self.transition_counts = [2, 2, 2, 2, 2, 2, 2, 2, 1]
		self.transition_labels = [[1, 19], [2, 19], [3, 19], [4, 19], [5, 19], [6, 19], [7, 19], [8, 19], [19]]
		self.branch_counts = [[1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1]]
	
	def set_initial_values(self, state: State) -> None:
		state.Main_location = 0
	
	def set_initial_transient_values(self, transient: Transient) -> None:
		pass
	
	def get_transient_value(self, state: State, transient_variable: str):
		location = state.Main_location
		return None
	
	def get_transition_count(self, state: State) -> int:
		return self.transition_counts[state.Main_location]
	
	def get_transition_label(self, state: State, transition: int) -> int:
		return self.transition_labels[state.Main_location][transition]
	
	def get_guard_value(self, state: State, transition: int) -> bool:
		location = state.Main_location
		if location == 8:
			return True
		elif location == 0:
			if transition == 0:
				return (state.cs == 64)
			elif transition == 1:
				return (state.cs < 64)
		elif location == 1:
			if transition == 0:
				return (state.cs == 59)
			elif transition == 1:
				return (state.cs < 59)
		elif location == 2:
			if transition == 0:
				return (state.cs == 33)
			elif transition == 1:
				return (state.cs < 33)
		elif location == 3:
			if transition == 0:
				return (state.cs == 58)
			elif transition == 1:
				return (state.cs < 58)
		elif location == 4:
			if transition == 0:
				return (state.cs == 34)
			elif transition == 1:
				return (state.cs < 34)
		elif location == 5:
			if transition == 0:
				return (state.cs == 48)
			elif transition == 1:
				return (state.cs < 48)
		elif location == 6:
			if transition == 0:
				return (state.cs == 44)
			elif transition == 1:
				return (state.cs < 44)
		elif location == 7:
			if transition == 0:
				return (state.cs == 58)
			elif transition == 1:
				return (state.cs < 58)
	
	def get_branch_count(self, state: State, transition: int) -> int:
		return self.branch_counts[state.Main_location][transition]
	
	def get_probability_value(self, state: State, transition: int, branch: int) -> float:
		location = state.Main_location
		if location == 0:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 1:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 2:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 3:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 4:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 5:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 6:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 7:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 8:
			if transition == 0:
				return 1
	
	def jump(self, state: State, transient: Transient, transition: int, branch: int, assignment_index: int, target_state: State, target_transient: Transient) -> None:
		if assignment_index == 0:
			location = state.Main_location
			if location == 0:
				if transition == 0:
					if branch == 0:
						target_state.cs = 0
						target_state.battery = (state.battery + -11624221)
			elif location == 1:
				if transition == 0:
					if branch == 0:
						target_state.cs = 0
						target_state.battery = (state.battery + 9466812)
			elif location == 2:
				if transition == 0:
					if branch == 0:
						target_state.cs = 0
						target_state.battery = (state.battery + -6013868)
			elif location == 3:
				if transition == 0:
					if branch == 0:
						target_state.cs = 0
						target_state.battery = (state.battery + 9474945)
			elif location == 4:
				if transition == 0:
					if branch == 0:
						target_state.cs = 0
						target_state.battery = (state.battery + -6004901)
			elif location == 5:
				if transition == 0:
					if branch == 0:
						target_state.cs = 0
						target_state.battery = (state.battery + 9480367)
			elif location == 6:
				if transition == 0:
					if branch == 0:
						target_state.cs = 0
						target_state.battery = (state.battery + -5998923)
			elif location == 7:
				if transition == 0:
					if branch == 0:
						target_state.cs = 0
						target_state.battery = (state.battery + 9488500)
		elif assignment_index == 2:
			location = state.Main_location
			if location == 0:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 1
				elif transition == 1:
					if branch == 0:
						target_state.Main_location = 0
			elif location == 1:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 2
				elif transition == 1:
					if branch == 0:
						target_state.Main_location = 1
			elif location == 2:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 3
				elif transition == 1:
					if branch == 0:
						target_state.Main_location = 2
			elif location == 3:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 4
				elif transition == 1:
					if branch == 0:
						target_state.Main_location = 3
			elif location == 4:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 5
				elif transition == 1:
					if branch == 0:
						target_state.Main_location = 4
			elif location == 5:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 6
				elif transition == 1:
					if branch == 0:
						target_state.Main_location = 5
			elif location == 6:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 7
				elif transition == 1:
					if branch == 0:
						target_state.Main_location = 6
			elif location == 7:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 8
				elif transition == 1:
					if branch == 0:
						target_state.Main_location = 7
			elif location == 8:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 8

# Automaton: Main
class MainAutomaton_1(object):
	__slots__ = ("network", "transition_counts", "transition_labels", "branch_counts")
	
	def __init__(self, network: Network):
		self.network = network
		self.transition_counts = [2, 3, 2, 2, 2, 2, 2, 3, 2, 2, 2, 2, 2, 3, 2, 2, 2, 3, 2, 2, 2, 2, 2, 3, 2, 2, 2, 3, 2, 2, 2, 2, 2, 3, 2, 2, 2, 1]
		self.transition_labels = [[9, 19], [10, 11, 19], [12, 19], [13, 19], [14, 19], [15, 19], [9, 19], [10, 11, 19], [12, 19], [13, 19], [14, 19], [15, 19], [9, 19], [10, 11, 19], [16, 19], [17, 19], [9, 19], [10, 11, 19], [12, 19], [13, 19], [14, 19], [15, 19], [9, 19], [10, 11, 19], [16, 19], [17, 19], [9, 19], [10, 11, 19], [12, 19], [13, 19], [14, 19], [15, 19], [9, 19], [10, 11, 19], [16, 19], [17, 19], [18, 19], [19]]
		self.branch_counts = [[1, 1], [1, 1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1, 1], [1, 1], [1, 1], [1, 1], [1, 1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1, 1], [1, 1], [1, 1], [1, 1], [1, 1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1, 1], [1, 1], [1, 1], [1, 1], [1]]
	
	def set_initial_values(self, state: State) -> None:
		state.Main_location_1 = 0
		state.n_n_n = 0
	
	def set_initial_transient_values(self, transient: Transient) -> None:
		pass
	
	def get_transient_value(self, state: State, transient_variable: str):
		location = state.Main_location_1
		return None
	
	def get_transition_count(self, state: State) -> int:
		return self.transition_counts[state.Main_location_1]
	
	def get_transition_label(self, state: State, transition: int) -> int:
		return self.transition_labels[state.Main_location_1][transition]
	
	def get_guard_value(self, state: State, transition: int) -> bool:
		location = state.Main_location_1
		if location == 37:
			return True
		elif location == 0:
			if transition == 0:
				return (state.d == 46)
			elif transition == 1:
				return (state.d < 46)
		elif location == 1:
			if transition == 0:
				return True
			elif transition == 1:
				return (state.battery >= (59904000 + (self.network._get_transient_value(state, "cons_lb_f3"))[0]))
			elif transition == 2:
				return True
		elif location == 2:
			if transition == 0:
				return (state.t == 20)
			elif transition == 1:
				return (state.t < 20)
		elif location == 3:
			if transition == 0:
				return (state.t == 10)
			elif transition == 1:
				return (state.t < 10)
		elif location == 4:
			if transition == 0:
				return (state.t == (self.network._get_transient_value(state, "dur_lb_f3"))[0])
			elif transition == 1:
				return (state.t < (self.network._get_transient_value(state, "dur_lb_f3"))[0])
		elif location == 5:
			if transition == 0:
				return (state.t == 10)
			elif transition == 1:
				return (state.t < 10)
		elif location == 6:
			if transition == 0:
				return (state.d == 45)
			elif transition == 1:
				return (state.d < 45)
		elif location == 7:
			if transition == 0:
				return True
			elif transition == 1:
				return (state.battery >= (59904000 + (self.network._get_transient_value(state, "cons_lb_f2"))[0]))
			elif transition == 2:
				return True
		elif location == 8:
			if transition == 0:
				return (state.t == 20)
			elif transition == 1:
				return (state.t < 20)
		elif location == 9:
			if transition == 0:
				return (state.t == 10)
			elif transition == 1:
				return (state.t < 10)
		elif location == 10:
			if transition == 0:
				return (state.t == (self.network._get_transient_value(state, "dur_lb_f2"))[0])
			elif transition == 1:
				return (state.t < (self.network._get_transient_value(state, "dur_lb_f2"))[0])
		elif location == 11:
			if transition == 0:
				return (state.t == 10)
			elif transition == 1:
				return (state.t < 10)
		elif location == 12:
			if transition == 0:
				return (state.d == 144)
			elif transition == 1:
				return (state.d < 144)
		elif location == 13:
			if transition == 0:
				return True
			elif transition == 1:
				return (state.battery >= (59904000 + (self.network._get_transient_value(state, "cons_uhf"))[0]))
			elif transition == 2:
				return True
		elif location == 14:
			if transition == 0:
				return (state.t == 20)
			elif transition == 1:
				return (state.t < 20)
		elif location == 15:
			if transition == 0:
				return (state.t == (self.network._get_transient_value(state, "dur_uhf"))[0])
			elif transition == 1:
				return (state.t < (self.network._get_transient_value(state, "dur_uhf"))[0])
		elif location == 16:
			if transition == 0:
				return (state.d == 74)
			elif transition == 1:
				return (state.d < 74)
		elif location == 17:
			if transition == 0:
				return True
			elif transition == 1:
				return (state.battery >= (59904000 + (self.network._get_transient_value(state, "cons_xband"))[0]))
			elif transition == 2:
				return True
		elif location == 18:
			if transition == 0:
				return (state.t == 20)
			elif transition == 1:
				return (state.t < 20)
		elif location == 19:
			if transition == 0:
				return (state.t == 10)
			elif transition == 1:
				return (state.t < 10)
		elif location == 20:
			if transition == 0:
				return (state.t == (self.network._get_transient_value(state, "dur_xband"))[0])
			elif transition == 1:
				return (state.t < (self.network._get_transient_value(state, "dur_xband"))[0])
		elif location == 21:
			if transition == 0:
				return (state.t == 10)
			elif transition == 1:
				return (state.t < 10)
		elif location == 22:
			if transition == 0:
				return (state.d == 18)
			elif transition == 1:
				return (state.d < 18)
		elif location == 23:
			if transition == 0:
				return True
			elif transition == 1:
				return (state.battery >= (59904000 + (self.network._get_transient_value(state, "cons_uhf"))[state.n_n_n]))
			elif transition == 2:
				return True
		elif location == 24:
			if transition == 0:
				return (state.t == 20)
			elif transition == 1:
				return (state.t < 20)
		elif location == 25:
			if transition == 0:
				return (state.t == (self.network._get_transient_value(state, "dur_uhf"))[state.n_n_n])
			elif transition == 1:
				return (state.t < (self.network._get_transient_value(state, "dur_uhf"))[state.n_n_n])
		elif location == 26:
			if transition == 0:
				return (state.d == 75)
			elif transition == 1:
				return (state.d < 75)
		elif location == 27:
			if transition == 0:
				return True
			elif transition == 1:
				return (state.battery >= (59904000 + (self.network._get_transient_value(state, "cons_xband"))[state.n_n_n]))
			elif transition == 2:
				return True
		elif location == 28:
			if transition == 0:
				return (state.t == 20)
			elif transition == 1:
				return (state.t < 20)
		elif location == 29:
			if transition == 0:
				return (state.t == 10)
			elif transition == 1:
				return (state.t < 10)
		elif location == 30:
			if transition == 0:
				return (state.t == (self.network._get_transient_value(state, "dur_xband"))[state.n_n_n])
			elif transition == 1:
				return (state.t < (self.network._get_transient_value(state, "dur_xband"))[state.n_n_n])
		elif location == 31:
			if transition == 0:
				return (state.t == 10)
			elif transition == 1:
				return (state.t < 10)
		elif location == 32:
			if transition == 0:
				return (state.d == 19)
			elif transition == 1:
				return (state.d < 19)
		elif location == 33:
			if transition == 0:
				return True
			elif transition == 1:
				return (state.battery >= (59904000 + (self.network._get_transient_value(state, "cons_uhf"))[state.n_n_n]))
			elif transition == 2:
				return True
		elif location == 34:
			if transition == 0:
				return (state.t == 20)
			elif transition == 1:
				return (state.t < 20)
		elif location == 35:
			if transition == 0:
				return (state.t == (self.network._get_transient_value(state, "dur_uhf"))[state.n_n_n])
			elif transition == 1:
				return (state.t < (self.network._get_transient_value(state, "dur_uhf"))[state.n_n_n])
		elif location == 36:
			if transition == 0:
				return (state.d == 59)
			elif transition == 1:
				return (state.d < 59)
	
	def get_branch_count(self, state: State, transition: int) -> int:
		return self.branch_counts[state.Main_location_1][transition]
	
	def get_probability_value(self, state: State, transition: int, branch: int) -> float:
		location = state.Main_location_1
		if location == 0:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 1:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
			elif transition == 2:
				return 1
		elif location == 2:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 3:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 4:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 5:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 6:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 7:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
			elif transition == 2:
				return 1
		elif location == 8:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 9:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 10:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 11:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 12:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 13:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
			elif transition == 2:
				return 1
		elif location == 14:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 15:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 16:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 17:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
			elif transition == 2:
				return 1
		elif location == 18:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 19:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 20:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 21:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 22:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 23:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
			elif transition == 2:
				return 1
		elif location == 24:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 25:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 26:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 27:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
			elif transition == 2:
				return 1
		elif location == 28:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 29:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 30:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 31:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 32:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 33:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
			elif transition == 2:
				return 1
		elif location == 34:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 35:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 36:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 37:
			if transition == 0:
				return 1
	
	def jump(self, state: State, transient: Transient, transition: int, branch: int, assignment_index: int, target_state: State, target_transient: Transient) -> None:
		if assignment_index == 0:
			location = state.Main_location_1
			if location == 0:
				if transition == 0:
					if branch == 0:
						target_state.d = 0
			elif location == 1:
				if transition == 0:
					if branch == 0:
						target_state.saved = (transient.cons_lb_f3)[0]
				elif transition == 1:
					if branch == 0:
						target_state.t = 0
						target_state.battery = (state.battery - 496800)
			elif location == 2:
				if transition == 0:
					if branch == 0:
						target_state.t = 0
						target_state.battery = (state.battery - 248400)
			elif location == 3:
				if transition == 0:
					if branch == 0:
						target_state.battery = (state.battery - (transient.cons_lb_f3)[0])
						target_state.t = 0
			elif location == 4:
				if transition == 0:
					if branch == 0:
						target_state.done_lb_f3 = (state.done_lb_f3 + 1)
						target_state.battery = (state.battery - 248400)
						target_state.t = 0
			elif location == 5:
				if transition == 0:
					if branch == 0:
						target_state.t = 0
			elif location == 6:
				if transition == 0:
					if branch == 0:
						target_state.d = 0
			elif location == 7:
				if transition == 0:
					if branch == 0:
						target_state.saved = (transient.cons_lb_f2)[0]
				elif transition == 1:
					if branch == 0:
						target_state.t = 0
						target_state.battery = (state.battery - 496800)
			elif location == 8:
				if transition == 0:
					if branch == 0:
						target_state.t = 0
						target_state.battery = (state.battery - 248400)
			elif location == 9:
				if transition == 0:
					if branch == 0:
						target_state.battery = (state.battery - (transient.cons_lb_f2)[0])
						target_state.t = 0
			elif location == 10:
				if transition == 0:
					if branch == 0:
						target_state.done_lb_f2 = (state.done_lb_f2 + 1)
						target_state.battery = (state.battery - 248400)
						target_state.t = 0
			elif location == 11:
				if transition == 0:
					if branch == 0:
						target_state.t = 0
			elif location == 12:
				if transition == 0:
					if branch == 0:
						target_state.d = 0
			elif location == 13:
				if transition == 0:
					if branch == 0:
						target_state.saved = (transient.cons_uhf)[0]
				elif transition == 1:
					if branch == 0:
						target_state.t = 0
						target_state.battery = (state.battery - 496800)
			elif location == 14:
				if transition == 0:
					if branch == 0:
						target_state.battery = (state.battery - (transient.cons_uhf)[0])
						target_state.t = 0
			elif location == 15:
				if transition == 0:
					if branch == 0:
						target_state.done_uhf = (state.done_uhf + 1)
						target_state.t = 0
			elif location == 16:
				if transition == 0:
					if branch == 0:
						target_state.d = 0
			elif location == 17:
				if transition == 0:
					if branch == 0:
						target_state.saved = (transient.cons_xband)[0]
				elif transition == 1:
					if branch == 0:
						target_state.t = 0
						target_state.battery = (state.battery - 496800)
			elif location == 18:
				if transition == 0:
					if branch == 0:
						target_state.t = 0
						target_state.battery = (state.battery - 248400)
			elif location == 19:
				if transition == 0:
					if branch == 0:
						target_state.battery = (state.battery - (transient.cons_xband)[0])
						target_state.t = 0
			elif location == 20:
				if transition == 0:
					if branch == 0:
						target_state.done_xband = (state.done_xband + 1)
						target_state.battery = (state.battery - 248400)
						target_state.t = 0
			elif location == 21:
				if transition == 0:
					if branch == 0:
						target_state.t = 0
			elif location == 22:
				if transition == 0:
					if branch == 0:
						target_state.d = 0
						target_state.n_n_n = 1
			elif location == 23:
				if transition == 0:
					if branch == 0:
						target_state.saved = (transient.cons_uhf)[state.n_n_n]
						target_state.n_n_n = 0
				elif transition == 1:
					if branch == 0:
						target_state.t = 0
						target_state.battery = (state.battery - 496800)
			elif location == 24:
				if transition == 0:
					if branch == 0:
						target_state.battery = (state.battery - (transient.cons_uhf)[state.n_n_n])
						target_state.t = 0
			elif location == 25:
				if transition == 0:
					if branch == 0:
						target_state.done_uhf = (state.done_uhf + 1)
						target_state.t = 0
						target_state.n_n_n = 0
			elif location == 26:
				if transition == 0:
					if branch == 0:
						target_state.d = 0
						target_state.n_n_n = 1
			elif location == 27:
				if transition == 0:
					if branch == 0:
						target_state.saved = (transient.cons_xband)[state.n_n_n]
						target_state.n_n_n = 0
				elif transition == 1:
					if branch == 0:
						target_state.t = 0
						target_state.battery = (state.battery - 496800)
			elif location == 28:
				if transition == 0:
					if branch == 0:
						target_state.t = 0
						target_state.battery = (state.battery - 248400)
			elif location == 29:
				if transition == 0:
					if branch == 0:
						target_state.battery = (state.battery - (transient.cons_xband)[state.n_n_n])
						target_state.t = 0
			elif location == 30:
				if transition == 0:
					if branch == 0:
						target_state.done_xband = (state.done_xband + 1)
						target_state.battery = (state.battery - 248400)
						target_state.t = 0
						target_state.n_n_n = 0
			elif location == 31:
				if transition == 0:
					if branch == 0:
						target_state.t = 0
			elif location == 32:
				if transition == 0:
					if branch == 0:
						target_state.d = 0
						target_state.n_n_n = 2
			elif location == 33:
				if transition == 0:
					if branch == 0:
						target_state.saved = (transient.cons_uhf)[state.n_n_n]
						target_state.n_n_n = 0
				elif transition == 1:
					if branch == 0:
						target_state.t = 0
						target_state.battery = (state.battery - 496800)
			elif location == 34:
				if transition == 0:
					if branch == 0:
						target_state.battery = (state.battery - (transient.cons_uhf)[state.n_n_n])
						target_state.t = 0
			elif location == 35:
				if transition == 0:
					if branch == 0:
						target_state.done_uhf = (state.done_uhf + 1)
						target_state.t = 0
						target_state.n_n_n = 0
			elif location == 36:
				if transition == 0:
					if branch == 0:
						target_state.time_elapsed = True
						target_state.d = 0
		elif assignment_index == 2:
			location = state.Main_location_1
			if location == 0:
				if transition == 0:
					if branch == 0:
						target_state.Main_location_1 = 1
				elif transition == 1:
					if branch == 0:
						target_state.Main_location_1 = 0
			elif location == 1:
				if transition == 0:
					if branch == 0:
						target_state.Main_location_1 = 6
				elif transition == 1:
					if branch == 0:
						target_state.Main_location_1 = 2
				elif transition == 2:
					if branch == 0:
						target_state.Main_location_1 = 1
			elif location == 2:
				if transition == 0:
					if branch == 0:
						target_state.Main_location_1 = 3
				elif transition == 1:
					if branch == 0:
						target_state.Main_location_1 = 2
			elif location == 3:
				if transition == 0:
					if branch == 0:
						target_state.Main_location_1 = 4
				elif transition == 1:
					if branch == 0:
						target_state.Main_location_1 = 3
			elif location == 4:
				if transition == 0:
					if branch == 0:
						target_state.Main_location_1 = 5
				elif transition == 1:
					if branch == 0:
						target_state.Main_location_1 = 4
			elif location == 5:
				if transition == 0:
					if branch == 0:
						target_state.Main_location_1 = 6
				elif transition == 1:
					if branch == 0:
						target_state.Main_location_1 = 5
			elif location == 6:
				if transition == 0:
					if branch == 0:
						target_state.Main_location_1 = 7
				elif transition == 1:
					if branch == 0:
						target_state.Main_location_1 = 6
			elif location == 7:
				if transition == 0:
					if branch == 0:
						target_state.Main_location_1 = 12
				elif transition == 1:
					if branch == 0:
						target_state.Main_location_1 = 8
				elif transition == 2:
					if branch == 0:
						target_state.Main_location_1 = 7
			elif location == 8:
				if transition == 0:
					if branch == 0:
						target_state.Main_location_1 = 9
				elif transition == 1:
					if branch == 0:
						target_state.Main_location_1 = 8
			elif location == 9:
				if transition == 0:
					if branch == 0:
						target_state.Main_location_1 = 10
				elif transition == 1:
					if branch == 0:
						target_state.Main_location_1 = 9
			elif location == 10:
				if transition == 0:
					if branch == 0:
						target_state.Main_location_1 = 11
				elif transition == 1:
					if branch == 0:
						target_state.Main_location_1 = 10
			elif location == 11:
				if transition == 0:
					if branch == 0:
						target_state.Main_location_1 = 12
				elif transition == 1:
					if branch == 0:
						target_state.Main_location_1 = 11
			elif location == 12:
				if transition == 0:
					if branch == 0:
						target_state.Main_location_1 = 13
				elif transition == 1:
					if branch == 0:
						target_state.Main_location_1 = 12
			elif location == 13:
				if transition == 0:
					if branch == 0:
						target_state.Main_location_1 = 16
				elif transition == 1:
					if branch == 0:
						target_state.Main_location_1 = 14
				elif transition == 2:
					if branch == 0:
						target_state.Main_location_1 = 13
			elif location == 14:
				if transition == 0:
					if branch == 0:
						target_state.Main_location_1 = 15
				elif transition == 1:
					if branch == 0:
						target_state.Main_location_1 = 14
			elif location == 15:
				if transition == 0:
					if branch == 0:
						target_state.Main_location_1 = 16
				elif transition == 1:
					if branch == 0:
						target_state.Main_location_1 = 15
			elif location == 16:
				if transition == 0:
					if branch == 0:
						target_state.Main_location_1 = 17
				elif transition == 1:
					if branch == 0:
						target_state.Main_location_1 = 16
			elif location == 17:
				if transition == 0:
					if branch == 0:
						target_state.Main_location_1 = 22
				elif transition == 1:
					if branch == 0:
						target_state.Main_location_1 = 18
				elif transition == 2:
					if branch == 0:
						target_state.Main_location_1 = 17
			elif location == 18:
				if transition == 0:
					if branch == 0:
						target_state.Main_location_1 = 19
				elif transition == 1:
					if branch == 0:
						target_state.Main_location_1 = 18
			elif location == 19:
				if transition == 0:
					if branch == 0:
						target_state.Main_location_1 = 20
				elif transition == 1:
					if branch == 0:
						target_state.Main_location_1 = 19
			elif location == 20:
				if transition == 0:
					if branch == 0:
						target_state.Main_location_1 = 21
				elif transition == 1:
					if branch == 0:
						target_state.Main_location_1 = 20
			elif location == 21:
				if transition == 0:
					if branch == 0:
						target_state.Main_location_1 = 22
				elif transition == 1:
					if branch == 0:
						target_state.Main_location_1 = 21
			elif location == 22:
				if transition == 0:
					if branch == 0:
						target_state.Main_location_1 = 23
				elif transition == 1:
					if branch == 0:
						target_state.Main_location_1 = 22
			elif location == 23:
				if transition == 0:
					if branch == 0:
						target_state.Main_location_1 = 26
				elif transition == 1:
					if branch == 0:
						target_state.Main_location_1 = 24
				elif transition == 2:
					if branch == 0:
						target_state.Main_location_1 = 23
			elif location == 24:
				if transition == 0:
					if branch == 0:
						target_state.Main_location_1 = 25
				elif transition == 1:
					if branch == 0:
						target_state.Main_location_1 = 24
			elif location == 25:
				if transition == 0:
					if branch == 0:
						target_state.Main_location_1 = 26
				elif transition == 1:
					if branch == 0:
						target_state.Main_location_1 = 25
			elif location == 26:
				if transition == 0:
					if branch == 0:
						target_state.Main_location_1 = 27
				elif transition == 1:
					if branch == 0:
						target_state.Main_location_1 = 26
			elif location == 27:
				if transition == 0:
					if branch == 0:
						target_state.Main_location_1 = 32
				elif transition == 1:
					if branch == 0:
						target_state.Main_location_1 = 28
				elif transition == 2:
					if branch == 0:
						target_state.Main_location_1 = 27
			elif location == 28:
				if transition == 0:
					if branch == 0:
						target_state.Main_location_1 = 29
				elif transition == 1:
					if branch == 0:
						target_state.Main_location_1 = 28
			elif location == 29:
				if transition == 0:
					if branch == 0:
						target_state.Main_location_1 = 30
				elif transition == 1:
					if branch == 0:
						target_state.Main_location_1 = 29
			elif location == 30:
				if transition == 0:
					if branch == 0:
						target_state.Main_location_1 = 31
				elif transition == 1:
					if branch == 0:
						target_state.Main_location_1 = 30
			elif location == 31:
				if transition == 0:
					if branch == 0:
						target_state.Main_location_1 = 32
				elif transition == 1:
					if branch == 0:
						target_state.Main_location_1 = 31
			elif location == 32:
				if transition == 0:
					if branch == 0:
						target_state.Main_location_1 = 33
				elif transition == 1:
					if branch == 0:
						target_state.Main_location_1 = 32
			elif location == 33:
				if transition == 0:
					if branch == 0:
						target_state.Main_location_1 = 36
				elif transition == 1:
					if branch == 0:
						target_state.Main_location_1 = 34
				elif transition == 2:
					if branch == 0:
						target_state.Main_location_1 = 33
			elif location == 34:
				if transition == 0:
					if branch == 0:
						target_state.Main_location_1 = 35
				elif transition == 1:
					if branch == 0:
						target_state.Main_location_1 = 34
			elif location == 35:
				if transition == 0:
					if branch == 0:
						target_state.Main_location_1 = 36
				elif transition == 1:
					if branch == 0:
						target_state.Main_location_1 = 35
			elif location == 36:
				if transition == 0:
					if branch == 0:
						target_state.Main_location_1 = 37
				elif transition == 1:
					if branch == 0:
						target_state.Main_location_1 = 36
			elif location == 37:
				if transition == 0:
					if branch == 0:
						target_state.Main_location_1 = 37

# Automaton: GlobalSync
class GlobalSyncAutomaton(object):
	__slots__ = ("network", "transition_counts", "transition_labels", "branch_counts")
	
	def __init__(self, network: Network):
		self.network = network
		self.transition_counts = [2]
		self.transition_labels = [[19, 20]]
		self.branch_counts = [[1, 1]]
	
	def set_initial_values(self, state: State) -> None:
		pass
	
	def set_initial_transient_values(self, transient: Transient) -> None:
		pass
	
	def get_transient_value(self, state: State, transient_variable: str):
		location = 0
		return None
	
	def get_transition_count(self, state: State) -> int:
		return self.transition_counts[0]
	
	def get_transition_label(self, state: State, transition: int) -> int:
		return self.transition_labels[0][transition]
	
	def get_guard_value(self, state: State, transition: int) -> bool:
		location = 0
		if location == 0:
			if transition >= 0 and transition < 2:
				return True
	
	def get_branch_count(self, state: State, transition: int) -> int:
		return self.branch_counts[0][transition]
	
	def get_probability_value(self, state: State, transition: int, branch: int) -> float:
		location = 0
		if location == 0:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
	
	def jump(self, state: State, transient: Transient, transition: int, branch: int, assignment_index: int, target_state: State, target_transient: Transient) -> None:
		if assignment_index == 0:
			location = 0
			if location == 0:
				if transition == 0:
					if branch == 0:
						target_state.d = min((state.d + 1), 145)
						target_state.t = min((state.t + 1), 101)
						target_state.cs = min((state.cs + 1), 65)
		elif assignment_index == 2:
			location = 0
			if location == 0:
				if transition == 0:
					if branch == 0:
						pass
				elif transition == 1:
					if branch == 0:
						target_transient.Property_1_edge_reward = state.saved

class PropertyExpression(object):
	__slots__ = ("op", "args")
	
	def __init__(self, op: str, args: List[Union[int, PropertyExpression]]):
		self.op = op
		self.args = args
	
	def __str__(self):
		result = self.op + "("
		needComma = False
		for arg in self.args:
			if needComma:
				result += ", "
			else:
				needComma = True
			result += str(arg)
		return result + ")"

class Property(object):
	__slots__ = ("name", "exp")
	
	def __init__(self, name: str, exp: PropertyExpression):
		self.name = name
		self.exp = exp
	
	def __str__(self):
		return self.name + ": " + str(self.exp)

class Transition(object):
	__slots__ = ("sync_vector", "label", "transitions")
	
	def __init__(self, sync_vector: int, label: int = 0, transitions: List[int] = [-1, -1, -1]):
		self.sync_vector = sync_vector
		self.label = label
		self.transitions = transitions

class Branch(object):
	__slots__ = ("probability", "branches")
	
	def __init__(self, probability = 0.0, branches = [0, 0, 0]):
		self.probability = probability
		self.branches = branches

class Network(object):
	__slots__ = ("network", "components", "transition_labels", "sync_vectors", "properties", "variables", "_initial_transient", "_aut_Main", "_aut_Main_1", "_aut_GlobalSync")
	
	def __init__(self):
		self.network = self
		self.transition_labels = { 0: "τ", 1: "sun_a", 2: "sun_b", 3: "sun_c", 4: "sun_d", 5: "sun_e", 6: "sun_f", 7: "sun_g", 8: "sun_h", 9: "event", 10: "skip", 11: "preheat", 12: "start_rotate", 13: "rot_start_job", 14: "df_rotate", 15: "rotated_back", 16: "norot_start_job", 17: "nodf_rotate", 18: "end", 19: "tick", 20: "tau" }
		self.sync_vectors = [[0, -1, -1, 0], [-1, 0, -1, 0], [-1, -1, 0, 0], [1, -1, 20, 1], [2, -1, 20, 2], [3, -1, 20, 3], [4, -1, 20, 4], [5, -1, 20, 5], [6, -1, 20, 6], [7, -1, 20, 7], [8, -1, 20, 8], [-1, 9, 20, 9], [-1, 10, 20, 10], [-1, 11, 20, 11], [-1, 12, 20, 12], [-1, 13, 20, 13], [-1, 14, 20, 14], [-1, 15, 20, 15], [-1, 16, 20, 16], [-1, 17, 20, 17], [-1, 18, 20, 18], [19, 19, 19, 19]]
		self.properties = [Property("Property 1", PropertyExpression("xmin", [0, PropertyExpression("ap", [1])]))]
		self.variables = [VariableInfo("battery", None, "int"), VariableInfo("done_uhf", None, "int"), VariableInfo("done_lb_f2", None, "int"), VariableInfo("done_lb_f3", None, "int"), VariableInfo("done_xband", None, "int"), VariableInfo("d", None, "int", 0, 145), VariableInfo("t", None, "int", 0, 101), VariableInfo("cs", None, "int", 0, 65), VariableInfo("time_elapsed", None, "bool"), VariableInfo("saved", None, "int", 0, 22000000), VariableInfo("Main_location", 0, "int", 0, 8), VariableInfo("Main_location", 1, "int", 0, 37), VariableInfo("n_n_n", 1, "int", 0, 2)]
		self._aut_Main = MainAutomaton(self)
		self._aut_Main_1 = MainAutomaton_1(self)
		self._aut_GlobalSync = GlobalSyncAutomaton(self)
		self.components = [self._aut_Main, self._aut_Main_1, self._aut_GlobalSync]
		self._initial_transient = self._get_initial_transient()
	
	def get_initial_state(self) -> State:
		state = State()
		state.battery = 149760000
		state.done_uhf = 0
		state.done_lb_f2 = 0
		state.done_lb_f3 = 0
		state.done_xband = 0
		state.d = 0
		state.t = 0
		state.cs = 0
		state.time_elapsed = False
		state.saved = 0
		self._aut_Main.set_initial_values(state)
		self._aut_Main_1.set_initial_values(state)
		self._aut_GlobalSync.set_initial_values(state)
		return state
	
	def _get_initial_transient(self) -> Transient:
		transient = Transient()
		transient.Property_1_edge_reward = 0
		transient.dur_xband = [6, 10]
		transient.dur_uhf = [4, 8, 10]
		transient.dur_lb_f2 = [93]
		transient.dur_lb_f3 = [93]
		transient.cons_uhf = [631200, 1262400, 1578000]
		transient.cons_lb_f2 = [21555540]
		transient.cons_lb_f3 = [21555540]
		transient.cons_xband = [4300200, 7167000]
		self._aut_Main.set_initial_transient_values(transient)
		self._aut_Main_1.set_initial_transient_values(transient)
		self._aut_GlobalSync.set_initial_transient_values(transient)
		return transient
	
	def get_expression_value(self, state: State, expression: int):
		if expression == 0:
			return self.network._get_transient_value(state, "Property_1_edge_reward")
		elif expression == 1:
			return state.time_elapsed
		else:
			raise IndexError
	
	def _get_jump_expression_value(self, state: State, transient: Transient, expression: int):
		if expression == 0:
			return transient.Property_1_edge_reward
		elif expression == 1:
			return state.time_elapsed
		else:
			raise IndexError
	
	def _get_transient_value(self, state: State, transient_variable: str):
		# Query the automata for the current value of the transient variable
		result = self._aut_Main.get_transient_value(state, transient_variable)
		if result is not None:
			return result
		result = self._aut_Main_1.get_transient_value(state, transient_variable)
		if result is not None:
			return result
		result = self._aut_GlobalSync.get_transient_value(state, transient_variable)
		if result is not None:
			return result
		# No automaton has a value: return the transient variable's (cached) initial value
		return getattr(self._initial_transient, transient_variable)
	
	def get_transitions(self, state: State) -> List[Transition]:
		# Collect all automaton transitions, gathered by label
		transitions = []
		trans_Main = [[], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], []]
		transition_count = self._aut_Main.get_transition_count(state)
		for i in range(transition_count):
			if self._aut_Main.get_guard_value(state, i):
				trans_Main[self._aut_Main.get_transition_label(state, i)].append(i)
		trans_Main_1 = [[], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], []]
		transition_count = self._aut_Main_1.get_transition_count(state)
		for i in range(transition_count):
			if self._aut_Main_1.get_guard_value(state, i):
				trans_Main_1[self._aut_Main_1.get_transition_label(state, i)].append(i)
		trans_GlobalSync = [[], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], []]
		transition_count = self._aut_GlobalSync.get_transition_count(state)
		for i in range(transition_count):
			if self._aut_GlobalSync.get_guard_value(state, i):
				trans_GlobalSync[self._aut_GlobalSync.get_transition_label(state, i)].append(i)
		# Match automaton transitions onto synchronisation vectors
		for svi in range(len(self.sync_vectors)):
			sv = self.sync_vectors[svi]
			synced = [[-1, -1, -1, -1]]
			# Main
			if synced is not None:
				if sv[0] != -1:
					if len(trans_Main[sv[0]]) == 0:
						synced = None
					else:
						existing = len(synced)
						for i in range(existing):
							synced[i][0] = trans_Main[sv[0]][0]
						for i in range(1, len(trans_Main[sv[0]])):
							for j in range(existing):
								synced.append(synced[j][:])
								synced[-1][0] = trans_Main[sv[0]][i]
			# Main
			if synced is not None:
				if sv[1] != -1:
					if len(trans_Main_1[sv[1]]) == 0:
						synced = None
					else:
						existing = len(synced)
						for i in range(existing):
							synced[i][1] = trans_Main_1[sv[1]][0]
						for i in range(1, len(trans_Main_1[sv[1]])):
							for j in range(existing):
								synced.append(synced[j][:])
								synced[-1][1] = trans_Main_1[sv[1]][i]
			# GlobalSync
			if synced is not None:
				if sv[2] != -1:
					if len(trans_GlobalSync[sv[2]]) == 0:
						synced = None
					else:
						existing = len(synced)
						for i in range(existing):
							synced[i][2] = trans_GlobalSync[sv[2]][0]
						for i in range(1, len(trans_GlobalSync[sv[2]])):
							for j in range(existing):
								synced.append(synced[j][:])
								synced[-1][2] = trans_GlobalSync[sv[2]][i]
			if synced is not None:
				for sync in synced:
					sync[-1] = sv[-1]
					sync.append(svi)
				transitions.extend(filter(lambda s: s[-2] != -1, synced))
		# Convert to Transition instances
		for i in range(len(transitions)):
			transitions[i] = Transition(transitions[i][-1], transitions[i][-2], transitions[i])
			del transitions[i].transitions[-1]
			del transitions[i].transitions[-1]
		# Done
		return transitions
	
	def get_branches(self, state: State, transition: Transition) -> List[Branch]:
		combs = [[-1, -1, -1]]
		probs = [1.0]
		if transition.transitions[0] != -1:
			existing = len(combs)
			branch_count = self._aut_Main.get_branch_count(state, transition.transitions[0])
			for i in range(1, branch_count):
				probability = self._aut_Main.get_probability_value(state, transition.transitions[0], i)
				for j in range(existing):
					combs.append(combs[j][:])
					combs[-1][0] = i
					probs.append(probs[j] * probability)
			probability = self._aut_Main.get_probability_value(state, transition.transitions[0], 0)
			for i in range(existing):
				combs[i][0] = 0
				probs[i] *= probability
		if transition.transitions[1] != -1:
			existing = len(combs)
			branch_count = self._aut_Main_1.get_branch_count(state, transition.transitions[1])
			for i in range(1, branch_count):
				probability = self._aut_Main_1.get_probability_value(state, transition.transitions[1], i)
				for j in range(existing):
					combs.append(combs[j][:])
					combs[-1][1] = i
					probs.append(probs[j] * probability)
			probability = self._aut_Main_1.get_probability_value(state, transition.transitions[1], 0)
			for i in range(existing):
				combs[i][1] = 0
				probs[i] *= probability
		if transition.transitions[2] != -1:
			existing = len(combs)
			branch_count = self._aut_GlobalSync.get_branch_count(state, transition.transitions[2])
			for i in range(1, branch_count):
				probability = self._aut_GlobalSync.get_probability_value(state, transition.transitions[2], i)
				for j in range(existing):
					combs.append(combs[j][:])
					combs[-1][2] = i
					probs.append(probs[j] * probability)
			probability = self._aut_GlobalSync.get_probability_value(state, transition.transitions[2], 0)
			for i in range(existing):
				combs[i][2] = 0
				probs[i] *= probability
		# Convert to Branch instances
		for i in range(len(combs)):
			combs[i] = Branch(probs[i], combs[i])
		# Done
		return list(filter(lambda b: b.probability > 0.0, combs))
	
	def jump(self, state: State, transition: Transition, branch: Branch, expressions: List[int] = []) -> State:
		transient = self._get_initial_transient()
		for i in range(0, 3):
			target_state = State()
			state.copy_to(target_state)
			target_transient = Transient()
			transient.copy_to(target_transient)
			if transition.transitions[0] != -1:
				self._aut_Main.jump(state, transient, transition.transitions[0], branch.branches[0], i, target_state, target_transient)
			if transition.transitions[1] != -1:
				self._aut_Main_1.jump(state, transient, transition.transitions[1], branch.branches[1], i, target_state, target_transient)
			if transition.transitions[2] != -1:
				self._aut_GlobalSync.jump(state, transient, transition.transitions[2], branch.branches[2], i, target_state, target_transient)
			state = target_state
			transient = target_transient
		for i in range(len(expressions)):
			expressions[i] = self._get_jump_expression_value(state, transient, expressions[i])
		return state
	
	def jump_np(self, state: State, transition: Transition, expressions: List[int] = []) -> State:
		return self.jump(state, transition, self.get_branches(state, transition)[0], expressions)
