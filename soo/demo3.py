# demo3

from __future__ import annotations
from typing import List, Union, Optional

class VariableInfo(object):
	__slots__ = ("name", "component", "type", "minValue", "maxValue")
	
	def __init__(self, name: str, component: Optional[int], type: str, minValue = None, maxValue = None):
		self.name = name
		self.component = component
		self.type = type
		self.minValue = minValue
		self.maxValue = maxValue

# States
class State(object):
	__slots__ = ("count", "Main_location")
	
	def get_variable_value(self, variable: int):
		if variable == 0:
			return self.count
		elif variable == 1:
			return self.Main_location
	
	def copy_to(self, other: State):
		other.count = self.count
		other.Main_location = self.Main_location
	
	def __eq__(self, other):
		return isinstance(other, self.__class__) and self.count == other.count and self.Main_location == other.Main_location
	
	def __ne__(self, other):
		return not self.__eq__(other)
	
	def __hash__(self):
		result = 75619
		result = (((101 * result) & 0xFFFFFFFF) + hash(self.count)) & 0xFFFFFFFF
		result = (((101 * result) & 0xFFFFFFFF) + hash(self.Main_location)) & 0xFFFFFFFF
		return result
	
	def __str__(self):
		result = "("
		result += "count = " + str(self.count)
		result += ", Main_location = " + str(self.Main_location)
		result += ")"
		return result

# Transients
class Transient(object):
	__slots__ = ()
	
	def copy_to(self, other: Transient):
		pass
	
	def __eq__(self, other):
		return isinstance(other, self.__class__)
	
	def __ne__(self, other):
		return not self.__eq__(other)
	
	def __hash__(self):
		result = 75619
		return result
	
	def __str__(self):
		result = "("
		result += ")"
		return result

# Automaton: Main
class MainAutomaton(object):
	__slots__ = ("network", "transition_counts", "transition_labels", "branch_counts")
	
	def __init__(self, network: Network):
		self.network = network
		self.transition_counts = [3, 1, 1, 2, 1, 1, 2, 1, 1, 2, 1, 1, 2, 1, 1, 2, 1, 1, 2, 1, 1, 2, 1, 1, 2, 1, 1, 2, 1, 1, 2, 1, 1, 2, 1, 1, 2, 1, 1, 2, 1, 1, 2, 1, 1, 2, 1, 1, 2, 1, 1, 2, 1, 1, 2, 1, 1, 2, 1, 1, 2, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
		self.transition_labels = [[1, 2, 3], [4], [0], [1, 3], [4], [0], [1, 3], [4], [0], [1, 3], [4], [0], [1, 3], [4], [0], [1, 3], [4], [0], [1, 3], [4], [0], [1, 3], [4], [0], [1, 3], [4], [0], [1, 3], [4], [0], [1, 3], [4], [0], [1, 3], [4], [0], [1, 3], [4], [0], [1, 3], [4], [0], [1, 3], [4], [0], [1, 3], [4], [0], [1, 3], [4], [0], [1, 3], [4], [0], [1, 3], [4], [0], [1, 3], [4], [0], [1, 3], [4], [0], [], [5], [5], [5], [5], [5], [5], [5], [5], [5], [5], [5], [5], [5], [5], [5], [5], [5], [5], [5], [5], [6], [5]]
		self.branch_counts = [[1, 1, 1], [1], [1], [1, 1], [1], [1], [1, 1], [1], [1], [1, 1], [1], [1], [1, 1], [1], [1], [1, 1], [1], [1], [1, 1], [1], [1], [1, 1], [1], [1], [1, 1], [1], [1], [1, 1], [1], [1], [1, 1], [1], [1], [1, 1], [1], [1], [1, 1], [1], [1], [1, 1], [1], [1], [1, 1], [1], [1], [1, 1], [1], [1], [1, 1], [1], [1], [1, 1], [1], [1], [1, 1], [1], [1], [1, 1], [1], [1], [1, 1], [1], [1], [], [1], [1], [1], [1], [1], [1], [1], [1], [1], [1], [1], [1], [1], [1], [1], [1], [1], [1], [1], [1], [1], [1]]
	
	def set_initial_values(self, state: State) -> None:
		state.Main_location = 0
	
	def set_initial_transient_values(self, transient: Transient) -> None:
		pass
	
	def get_transient_value(self, state: State, transient_variable: str):
		location = state.Main_location
		return None
	
	def get_transition_count(self, state: State) -> int:
		return self.transition_counts[state.Main_location]
	
	def get_transition_label(self, state: State, transition: int) -> int:
		return self.transition_labels[state.Main_location][transition]
	
	def get_guard_value(self, state: State, transition: int) -> bool:
		location = state.Main_location
		if location == 1 or location == 2 or location == 4 or location == 5 or location == 7 or location == 8 or location == 10 or location == 11 or location == 13 or location == 14 or location == 16 or location == 17 or location == 19 or location == 20 or location == 22 or location == 23 or location == 25 or location == 26 or location == 28 or location == 29 or location == 31 or location == 32 or location == 34 or location == 35 or location == 37 or location == 38 or location == 40 or location == 41 or location == 43 or location == 44 or location == 46 or location == 47 or location == 49 or location == 50 or location == 52 or location == 53 or location == 55 or location == 56 or location == 58 or location == 59 or location == 61 or location == 62 or location == 64 or location == 65 or location == 66 or location == 67 or location == 68 or location == 69 or location == 70 or location == 71 or location == 72 or location == 73 or location == 74 or location == 75 or location == 76 or location == 77 or location == 78 or location == 79 or location == 80 or location == 81 or location == 82 or location == 83 or location == 84 or location == 85:
			return True
		elif location == 0:
			if transition >= 0 and transition < 3:
				return True
		elif location == 3:
			if transition >= 0 and transition < 2:
				return True
		elif location == 6:
			if transition >= 0 and transition < 2:
				return True
		elif location == 9:
			if transition >= 0 and transition < 2:
				return True
		elif location == 12:
			if transition >= 0 and transition < 2:
				return True
		elif location == 15:
			if transition >= 0 and transition < 2:
				return True
		elif location == 18:
			if transition >= 0 and transition < 2:
				return True
		elif location == 21:
			if transition >= 0 and transition < 2:
				return True
		elif location == 24:
			if transition >= 0 and transition < 2:
				return True
		elif location == 27:
			if transition >= 0 and transition < 2:
				return True
		elif location == 30:
			if transition >= 0 and transition < 2:
				return True
		elif location == 33:
			if transition >= 0 and transition < 2:
				return True
		elif location == 36:
			if transition >= 0 and transition < 2:
				return True
		elif location == 39:
			if transition >= 0 and transition < 2:
				return True
		elif location == 42:
			if transition >= 0 and transition < 2:
				return True
		elif location == 45:
			if transition >= 0 and transition < 2:
				return True
		elif location == 48:
			if transition >= 0 and transition < 2:
				return True
		elif location == 51:
			if transition >= 0 and transition < 2:
				return True
		elif location == 54:
			if transition >= 0 and transition < 2:
				return True
		elif location == 57:
			if transition >= 0 and transition < 2:
				return True
		elif location == 60:
			if transition >= 0 and transition < 2:
				return True
	
	def get_branch_count(self, state: State, transition: int) -> int:
		return self.branch_counts[state.Main_location][transition]
	
	def get_probability_value(self, state: State, transition: int, branch: int) -> float:
		location = state.Main_location
		if location == 0:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
			elif transition == 2:
				return 1
		elif location == 1:
			if transition == 0:
				return 1
		elif location == 2:
			if transition == 0:
				return 1
		elif location == 3:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 4:
			if transition == 0:
				return 1
		elif location == 5:
			if transition == 0:
				return 1
		elif location == 6:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 7:
			if transition == 0:
				return 1
		elif location == 8:
			if transition == 0:
				return 1
		elif location == 9:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 10:
			if transition == 0:
				return 1
		elif location == 11:
			if transition == 0:
				return 1
		elif location == 12:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 13:
			if transition == 0:
				return 1
		elif location == 14:
			if transition == 0:
				return 1
		elif location == 15:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 16:
			if transition == 0:
				return 1
		elif location == 17:
			if transition == 0:
				return 1
		elif location == 18:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 19:
			if transition == 0:
				return 1
		elif location == 20:
			if transition == 0:
				return 1
		elif location == 21:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 22:
			if transition == 0:
				return 1
		elif location == 23:
			if transition == 0:
				return 1
		elif location == 24:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 25:
			if transition == 0:
				return 1
		elif location == 26:
			if transition == 0:
				return 1
		elif location == 27:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 28:
			if transition == 0:
				return 1
		elif location == 29:
			if transition == 0:
				return 1
		elif location == 30:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 31:
			if transition == 0:
				return 1
		elif location == 32:
			if transition == 0:
				return 1
		elif location == 33:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 34:
			if transition == 0:
				return 1
		elif location == 35:
			if transition == 0:
				return 1
		elif location == 36:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 37:
			if transition == 0:
				return 1
		elif location == 38:
			if transition == 0:
				return 1
		elif location == 39:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 40:
			if transition == 0:
				return 1
		elif location == 41:
			if transition == 0:
				return 1
		elif location == 42:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 43:
			if transition == 0:
				return 1
		elif location == 44:
			if transition == 0:
				return 1
		elif location == 45:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 46:
			if transition == 0:
				return 1
		elif location == 47:
			if transition == 0:
				return 1
		elif location == 48:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 49:
			if transition == 0:
				return 1
		elif location == 50:
			if transition == 0:
				return 1
		elif location == 51:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 52:
			if transition == 0:
				return 1
		elif location == 53:
			if transition == 0:
				return 1
		elif location == 54:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 55:
			if transition == 0:
				return 1
		elif location == 56:
			if transition == 0:
				return 1
		elif location == 57:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 58:
			if transition == 0:
				return 1
		elif location == 59:
			if transition == 0:
				return 1
		elif location == 60:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 61:
			if transition == 0:
				return 1
		elif location == 62:
			if transition == 0:
				return 1
		elif location == 63:
			pass
		elif location == 64:
			if transition == 0:
				return 1
		elif location == 65:
			if transition == 0:
				return 1
		elif location == 66:
			if transition == 0:
				return 1
		elif location == 67:
			if transition == 0:
				return 1
		elif location == 68:
			if transition == 0:
				return 1
		elif location == 69:
			if transition == 0:
				return 1
		elif location == 70:
			if transition == 0:
				return 1
		elif location == 71:
			if transition == 0:
				return 1
		elif location == 72:
			if transition == 0:
				return 1
		elif location == 73:
			if transition == 0:
				return 1
		elif location == 74:
			if transition == 0:
				return 1
		elif location == 75:
			if transition == 0:
				return 1
		elif location == 76:
			if transition == 0:
				return 1
		elif location == 77:
			if transition == 0:
				return 1
		elif location == 78:
			if transition == 0:
				return 1
		elif location == 79:
			if transition == 0:
				return 1
		elif location == 80:
			if transition == 0:
				return 1
		elif location == 81:
			if transition == 0:
				return 1
		elif location == 82:
			if transition == 0:
				return 1
		elif location == 83:
			if transition == 0:
				return 1
		elif location == 84:
			if transition == 0:
				return 1
		elif location == 85:
			if transition == 0:
				return 1
	
	def jump(self, state: State, transient: Transient, transition: int, branch: int, assignment_index: int, target_state: State, target_transient: Transient) -> None:
		if assignment_index == 0:
			location = state.Main_location
			if location == 0:
				if transition == 0:
					if branch == 0:
						target_state.count = min((state.count + 9), 3000)
						target_state.Main_location = 85
				elif transition == 1:
					if branch == 0:
						target_state.count = min((state.count + 9), 3000)
						target_state.Main_location = 84
				elif transition == 2:
					if branch == 0:
						target_state.count = min((state.count + 3), 3000)
						target_state.Main_location = 1
			elif location == 1:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 2
			elif location == 2:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 3
			elif location == 3:
				if transition == 0:
					if branch == 0:
						target_state.count = min((state.count + 9), 3000)
						target_state.Main_location = 83
				elif transition == 1:
					if branch == 0:
						target_state.count = min((state.count + 3), 3000)
						target_state.Main_location = 4
			elif location == 4:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 5
			elif location == 5:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 6
			elif location == 6:
				if transition == 0:
					if branch == 0:
						target_state.count = min((state.count + 9), 3000)
						target_state.Main_location = 82
				elif transition == 1:
					if branch == 0:
						target_state.count = min((state.count + 3), 3000)
						target_state.Main_location = 7
			elif location == 7:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 8
			elif location == 8:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 9
			elif location == 9:
				if transition == 0:
					if branch == 0:
						target_state.count = min((state.count + 9), 3000)
						target_state.Main_location = 81
				elif transition == 1:
					if branch == 0:
						target_state.count = min((state.count + 3), 3000)
						target_state.Main_location = 10
			elif location == 10:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 11
			elif location == 11:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 12
			elif location == 12:
				if transition == 0:
					if branch == 0:
						target_state.count = min((state.count + 9), 3000)
						target_state.Main_location = 80
				elif transition == 1:
					if branch == 0:
						target_state.count = min((state.count + 3), 3000)
						target_state.Main_location = 13
			elif location == 13:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 14
			elif location == 14:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 15
			elif location == 15:
				if transition == 0:
					if branch == 0:
						target_state.count = min((state.count + 9), 3000)
						target_state.Main_location = 79
				elif transition == 1:
					if branch == 0:
						target_state.count = min((state.count + 3), 3000)
						target_state.Main_location = 16
			elif location == 16:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 17
			elif location == 17:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 18
			elif location == 18:
				if transition == 0:
					if branch == 0:
						target_state.count = min((state.count + 9), 3000)
						target_state.Main_location = 78
				elif transition == 1:
					if branch == 0:
						target_state.count = min((state.count + 3), 3000)
						target_state.Main_location = 19
			elif location == 19:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 20
			elif location == 20:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 21
			elif location == 21:
				if transition == 0:
					if branch == 0:
						target_state.count = min((state.count + 9), 3000)
						target_state.Main_location = 77
				elif transition == 1:
					if branch == 0:
						target_state.count = min((state.count + 3), 3000)
						target_state.Main_location = 22
			elif location == 22:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 23
			elif location == 23:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 24
			elif location == 24:
				if transition == 0:
					if branch == 0:
						target_state.count = min((state.count + 9), 3000)
						target_state.Main_location = 76
				elif transition == 1:
					if branch == 0:
						target_state.count = min((state.count + 3), 3000)
						target_state.Main_location = 25
			elif location == 25:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 26
			elif location == 26:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 27
			elif location == 27:
				if transition == 0:
					if branch == 0:
						target_state.count = min((state.count + 9), 3000)
						target_state.Main_location = 75
				elif transition == 1:
					if branch == 0:
						target_state.count = min((state.count + 3), 3000)
						target_state.Main_location = 28
			elif location == 28:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 29
			elif location == 29:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 30
			elif location == 30:
				if transition == 0:
					if branch == 0:
						target_state.count = min((state.count + 9), 3000)
						target_state.Main_location = 74
				elif transition == 1:
					if branch == 0:
						target_state.count = min((state.count + 3), 3000)
						target_state.Main_location = 31
			elif location == 31:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 32
			elif location == 32:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 33
			elif location == 33:
				if transition == 0:
					if branch == 0:
						target_state.count = min((state.count + 9), 3000)
						target_state.Main_location = 73
				elif transition == 1:
					if branch == 0:
						target_state.count = min((state.count + 3), 3000)
						target_state.Main_location = 34
			elif location == 34:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 35
			elif location == 35:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 36
			elif location == 36:
				if transition == 0:
					if branch == 0:
						target_state.count = min((state.count + 9), 3000)
						target_state.Main_location = 72
				elif transition == 1:
					if branch == 0:
						target_state.count = min((state.count + 3), 3000)
						target_state.Main_location = 37
			elif location == 37:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 38
			elif location == 38:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 39
			elif location == 39:
				if transition == 0:
					if branch == 0:
						target_state.count = min((state.count + 9), 3000)
						target_state.Main_location = 71
				elif transition == 1:
					if branch == 0:
						target_state.count = min((state.count + 3), 3000)
						target_state.Main_location = 40
			elif location == 40:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 41
			elif location == 41:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 42
			elif location == 42:
				if transition == 0:
					if branch == 0:
						target_state.count = min((state.count + 9), 3000)
						target_state.Main_location = 70
				elif transition == 1:
					if branch == 0:
						target_state.count = min((state.count + 3), 3000)
						target_state.Main_location = 43
			elif location == 43:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 44
			elif location == 44:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 45
			elif location == 45:
				if transition == 0:
					if branch == 0:
						target_state.count = min((state.count + 9), 3000)
						target_state.Main_location = 69
				elif transition == 1:
					if branch == 0:
						target_state.count = min((state.count + 3), 3000)
						target_state.Main_location = 46
			elif location == 46:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 47
			elif location == 47:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 48
			elif location == 48:
				if transition == 0:
					if branch == 0:
						target_state.count = min((state.count + 9), 3000)
						target_state.Main_location = 68
				elif transition == 1:
					if branch == 0:
						target_state.count = min((state.count + 3), 3000)
						target_state.Main_location = 49
			elif location == 49:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 50
			elif location == 50:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 51
			elif location == 51:
				if transition == 0:
					if branch == 0:
						target_state.count = min((state.count + 9), 3000)
						target_state.Main_location = 67
				elif transition == 1:
					if branch == 0:
						target_state.count = min((state.count + 3), 3000)
						target_state.Main_location = 52
			elif location == 52:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 53
			elif location == 53:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 54
			elif location == 54:
				if transition == 0:
					if branch == 0:
						target_state.count = min((state.count + 9), 3000)
						target_state.Main_location = 66
				elif transition == 1:
					if branch == 0:
						target_state.count = min((state.count + 3), 3000)
						target_state.Main_location = 55
			elif location == 55:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 56
			elif location == 56:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 57
			elif location == 57:
				if transition == 0:
					if branch == 0:
						target_state.count = min((state.count + 9), 3000)
						target_state.Main_location = 65
				elif transition == 1:
					if branch == 0:
						target_state.count = min((state.count + 3), 3000)
						target_state.Main_location = 58
			elif location == 58:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 59
			elif location == 59:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 60
			elif location == 60:
				if transition == 0:
					if branch == 0:
						target_state.count = min((state.count + 9), 3000)
						target_state.Main_location = 64
				elif transition == 1:
					if branch == 0:
						target_state.count = min((state.count + 3), 3000)
						target_state.Main_location = 61
			elif location == 61:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 62
			elif location == 62:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 63
			elif location == 64:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 62
			elif location == 65:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 59
			elif location == 66:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 56
			elif location == 67:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 53
			elif location == 68:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 50
			elif location == 69:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 47
			elif location == 70:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 44
			elif location == 71:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 41
			elif location == 72:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 38
			elif location == 73:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 35
			elif location == 74:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 32
			elif location == 75:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 29
			elif location == 76:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 26
			elif location == 77:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 23
			elif location == 78:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 20
			elif location == 79:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 17
			elif location == 80:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 14
			elif location == 81:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 11
			elif location == 82:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 8
			elif location == 83:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 5
			elif location == 84:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 2
			elif location == 85:
				if transition == 0:
					if branch == 0:
						target_state.Main_location = 2

class PropertyExpression(object):
	__slots__ = ("op", "args")
	
	def __init__(self, op: str, args: List[Union[int, PropertyExpression]]):
		self.op = op
		self.args = args
	
	def __str__(self):
		result = self.op + "("
		needComma = False
		for arg in self.args:
			if needComma:
				result += ", "
			else:
				needComma = True
			result += str(arg)
		return result + ")"

class Property(object):
	__slots__ = ("name", "exp")
	
	def __init__(self, name: str, exp: PropertyExpression):
		self.name = name
		self.exp = exp
	
	def __str__(self):
		return self.name + ": " + str(self.exp)

class Transition(object):
	__slots__ = ("sync_vector", "label", "transitions")
	
	def __init__(self, sync_vector: int, label: int = 0, transitions: List[int] = [-1]):
		self.sync_vector = sync_vector
		self.label = label
		self.transitions = transitions

class Branch(object):
	__slots__ = ("probability", "branches")
	
	def __init__(self, probability = 0.0, branches = [0]):
		self.probability = probability
		self.branches = branches

class Network(object):
	__slots__ = ("network", "components", "transition_labels", "sync_vectors", "properties", "variables", "_initial_transient", "_aut_Main")
	
	def __init__(self):
		self.network = self
		self.transition_labels = { 0: "τ", 1: "doJob", 2: "doJob2", 3: "skipJob", 4: "c", 5: "b", 6: "d" }
		self.sync_vectors = [[0, 0], [1, 1], [2, 2], [3, 3], [4, 4], [5, 5], [6, 6]]
		self.properties = [Property("DidWeDoC", PropertyExpression("exists", [PropertyExpression("eventually", [PropertyExpression("ap", [0])])])), Property("allc", PropertyExpression("exists", [PropertyExpression("always", [PropertyExpression("ap", [1])])]))]
		self.variables = [VariableInfo("count", None, "int", 0, 3000), VariableInfo("Main_location", 0, "int", 0, 85)]
		self._aut_Main = MainAutomaton(self)
		self.components = [self._aut_Main]
		self._initial_transient = self._get_initial_transient()
	
	def get_initial_state(self) -> State:
		state = State()
		state.count = 0
		self._aut_Main.set_initial_values(state)
		return state
	
	def _get_initial_transient(self) -> Transient:
		transient = Transient()
		self._aut_Main.set_initial_transient_values(transient)
		return transient
	
	def get_expression_value(self, state: State, expression: int):
		if expression == 0:
			return False
		elif expression == 1:
			return True
		else:
			raise IndexError
	
	def _get_jump_expression_value(self, state: State, transient: Transient, expression: int):
		if expression == 0:
			return False
		elif expression == 1:
			return True
		else:
			raise IndexError
	
	def _get_transient_value(self, state: State, transient_variable: str):
		# Query the automata for the current value of the transient variable
		result = self._aut_Main.get_transient_value(state, transient_variable)
		if result is not None:
			return result
		# No automaton has a value: return the transient variable's (cached) initial value
		return getattr(self._initial_transient, transient_variable)
	
	def get_transitions(self, state: State) -> List[Transition]:
		# Collect all automaton transitions, gathered by label
		transitions = []
		trans_Main = [[], [], [], [], [], [], []]
		transition_count = self._aut_Main.get_transition_count(state)
		for i in range(transition_count):
			if self._aut_Main.get_guard_value(state, i):
				trans_Main[self._aut_Main.get_transition_label(state, i)].append(i)
		# Match automaton transitions onto synchronisation vectors
		for svi in range(len(self.sync_vectors)):
			sv = self.sync_vectors[svi]
			synced = [[-1, -1]]
			# Main
			if synced is not None:
				if sv[0] != -1:
					if len(trans_Main[sv[0]]) == 0:
						synced = None
					else:
						existing = len(synced)
						for i in range(existing):
							synced[i][0] = trans_Main[sv[0]][0]
						for i in range(1, len(trans_Main[sv[0]])):
							for j in range(existing):
								synced.append(synced[j][:])
								synced[-1][0] = trans_Main[sv[0]][i]
			if synced is not None:
				for sync in synced:
					sync[-1] = sv[-1]
					sync.append(svi)
				transitions.extend(filter(lambda s: s[-2] != -1, synced))
		# Convert to Transition instances
		for i in range(len(transitions)):
			transitions[i] = Transition(transitions[i][-1], transitions[i][-2], transitions[i])
			del transitions[i].transitions[-1]
			del transitions[i].transitions[-1]
		# Done
		return transitions
	
	def get_branches(self, state: State, transition: Transition) -> List[Branch]:
		combs = [[-1]]
		probs = [1.0]
		if transition.transitions[0] != -1:
			existing = len(combs)
			branch_count = self._aut_Main.get_branch_count(state, transition.transitions[0])
			for i in range(1, branch_count):
				probability = self._aut_Main.get_probability_value(state, transition.transitions[0], i)
				for j in range(existing):
					combs.append(combs[j][:])
					combs[-1][0] = i
					probs.append(probs[j] * probability)
			probability = self._aut_Main.get_probability_value(state, transition.transitions[0], 0)
			for i in range(existing):
				combs[i][0] = 0
				probs[i] *= probability
		# Convert to Branch instances
		for i in range(len(combs)):
			combs[i] = Branch(probs[i], combs[i])
		# Done
		return list(filter(lambda b: b.probability > 0.0, combs))
	
	def jump(self, state: State, transition: Transition, branch: Branch, expressions: List[int] = []) -> State:
		transient = self._get_initial_transient()
		for i in range(0, 1):
			target_state = State()
			state.copy_to(target_state)
			target_transient = Transient()
			transient.copy_to(target_transient)
			if transition.transitions[0] != -1:
				self._aut_Main.jump(state, transient, transition.transitions[0], branch.branches[0], i, target_state, target_transient)
			state = target_state
			transient = target_transient
		for i in range(len(expressions)):
			expressions[i] = self._get_jump_expression_value(state, transient, expressions[i])
		return state
	
	def jump_np(self, state: State, transition: Transition, expressions: List[int] = []) -> State:
		return self.jump(state, transition, self.get_branches(state, transition)[0], expressions)
