import time
def paths2state(state_id, rtransition_dictionary):
	paths2state = {}
	lastadded ={}
	i=0
	stop_cond = True
	for state in rtransition_dictionary[state_id]:
		paths2state[i]={state_id,state}
		lastadded[i] = state
		if (state != 1):
			stop_cond =False
		i=i+1

	while stop_cond:
		stop_cond = True
		len_paths2state = len(paths2state)
		for j in range(len_paths2state):
			if(lastadded[j]!=1):
				parents = rtransition_dictionary[lastadded[j]]
				k=0
				for parent in parents:
					if(k!=0):
						paths2state[i+1]= paths2state[j].add(parent)
					else:
						paths2state[j].add(parent)

					if (parent !=1):
						stop_cond = False






def paths2state00(state_id, rtransition_dictionary):
	paths=[]
	stop_condition=True
	print("[DEGUG] p2s point 0")
	for state in rtransition_dictionary[state_id]:
		paths.append([state,state_id])
		if(state!=1):
			stop_condition=False # as soon as you find a state different then the initial state set the stop condition to false
	
	print("[DEGUG] p2s paths:",paths)
	print("[DEGUG] p2s point 1")
	debug_set = set()
	while not stop_condition:
		#print("[DEGUG] p2s point 2")
		stop_condition=True
		paths_update=[]
		for path in paths:
#			print("[DEGUG] p2s point 3")
#			print("[DEGUG] p2s paths_update:", paths_update)
			if (path[0]!=1):
				current_state_id=path[0]
				back_states=rtransition_dictionary[current_state_id]
				print("[DEGUG] p2s back_states",back_states)
				for back_state in back_states:
					#paths_update=paths_update+[[back_state]+path]
					debug_set.add(back_state)
					if (back_state != 1):
						stop_condition=False
			else:
				paths_update.append(path)
		paths=paths_update.copy()
		#print("[DEGUG] p2s paths:",len(paths[0]))
	#return paths
	print(debug_set)
	return []




def paths2state0(state_id, rtransition_dictionary):
	paths=[]
	stop_condition=True
	print("[DEGUG] p2s point 0")
	for state in rtransition_dictionary[state_id]:
		paths.append([state,state_id])
		if(state!=1):
			stop_condition=False # as soon as you find a state different then the initial state set the stop condition to false
	
	print("[DEGUG] p2s paths:",paths)
	print("[DEGUG] p2s point 1")
	while not stop_condition:
		#print("[DEGUG] p2s point 2")
		stop_condition=True
		paths_update=[]
		for path in paths:
#			print("[DEGUG] p2s point 3")
#			print("[DEGUG] p2s paths_update:", paths_update)
			if (path[0]!=1):
				current_state_id=path[0]
				back_states=rtransition_dictionary[current_state_id]
				for back_state in back_states:
					paths_update.append([back_state]+path)
					if (back_state != 1):
						stop_condition=False
			else:
				paths_update.append(path)
		paths=paths_update.copy()
		#print("[DEGUG] p2s paths:",len(paths[0]))
	return paths



#network.get_expression_value(current_state, ap)


def myfunc(a,b, *args, **kwargs):
      c = kwargs.get('c', None)
      d = kwargs.get('d', None)
      if(c):
      	print("c was given")
      else:
      	print("c was NOT given")
      print("a: ",a)
      print("b: ",b)
      print("c: ",c)
      print("d: ",d)

#myfunc(a,b, c='nick', d='dog', ...)
myfunc('demo a', 'demo b', d="I am d")



# initializing dictionary 
test_dict = {0 : 11, 1 : 2, 2 : 11, 3:8, 4:2} 
  
# printing original dictionary 
print("The original dictionary is : " + str(test_dict)) 
  
# Using min() + list comprehension + values() 
# Finding min value keys in dictionary

m1_start=time.time()
temp = min(test_dict.values()) 
res = [key for key in test_dict if test_dict[key] == temp] 
m1_stop=time.time()


# printing result  
print("Keys with minimum values are : " + str(res)) 
print("temp: ", temp)
m2_start=time.time()
res =  [key for key in test_dict if 
        all(test_dict[temp] >= test_dict[key] 
        for temp in test_dict)] 
m2_stop=time.time() 

# printing result  
print("Keys with minimum values are : " + str(res)) 
print('Method 1: ', m1_stop-m1_start)
print('Method 2: ', m2_stop-m2_start)