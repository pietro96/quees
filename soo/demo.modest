//
// Common synchronising actions:
//
action close, open, approaching, leave;
bool crossing = false;
bool opened = true;


//
// Properties
//
property GateIsUnsafe = E <> (crossing && opened);
property pGateIsUnsafe = Pmax(<> (crossing && opened));

//
// A model for the Train
//
process Train(int MIN_TIME, int MAX_TIME){
	clock t;
	
	do{
		approaching {= t = 0 =};
		invariant(t <= MAX_TIME) when(t >= MIN_TIME) {= t = 0, crossing = true =};
		invariant(t <= 10) leave {= crossing = false =}
	}
}

//
// A model for the Controller. Notice that Modest uses weak invariant and thus
// we can use invariant(false) to model an urgent transition.
//
process Controller(){
	do{
		approaching; invariant(false) close; leave; invariant(false) open
	}
}

//
// A model for the Gate
//
process Gate(){
	clock g;
	
	do{
		close {= g = 0 =};
		invariant(g <= 6) when(g>=6) {= g = 0, opened = false =};
		open {= g = 0 =};
		invariant(g <= 4) when(g>=4) {= opened = true =}
	}
}

//
// A model for the Gate with the 30 min time-over 
//
process GateTO(){
	clock g;
	
	do{
		close {= g = 0 =};
		invariant(g <= 6) when(g>=6) {= g = 0, opened = false =};
		alt{ :: open {= g = 0 =}
			 :: when(g>=30) tau {= g = 0 =}
			 };
		invariant(g <= 4) when(g>=4) {= opened = true =}
	}
}

//
// Parallel composition into an interacting network
//
par {
	:: Train(7,41)
	:: Gate()
	:: Controller()
}
