# scheduler_v1

from __future__ import annotations
from typing import List, Union, Optional

class VariableInfo(object):
	__slots__ = ("name", "component", "type", "minValue", "maxValue")
	
	def __init__(self, name: str, component: Optional[int], type: str, minValue = None, maxValue = None):
		self.name = name
		self.component = component
		self.type = type
		self.minValue = minValue
		self.maxValue = maxValue

# States
class State(object):
	__slots__ = ("success", "battery_level", "JobProvider_location", "t", "c", "jid")
	
	def get_variable_value(self, variable: int):
		if variable == 0:
			return self.success
		elif variable == 1:
			return self.battery_level
		elif variable == 2:
			return self.JobProvider_location
		elif variable == 3:
			return self.t
		elif variable == 4:
			return self.c
		elif variable == 5:
			return self.jid
	
	def copy_to(self, other: State):
		other.success = self.success
		other.battery_level = self.battery_level
		other.JobProvider_location = self.JobProvider_location
		other.t = self.t
		other.c = self.c
		other.jid = self.jid
	
	def __eq__(self, other):
		return isinstance(other, self.__class__) and self.success == other.success and self.battery_level == other.battery_level and self.JobProvider_location == other.JobProvider_location and self.t == other.t and self.c == other.c and self.jid == other.jid
	
	def __ne__(self, other):
		return not self.__eq__(other)
	
	def __hash__(self):
		result = 75619
		result = (((101 * result) & 0xFFFFFFFF) + hash(self.success)) & 0xFFFFFFFF
		result = (((101 * result) & 0xFFFFFFFF) + hash(self.battery_level)) & 0xFFFFFFFF
		result = (((101 * result) & 0xFFFFFFFF) + hash(self.JobProvider_location)) & 0xFFFFFFFF
		result = (((101 * result) & 0xFFFFFFFF) + hash(self.t)) & 0xFFFFFFFF
		result = (((101 * result) & 0xFFFFFFFF) + hash(self.c)) & 0xFFFFFFFF
		result = (((101 * result) & 0xFFFFFFFF) + hash(self.jid)) & 0xFFFFFFFF
		return result
	
	def __str__(self):
		result = "("
		result += "success = " + str(self.success)
		result += ", battery_level = " + str(self.battery_level)
		result += ", JobProvider_location = " + str(self.JobProvider_location)
		result += ", t = " + str(self.t)
		result += ", c = " + str(self.c)
		result += ", jid = " + str(self.jid)
		result += ")"
		return result

# Transients
class Transient(object):
	__slots__ = ("jobs", "jobs_starttimes", "skippedJob")
	
	def copy_to(self, other: Transient):
		other.jobs = list(self.jobs)
		other.jobs_starttimes = list(self.jobs_starttimes)
		other.skippedJob = self.skippedJob
	
	def __eq__(self, other):
		return isinstance(other, self.__class__) and self.jobs == other.jobs and self.jobs_starttimes == other.jobs_starttimes and self.skippedJob == other.skippedJob
	
	def __ne__(self, other):
		return not self.__eq__(other)
	
	def __hash__(self):
		result = 75619
		for x in self.jobs:
			result = (((101 * result) & 0xFFFFFFFF) + hash(x)) & 0xFFFFFFFF
		for x in self.jobs_starttimes:
			result = (((101 * result) & 0xFFFFFFFF) + hash(x)) & 0xFFFFFFFF
		result = (((101 * result) & 0xFFFFFFFF) + hash(self.skippedJob)) & 0xFFFFFFFF
		return result
	
	def __str__(self):
		result = "("
		result += "jobs = " + str(self.jobs)
		result += ", jobs_starttimes = " + str(self.jobs_starttimes)
		result += ", skippedJob = " + str(self.skippedJob)
		result += ")"
		return result

# Automaton: JobProvider
class JobProviderAutomaton(object):
	__slots__ = ("network", "transition_counts", "transition_labels", "branch_counts")
	
	def __init__(self, network: Network):
		self.network = network
		self.transition_counts = [2, 2, 3, 2, 3, 2, 3, 2, 3, 1, 3, 2, 2, 3, 2, 2, 3, 2, 2]
		self.transition_labels = [[1, 10], [2, 10], [3, 4, 10], [2, 10], [3, 4, 10], [2, 10], [3, 4, 10], [2, 10], [5, 6, 10], [10], [7, 8, 10], [9, 10], [9, 10], [7, 8, 10], [9, 10], [9, 10], [7, 8, 10], [9, 10], [9, 10]]
		self.branch_counts = [[1, 1], [1, 1], [1, 1, 1], [1, 1], [1, 1, 1], [1, 1], [1, 1, 1], [1, 1], [1, 1, 1], [1], [1, 1, 1], [1, 1], [1, 1], [1, 1, 1], [1, 1], [1, 1], [1, 1, 1], [1, 1], [1, 1]]
	
	def set_initial_values(self, state: State) -> None:
		state.JobProvider_location = 0
		state.t = 0
		state.c = 0
		state.jid = 0
	
	def set_initial_transient_values(self, transient: Transient) -> None:
		pass
	
	def get_transient_value(self, state: State, transient_variable: str):
		location = state.JobProvider_location
		return None
	
	def get_transition_count(self, state: State) -> int:
		return self.transition_counts[state.JobProvider_location]
	
	def get_transition_label(self, state: State, transition: int) -> int:
		return self.transition_labels[state.JobProvider_location][transition]
	
	def get_guard_value(self, state: State, transition: int) -> bool:
		location = state.JobProvider_location
		if location == 9:
			return True
		elif location == 0:
			if transition >= 0 and transition < 2:
				return True
		elif location == 1:
			if transition == 0:
				return True
			elif transition == 1:
				return (state.battery_level <= 80)
		elif location == 2:
			if transition == 0:
				return ((state.battery_level > 40) and (state.t >= (self.network._get_transient_value(state, "jobs_starttimes"))[0]))
			elif transition == 1:
				return ((state.battery_level <= 40) and (state.t >= (self.network._get_transient_value(state, "jobs_starttimes"))[0]))
			elif transition == 2:
				return (state.battery_level >= 40)
		elif location == 3:
			if transition >= 0 and transition < 2:
				return True
		elif location == 4:
			if transition == 0:
				return ((state.battery_level > 40) and (state.t >= (self.network._get_transient_value(state, "jobs_starttimes"))[1]))
			elif transition == 1:
				return ((state.battery_level <= 40) and (state.t >= (self.network._get_transient_value(state, "jobs_starttimes"))[1]))
			elif transition == 2:
				return (state.battery_level >= 40)
		elif location == 5:
			if transition >= 0 and transition < 2:
				return True
		elif location == 6:
			if transition == 0:
				return ((state.battery_level > 40) and (state.t >= (self.network._get_transient_value(state, "jobs_starttimes"))[2]))
			elif transition == 1:
				return ((state.battery_level <= 40) and (state.t >= (self.network._get_transient_value(state, "jobs_starttimes"))[2]))
			elif transition == 2:
				return (state.battery_level >= 40)
		elif location == 7:
			if transition >= 0 and transition < 2:
				return True
		elif location == 8:
			if transition == 0:
				return (state.battery_level >= 40)
			elif transition == 1:
				return (state.battery_level < 40)
			elif transition == 2:
				return True
		elif location == 10:
			if transition == 0:
				return (state.jid == 0)
			elif transition == 1:
				return (state.jid == 1)
			elif transition == 2:
				return True
		elif location == 11:
			if transition == 0:
				return (state.c >= 517)
			elif transition == 1:
				return True
		elif location == 12:
			if transition == 0:
				return (state.c >= 5570)
			elif transition == 1:
				return True
		elif location == 13:
			if transition == 0:
				return (state.jid == 0)
			elif transition == 1:
				return (state.jid == 1)
			elif transition == 2:
				return True
		elif location == 14:
			if transition == 0:
				return (state.c >= 517)
			elif transition == 1:
				return True
		elif location == 15:
			if transition == 0:
				return (state.c >= 5570)
			elif transition == 1:
				return True
		elif location == 16:
			if transition == 0:
				return (state.jid == 0)
			elif transition == 1:
				return (state.jid == 1)
			elif transition == 2:
				return True
		elif location == 17:
			if transition == 0:
				return (state.c >= 517)
			elif transition == 1:
				return True
		elif location == 18:
			if transition == 0:
				return (state.c >= 5570)
			elif transition == 1:
				return True
	
	def get_branch_count(self, state: State, transition: int) -> int:
		return self.branch_counts[state.JobProvider_location][transition]
	
	def get_probability_value(self, state: State, transition: int, branch: int) -> float:
		location = state.JobProvider_location
		if location == 0:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 1:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 2:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
			elif transition == 2:
				return 1
		elif location == 3:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 4:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
			elif transition == 2:
				return 1
		elif location == 5:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 6:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
			elif transition == 2:
				return 1
		elif location == 7:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 8:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
			elif transition == 2:
				return 1
		elif location == 9:
			if transition == 0:
				return 1
		elif location == 10:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
			elif transition == 2:
				return 1
		elif location == 11:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 12:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 13:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
			elif transition == 2:
				return 1
		elif location == 14:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 15:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 16:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
			elif transition == 2:
				return 1
		elif location == 17:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
		elif location == 18:
			if transition == 0:
				return 1
			elif transition == 1:
				return 1
	
	def jump(self, state: State, transient: Transient, transition: int, branch: int, assignment_index: int, target_state: State, target_transient: Transient) -> None:
		if assignment_index == 0:
			location = state.JobProvider_location
			if location == 0:
				if transition == 0:
					if branch == 0:
						target_state.battery_level = 80
				elif transition == 1:
					if branch == 0:
						target_state.t = min((state.t + 1), 14263)
			elif location == 1:
				if transition == 0:
					if branch == 0:
						target_state.jid = (transient.jobs)[0]
				elif transition == 1:
					if branch == 0:
						target_state.t = min((state.t + 1), 14263)
			elif location == 2:
				if transition == 0:
					if branch == 0:
						target_state.c = 0
				elif transition == 1:
					if branch == 0:
						target_transient.skippedJob = 1
						target_state.jid = 0
				elif transition == 2:
					if branch == 0:
						target_state.t = min((state.t + 1), 14263)
			elif location == 3:
				if transition == 0:
					if branch == 0:
						target_state.jid = (transient.jobs)[1]
				elif transition == 1:
					if branch == 0:
						target_state.t = min((state.t + 1), 14263)
			elif location == 4:
				if transition == 0:
					if branch == 0:
						target_state.c = 0
				elif transition == 1:
					if branch == 0:
						target_transient.skippedJob = 1
						target_state.jid = 0
				elif transition == 2:
					if branch == 0:
						target_state.t = min((state.t + 1), 14263)
			elif location == 5:
				if transition == 0:
					if branch == 0:
						target_state.jid = (transient.jobs)[2]
				elif transition == 1:
					if branch == 0:
						target_state.t = min((state.t + 1), 14263)
			elif location == 6:
				if transition == 0:
					if branch == 0:
						target_state.c = 0
				elif transition == 1:
					if branch == 0:
						target_transient.skippedJob = 1
						target_state.jid = 0
				elif transition == 2:
					if branch == 0:
						target_state.t = min((state.t + 1), 14263)
			elif location == 8:
				if transition == 0:
					if branch == 0:
						target_state.success = True
			elif location == 10:
				if transition == 0:
					if branch == 0:
						target_state.battery_level = (state.battery_level - 10)
						target_state.jid = 0
				elif transition == 1:
					if branch == 0:
						target_state.battery_level = (state.battery_level - 5)
						target_state.jid = 0
				elif transition == 2:
					if branch == 0:
						target_state.c = min((state.c + 1), 5571)
			elif location == 11:
				if transition == 1:
					if branch == 0:
						target_state.c = min((state.c + 1), 5571)
			elif location == 12:
				if transition == 1:
					if branch == 0:
						target_state.c = min((state.c + 1), 5571)
			elif location == 13:
				if transition == 0:
					if branch == 0:
						target_state.battery_level = (state.battery_level - 10)
						target_state.jid = 0
				elif transition == 1:
					if branch == 0:
						target_state.battery_level = (state.battery_level - 5)
						target_state.jid = 0
				elif transition == 2:
					if branch == 0:
						target_state.t = min((state.t + 1), 14263)
						target_state.c = min((state.c + 1), 5571)
			elif location == 14:
				if transition == 0:
					if branch == 0:
						target_state.jid = (transient.jobs)[2]
						target_state.c = 0
				elif transition == 1:
					if branch == 0:
						target_state.t = min((state.t + 1), 14263)
						target_state.c = min((state.c + 1), 5571)
			elif location == 15:
				if transition == 0:
					if branch == 0:
						target_state.jid = (transient.jobs)[2]
						target_state.c = 0
				elif transition == 1:
					if branch == 0:
						target_state.t = min((state.t + 1), 14263)
						target_state.c = min((state.c + 1), 5571)
			elif location == 16:
				if transition == 0:
					if branch == 0:
						target_state.battery_level = (state.battery_level - 10)
						target_state.jid = 0
				elif transition == 1:
					if branch == 0:
						target_state.battery_level = (state.battery_level - 5)
						target_state.jid = 0
				elif transition == 2:
					if branch == 0:
						target_state.t = min((state.t + 1), 14263)
						target_state.c = min((state.c + 1), 5571)
			elif location == 17:
				if transition == 0:
					if branch == 0:
						target_state.jid = (transient.jobs)[1]
						target_state.c = 0
				elif transition == 1:
					if branch == 0:
						target_state.t = min((state.t + 1), 14263)
						target_state.c = min((state.c + 1), 5571)
			elif location == 18:
				if transition == 0:
					if branch == 0:
						target_state.jid = (transient.jobs)[1]
						target_state.c = 0
				elif transition == 1:
					if branch == 0:
						target_state.t = min((state.t + 1), 14263)
						target_state.c = min((state.c + 1), 5571)
		elif assignment_index == 2:
			location = state.JobProvider_location
			if location == 0:
				if transition == 0:
					if branch == 0:
						target_state.JobProvider_location = 1
				elif transition == 1:
					if branch == 0:
						target_state.JobProvider_location = 0
			elif location == 1:
				if transition == 0:
					if branch == 0:
						target_state.JobProvider_location = 2
				elif transition == 1:
					if branch == 0:
						target_state.JobProvider_location = 1
			elif location == 2:
				if transition == 0:
					if branch == 0:
						target_state.JobProvider_location = 16
				elif transition == 1:
					if branch == 0:
						target_state.JobProvider_location = 3
				elif transition == 2:
					if branch == 0:
						target_state.JobProvider_location = 2
			elif location == 3:
				if transition == 0:
					if branch == 0:
						target_state.JobProvider_location = 4
				elif transition == 1:
					if branch == 0:
						target_state.JobProvider_location = 3
			elif location == 4:
				if transition == 0:
					if branch == 0:
						target_state.JobProvider_location = 13
				elif transition == 1:
					if branch == 0:
						target_state.JobProvider_location = 5
				elif transition == 2:
					if branch == 0:
						target_state.JobProvider_location = 4
			elif location == 5:
				if transition == 0:
					if branch == 0:
						target_state.JobProvider_location = 6
				elif transition == 1:
					if branch == 0:
						target_state.JobProvider_location = 5
			elif location == 6:
				if transition == 0:
					if branch == 0:
						target_state.t = 0
						target_state.JobProvider_location = 10
				elif transition == 1:
					if branch == 0:
						target_state.t = 0
						target_state.JobProvider_location = 7
				elif transition == 2:
					if branch == 0:
						target_state.JobProvider_location = 6
			elif location == 7:
				if transition == 0:
					if branch == 0:
						target_state.JobProvider_location = 8
				elif transition == 1:
					if branch == 0:
						target_state.JobProvider_location = 7
			elif location == 8:
				if transition == 0:
					if branch == 0:
						target_state.JobProvider_location = 9
				elif transition == 1:
					if branch == 0:
						target_state.JobProvider_location = 9
				elif transition == 2:
					if branch == 0:
						target_state.JobProvider_location = 8
			elif location == 9:
				if transition == 0:
					if branch == 0:
						target_state.JobProvider_location = 9
			elif location == 10:
				if transition == 0:
					if branch == 0:
						target_state.JobProvider_location = 12
				elif transition == 1:
					if branch == 0:
						target_state.JobProvider_location = 11
				elif transition == 2:
					if branch == 0:
						target_state.JobProvider_location = 10
			elif location == 11:
				if transition == 0:
					if branch == 0:
						target_state.c = 0
						target_state.JobProvider_location = 8
				elif transition == 1:
					if branch == 0:
						target_state.JobProvider_location = 11
			elif location == 12:
				if transition == 0:
					if branch == 0:
						target_state.c = 0
						target_state.JobProvider_location = 8
				elif transition == 1:
					if branch == 0:
						target_state.JobProvider_location = 12
			elif location == 13:
				if transition == 0:
					if branch == 0:
						target_state.JobProvider_location = 15
				elif transition == 1:
					if branch == 0:
						target_state.JobProvider_location = 14
				elif transition == 2:
					if branch == 0:
						target_state.JobProvider_location = 13
			elif location == 14:
				if transition == 0:
					if branch == 0:
						target_state.JobProvider_location = 6
				elif transition == 1:
					if branch == 0:
						target_state.JobProvider_location = 14
			elif location == 15:
				if transition == 0:
					if branch == 0:
						target_state.JobProvider_location = 6
				elif transition == 1:
					if branch == 0:
						target_state.JobProvider_location = 15
			elif location == 16:
				if transition == 0:
					if branch == 0:
						target_state.JobProvider_location = 18
				elif transition == 1:
					if branch == 0:
						target_state.JobProvider_location = 17
				elif transition == 2:
					if branch == 0:
						target_state.JobProvider_location = 16
			elif location == 17:
				if transition == 0:
					if branch == 0:
						target_state.JobProvider_location = 4
				elif transition == 1:
					if branch == 0:
						target_state.JobProvider_location = 17
			elif location == 18:
				if transition == 0:
					if branch == 0:
						target_state.JobProvider_location = 4
				elif transition == 1:
					if branch == 0:
						target_state.JobProvider_location = 18

class PropertyExpression(object):
	__slots__ = ("op", "args")
	
	def __init__(self, op: str, args: List[Union[int, PropertyExpression]]):
		self.op = op
		self.args = args
	
	def __str__(self):
		result = self.op + "("
		needComma = False
		for arg in self.args:
			if needComma:
				result += ", "
			else:
				needComma = True
			result += str(arg)
		return result + ")"

class Property(object):
	__slots__ = ("name", "exp")
	
	def __init__(self, name: str, exp: PropertyExpression):
		self.name = name
		self.exp = exp
	
	def __str__(self):
		return self.name + ": " + str(self.exp)

class Transition(object):
	__slots__ = ("sync_vector", "label", "transitions")
	
	def __init__(self, sync_vector: int, label: int = 0, transitions: List[int] = [-1]):
		self.sync_vector = sync_vector
		self.label = label
		self.transitions = transitions

class Branch(object):
	__slots__ = ("probability", "branches")
	
	def __init__(self, probability = 0.0, branches = [0]):
		self.probability = probability
		self.branches = branches

class Network(object):
	__slots__ = ("network", "components", "transition_labels", "sync_vectors", "properties", "variables", "_initial_transient", "_aut_JobProvider")
	
	def __init__(self):
		self.network = self
		self.transition_labels = { 0: "τ", 1: "init", 2: "idle", 3: "check", 4: "skipJob", 5: "Suc", 6: "Fail", 7: "lband", 8: "xband", 9: "break", 10: "tick" }
		self.sync_vectors = [[0, 0], [1, 1], [2, 2], [3, 3], [4, 4], [5, 5], [6, 6], [7, 7], [8, 8], [9, 0], [10, 10]]
		self.properties = [Property("E_Succ", PropertyExpression("exists", [PropertyExpression("eventually", [PropertyExpression("ap", [0])])]))]
		self.variables = [VariableInfo("success", None, "bool"), VariableInfo("battery_level", None, "int"), VariableInfo("JobProvider_location", 0, "int", 0, 18), VariableInfo("t", 0, "int", 0, 14263), VariableInfo("c", 0, "int", 0, 5571), VariableInfo("jid", 0, "int", 0, 2)]
		self._aut_JobProvider = JobProviderAutomaton(self)
		self.components = [self._aut_JobProvider]
		self._initial_transient = self._get_initial_transient()
	
	def get_initial_state(self) -> State:
		state = State()
		state.success = False
		state.battery_level = 0
		self._aut_JobProvider.set_initial_values(state)
		return state
	
	def _get_initial_transient(self) -> Transient:
		transient = Transient()
		transient.jobs = [0, 0, 1, 0, 0, 0]
		transient.jobs_starttimes = [2707, 5368, 6837, 8522, 11205, 14261]
		transient.skippedJob = 0
		self._aut_JobProvider.set_initial_transient_values(transient)
		return transient
	
	def get_expression_value(self, state: State, expression: int):
		if expression == 0:
			return state.success
		else:
			raise IndexError
	
	def _get_jump_expression_value(self, state: State, transient: Transient, expression: int):
		if expression == 0:
			return state.success
		else:
			raise IndexError
	
	def _get_transient_value(self, state: State, transient_variable: str):
		# Query the automata for the current value of the transient variable
		result = self._aut_JobProvider.get_transient_value(state, transient_variable)
		if result is not None:
			return result
		# No automaton has a value: return the transient variable's (cached) initial value
		return getattr(self._initial_transient, transient_variable)
	
	def get_transitions(self, state: State) -> List[Transition]:
		# Collect all automaton transitions, gathered by label
		transitions = []
		trans_JobProvider = [[], [], [], [], [], [], [], [], [], [], []]
		transition_count = self._aut_JobProvider.get_transition_count(state)
		for i in range(transition_count):
			if self._aut_JobProvider.get_guard_value(state, i):
				trans_JobProvider[self._aut_JobProvider.get_transition_label(state, i)].append(i)
		# Match automaton transitions onto synchronisation vectors
		for svi in range(len(self.sync_vectors)):
			sv = self.sync_vectors[svi]
			synced = [[-1, -1]]
			# JobProvider
			if synced is not None:
				if sv[0] != -1:
					if len(trans_JobProvider[sv[0]]) == 0:
						synced = None
					else:
						existing = len(synced)
						for i in range(existing):
							synced[i][0] = trans_JobProvider[sv[0]][0]
						for i in range(1, len(trans_JobProvider[sv[0]])):
							for j in range(existing):
								synced.append(synced[j][:])
								synced[-1][0] = trans_JobProvider[sv[0]][i]
			if synced is not None:
				for sync in synced:
					sync[-1] = sv[-1]
					sync.append(svi)
				transitions.extend(filter(lambda s: s[-2] != -1, synced))
		# Convert to Transition instances
		for i in range(len(transitions)):
			transitions[i] = Transition(transitions[i][-1], transitions[i][-2], transitions[i])
			del transitions[i].transitions[-1]
			del transitions[i].transitions[-1]
		# Done
		return transitions
	
	def get_branches(self, state: State, transition: Transition) -> List[Branch]:
		combs = [[-1]]
		probs = [1.0]
		if transition.transitions[0] != -1:
			existing = len(combs)
			branch_count = self._aut_JobProvider.get_branch_count(state, transition.transitions[0])
			for i in range(1, branch_count):
				probability = self._aut_JobProvider.get_probability_value(state, transition.transitions[0], i)
				for j in range(existing):
					combs.append(combs[j][:])
					combs[-1][0] = i
					probs.append(probs[j] * probability)
			probability = self._aut_JobProvider.get_probability_value(state, transition.transitions[0], 0)
			for i in range(existing):
				combs[i][0] = 0
				probs[i] *= probability
		# Convert to Branch instances
		for i in range(len(combs)):
			combs[i] = Branch(probs[i], combs[i])
		# Done
		return list(filter(lambda b: b.probability > 0.0, combs))
	
	def jump(self, state: State, transition: Transition, branch: Branch, expressions: List[int] = []) -> State:
		transient = self._get_initial_transient()
		for i in range(0, 3):
			target_state = State()
			state.copy_to(target_state)
			target_transient = Transient()
			transient.copy_to(target_transient)
			if transition.transitions[0] != -1:
				self._aut_JobProvider.jump(state, transient, transition.transitions[0], branch.branches[0], i, target_state, target_transient)
			state = target_state
			transient = target_transient
		for i in range(len(expressions)):
			expressions[i] = self._get_jump_expression_value(state, transient, expressions[i])
		return state
	
	def jump_np(self, state: State, transition: Transition, expressions: List[int] = []) -> State:
		return self.jump(state, transition, self.get_branches(state, transition)[0], expressions)
